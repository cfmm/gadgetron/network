#include "gadgetron_protocol/Deserializers.hpp"
#include "gadgetron_protocol/MessagePayload.hpp"
#include "gadgetron_protocol/Serializers.hpp"
#include "gadgetron_protocol/SimpleProcessingClient.hpp"
#include <boost/program_options.hpp>
#include <fstream>
#include <ismrmrd/dataset.h>
#include <ismrmrd/ismrmrd.h>
#include <ismrmrd/waveform.h>
#include <ismrmrd/xml.h>

namespace po = boost::program_options;
namespace gp = gadgetron_protocol;

std::mutex mtx;

int main(int argc, char** argv) {
    std::string hostName;
    std::string port;
    std::string inFilename;
    std::string hdf5InGroup;
    std::string configFile;
    std::string configFileLocal;
    std::string configXmlLocal;
    unsigned int timeoutMs;
    bool openInputFile = true;

    po::options_description desc("Allowed options");

    desc.add_options()("help,h", "Produce help message")("port,p", po::value<std::string>(&port)->default_value("9002"),
                                                         "Port")(
        "address,a", po::value<std::string>(&hostName)->default_value("localhost"),
        "Address (hostname) of Gadgetron host")("filename,f", po::value<std::string>(&inFilename),
                                                "Input ISMRMRD dataset file")(
        "in-group,g", po::value<std::string>(&hdf5InGroup)->default_value("/dataset"), "Input data group")(
        "config-filename,c", po::value<std::string>(&configFile)->default_value("default.xml"),
        "Configuration filename on remote")("config-local,C", po::value<std::string>(&configFileLocal),
                                            "Local configuration script to be read and sent")(
        "timeout,t", po::value<unsigned int>(&timeoutMs)->default_value(10000), "Timeout [ms]");

    po::variables_map vm;
    po::store(po::parse_command_line(argc, argv, desc), vm);
    po::notify(vm);

    if (vm.count("help")) {
        std::cout << desc << std::endl;
        return 0;
    }

    if (!vm.count("filename")) {
        std::cout << std::endl << std::endl << "\tYou must supply a filename" << std::endl << std::endl;
        std::cout << desc << std::endl;
        return -1;
    }
    if (vm.count("config-local")) {
        std::ifstream t(configFileLocal.c_str());
        if (t) {
            // Read in the file.
            configXmlLocal = std::string((std::istreambuf_iterator<char>(t)), std::istreambuf_iterator<char>());
        } else {
            std::cout << "Unable to read local xml configuration: " << configFileLocal << std::endl;
            return -1;
        }
    }
    std::string hdf5XmlVarname = std::string(hdf5InGroup) + std::string("/xml");
    std::string hdf5DataVarname = std::string(hdf5InGroup) + std::string("/data");
    std::string hdf5WaveformVarname = std::string(hdf5InGroup) + std::string("/waveform");

    // Let's open the input file
    std::shared_ptr<ISMRMRD::Dataset> ismrmrdDataset;
    ISMRMRD::IsmrmrdHeader configParams;
    if (openInputFile) {
        ismrmrdDataset = std::make_shared<ISMRMRD::Dataset>(inFilename.c_str(), hdf5InGroup.c_str(), false);
        // Read the header
        std::string xmlConfig;
        ismrmrdDataset->readHeader(xmlConfig);
        ISMRMRD::deserialize(xmlConfig.c_str(), configParams);
    }

    std::cout << "Gadgetron ISMRMRD client" << std::endl;
    std::cout << "  -- host            :      " << hostName << std::endl;
    std::cout << "  -- port            :      " << port << std::endl;
    std::cout << "  -- hdf5 file  in   :      " << inFilename << std::endl;
    std::cout << "  -- hdf5 group in   :      " << hdf5InGroup << std::endl;
    std::cout << "  -- conf            :      " << configFile << std::endl;

    gp::SimpleProcessingClient<gp::ProcessingClient> client(
        hostName, std::stoi(port), std::chrono::milliseconds(timeoutMs), std::chrono::milliseconds(timeoutMs));
    std::thread t([&client]() { client.run(); });

    client.addSerializer(std::make_unique<gp::serializers::ConfigFileSerializer>());
    client.addSerializer(std::make_unique<gp::serializers::ConfigScriptSerializer>());
    client.addSerializer(std::make_unique<gp::serializers::HeaderSerializer>());
    client.addSerializer(std::make_unique<gp::serializers::QuerySerializer>());
    client.addSerializer(std::make_unique<gp::serializers::ImageSerializer>());
    client.addSerializer(std::make_unique<gp::serializers::AcquisitionSerializer>());
    client.addSerializer(std::make_unique<gp::serializers::WaveformSerializer>());
    client.addSerializer(std::make_unique<gp::serializers::CloseSerializer>());

    //    client.addDeserializer(std::make_unique<gp::deserializers::ResponseDeserializer>());
    //    client.addDeserializer(std::make_unique<gp::deserializers::ImageDeserializer>());
    //    client.addDeserializer(std::make_unique<gp::deserializers::AcquisitionDeserializer>());
    //    client.addDeserializer(std::make_unique<gp::deserializers::WaveformDeserializer>());
    client.addDeserializer(std::make_unique<gp::deserializers::CloseDeserializer>());

    // TODO: use client.addProcessor to add a processor which saves (to disk) images/acquisition/waveform returned
    //  from gadgetron or prints a query response to stdout

    try {
        if (vm.count("config-local")) {
            gp::ConfigScript script;
            script.assign(configXmlLocal);
            client.serialize(gp::any(std::move(script)));
        } else {
            gp::ConfigFile filename;
            filename.assign(configFile);
            client.serialize(gp::any(std::move(filename)));
        }

        if (openInputFile) {
            client.serialize(gp::any(std::move(configParams)));

            uint32_t acquisitions = 0;
            {
                mtx.lock();
                acquisitions = ismrmrdDataset->getNumberOfAcquisitions();
                mtx.unlock();
            }

            uint32_t waveforms = 0;
            {
                mtx.lock();
                waveforms = ismrmrdDataset->getNumberOfWaveforms();
                mtx.unlock();
            }

            std::cout << "Find " << acquisitions << " ismrmrd acquisitions" << std::endl;
            std::cout << "Find " << waveforms << " ismrmrd waveforms" << std::endl;

            ISMRMRD::Waveform tempWaveform;

            uint32_t i(0), j(0); // i : index over the acquisition; j : index over the waveform

            if (waveforms > 0) {
                throw std::runtime_error("Waveforms not implemented yet.");
            } else {
                for (i = 0; i < acquisitions; i++) {
                    {
                        ISMRMRD::Acquisition tempAcquisition;
                        std::lock_guard<std::mutex> scopedLock(mtx);
                        ismrmrdDataset->readAcquisition(i, tempAcquisition);
                        client.serialize(gp::any(std::move(tempAcquisition)));
                    }
                }
            }
        }
        client.serialize(gp::any(gp::CloseObj()));
        client.enableReadTimeout();
        client.resetWg();
        t.join();
    } catch (std::exception& ex) {
        std::cerr << "Error caught: " << ex.what() << std::endl;
        return -1;
    }
    return 0;
}
