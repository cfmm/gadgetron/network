#include <array>
#include <cstdio>
#include <future>
#include <iostream>
#include <thread>
#include <tuple>

// execute system command and set output with a promise
// https://stackoverflow.com/questions/52164723/how-to-execute-a-command-and-get-return-code-stdout-and-stderr-of-command-in-c
void execPromise(const char* cmd, std::promise<std::tuple<int, std::string>>&& p) {
    std::array<char, 128> buffer{};
    std::string result;

    auto pipe = popen(cmd, "r"); // get rid of shared_ptr

    if (!pipe)
        throw std::runtime_error("popen() failed!");

    while (!feof(pipe)) {
        if (fgets(buffer.data(), 128, pipe) != nullptr)
            result += buffer.data();
    }

    int rc = pclose(pipe);

    p.set_value(std::make_tuple(rc, result));
}

// run a function in a thread, kill thread if processing is longer than timeout
// https://stackoverflow.com/a/30235437
std::tuple<int, std::string> execTimeout(std::string& cmd, long timeout) {
    // run the command in a separate thread
    // https://stackoverflow.com/questions/7686939/c-simple-return-value-from-stdthread
    std::promise<std::tuple<int, std::string>> p;
    auto f = p.get_future();
    std::thread t1(execPromise, cmd.c_str(), std::move(p));

    int rc = ETIME;
    std::string stdoutStr;

    // wait for timeout seconds
    auto status = f.wait_for(std::chrono::seconds(timeout));

    if (status == std::future_status::ready) {
        if (t1.joinable()) {
            t1.join();
            tie(rc, stdoutStr) = f.get();
        }
    } else {
        // https://www.bo-yang.net/2017/11/19/cpp-kill-detached-thread
        pthread_t t1Handle = t1.native_handle();
        pthread_cancel(t1Handle);
        if (t1.joinable()) {
            t1.join();
        }
    }
    return make_tuple(rc, stdoutStr);
}
