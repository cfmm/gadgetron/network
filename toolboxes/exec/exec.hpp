//
// Created by akuurstr on 2021-06-04.
//
#include <string>
#include <tuple>

#ifndef GADGETRON_EXEC_HPP
#define GADGETRON_EXEC_HPP
std::tuple<int, std::string> execTimeout(std::string& cmd, long timeout);
#endif // GADGETRON_EXEC_HPP
