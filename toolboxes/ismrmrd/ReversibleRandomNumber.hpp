/*
 RLCG - Reversible Linear Congruential Generator
 Copyright (c) 2013 Johan Klokkhammer Helsing
 This software is provided 'as-is', without any express or implied
 warranty. In no event will the authors be held liable for any damages
 arising from the use of this software.
 Permission is granted to anyone to use this software for any purpose,
 including commercial applications, and to alter it and redistribute it
 freely, subject to the following restrictions:
 1. The origin of this software must not be misrepresented; you must not
 claim that you wrote the original software. If you use this software
 in a product, an acknowledgment in the product documentation would be
 appreciated but is not required.
 2. Altered source versions must be plainly marked as such, and must not be
 misrepresented as being the original software.
 3. This notice may not be removed or altered from any source
 distribution.
 Johan Klokkhammer Helsing
 (johanhelsing@gmail.com)
 */

// Heavily modified for use in c++11 random
// NOLINTBEGIN(readability-identifier-naming)
#ifndef NETWORK_REVERSIBLE_RANDOM_NUMBER_H
#define NETWORK_REVERSIBLE_RANDOM_NUMBER_H

#include <cstdint>
#include <limits>
#include <openssl/sha.h>

namespace rlcg {

class ReversibleHash {
  public:
    typedef size_t result_type;

    explicit ReversibleHash(result_type seed = 0)
        : m_pFunction(nullptr), m_bForward(true), m_iIndex(seed), m_aTweak("") {
        this->direction(m_bForward);
        this->tweak(seed);
    }

    result_type operator()() { return ((*this).*(m_pFunction))(); }

    void reverse() { this->direction(!m_bForward); }

    [[maybe_unused]] void discard(size_t N) {
        if (m_bForward)
            m_iIndex += N;
        else
            m_iIndex -= N;
    }

    [[nodiscard]] result_type seed() const { return m_iIndex; }

    void tweak(result_type key) { SHA1(reinterpret_cast<unsigned char*>(&key), sizeof(key), m_aTweak); }

    static constexpr result_type max() { return Max_; }

    static constexpr result_type min() { return Min_; }

  private:
    static_assert(sizeof(result_type) < SHA_DIGEST_LENGTH, "SHA digest is smaller than size_t");

    result_type forward() {
        result_type value = hash(++m_iIndex);
        return value;
    }

    result_type backward() {
        result_type value = hash(m_iIndex--);
        return value;
    }

    result_type hash(result_type index) {
        unsigned char md1[SHA_DIGEST_LENGTH];
        unsigned char md2[SHA_DIGEST_LENGTH];
        SHA1(reinterpret_cast<unsigned char*>(&index), sizeof(index), md1);
        for (size_t i = 0; i < SHA_DIGEST_LENGTH; i++)
            md1[i] ^= m_aTweak[i];
        SHA1(md1, sizeof(md1), md2);
        return *(reinterpret_cast<result_type*>(md2));
    }

    void direction(bool forward = true) {
        m_bForward = forward;
        if (m_bForward)
            m_pFunction = &ReversibleHash::forward;
        else
            m_pFunction = &ReversibleHash::backward;
    }

    result_type (ReversibleHash::*m_pFunction)();

    bool m_bForward;
    result_type m_iIndex;
    unsigned char m_aTweak[SHA_DIGEST_LENGTH];

    static constexpr result_type Max_ = std::numeric_limits<result_type>::max();
    static constexpr result_type Min_ = std::numeric_limits<result_type>::min();
};

namespace details {

template <class T> constexpr bool isPowerOfTwo(T x) { return (x & (x - 1)) == 0; }

// constexpr implementation of Euclid's algorithm
/* Based on this recursive definition from wikipedia:
 function extended_gcd(a, b)
 if b == 0
 return (1, 0)
 else
 (q, r) := divide (a, b)
 (s, t) := extended_gcd(b, r)
 return (t, s - q * t)
 This assumes a "divide" procedure exists that returns a (quotient,remainder) pair (one could alternatively put q := a
 div b, and then r = a − b * q).
 */
static constexpr uint64_t extendedEuclidY(uint64_t a, uint64_t b);

#pragma clang diagnostic push
#pragma ide diagnostic ignored "misc-no-recursion"
// NOLINTBEGIN(misc-no-recursion)
static constexpr uint64_t extendedEuclidX(uint64_t a, uint64_t b) {
    return (b == 0) ? 1 : extendedEuclidY(b, a - b * (a / b));
}

static constexpr uint64_t extendedEuclidY(uint64_t a, uint64_t b) {
    return (b == 0) ? 0 : extendedEuclidX(b, a - b * (a / b)) - (a / b) * extendedEuclidY(b, a - b * (a / b));
}
// NOLINTEND(misc-no-recursion)
#pragma clang diagnostic pop

// modulus M, multiplicand A, increment C, the least significant bits to discard D
template <uint64_t M = 1ul << 63ul, uint64_t A = 6364136223846793005, uint64_t C = 1442695040888963407, uint64_t D = 32>
class ReversibleLCG {
  public:
    typedef uint32_t result_type;

  private:
    typedef uint64_t intermediate_type;

    static_assert(isPowerOfTwo(M), "M is not a power of two as it should be");
    static_assert(D >= 32, "Must discard at least 32 bits");
    intermediate_type x;
    bool m_bForward;

    result_type (ReversibleLCG<M, A, C, D>::*m_func)();

    result_type forward() {
        // next x = (a * x + c) % m;
        x = (A * x + C) & (M - 1);
        return static_cast<result_type>(x >> D);
    }

    result_type backward() {
        // prev x = (Ainverse * (x - c)) mod m
        auto value = static_cast<result_type>(x >> D);
        x = Ainverse_ * (x - C) & (M - 1);
        return value;
    }

    void direction(bool forward = true) {
        m_bForward = forward;
        if (m_bForward)
            m_func = &ReversibleLCG<M, A, C, D>::forward;
        else
            m_func = &ReversibleLCG<M, A, C, D>::backward;

            // clang cannot compute extendedEuclidX as a constexpr
            // It is unclear why as increasing the permitted steps does not
            // alleviate the issue. g++ on the other hand handles this without
            // issue and constexpr is infinitely faster.
#ifdef __clang__
        if (!Ainverse_) {
            Ainverse_ = extendedEuclidX(A, M);
        }
    }

    intermediate_type Ainverse_;
#else
    }

    static constexpr const intermediate_type Ainverse_ = extendedEuclidX(A, M);
#endif
    static constexpr const result_type Max_ = static_cast<result_type>((M - 1) >> D);

  public:
    static constexpr result_type max() { return Max_; }

    static constexpr result_type min() { return 0; }

    explicit ReversibleLCG(result_type seed = 0, bool forward = true)
        : x(seed)
#ifdef __clang__
          ,
          Ainverse_(0)
#endif
          ,
          m_bForward(forward) {
        this->direction(m_bForward);
    }

    result_type operator()() { return ((*this).*(m_func))(); }

    [[maybe_unused]] void discard(size_t values) {
        for (size_t i = 0; i < values; i++)
            ((*this).*(m_func))();
    }

    void reverse() { this->direction(!m_bForward); }

    void seed(result_type seed) { x = seed; }

    intermediate_type seed() { return x; }
};

} // end namespace details

using ReversibleLCG = details::ReversibleLCG<>;

} // end namespace rlcg

#endif /* NETWORK_REVERSIBLE_RANDOM_NUMBER_H */
       // NOLINTEND(readability-identifier-naming)