//
//  DatasetExtension.cpp
//  network
//
//  Created by Martyn Klassen on 2015-04-22.
//  Copyright (c) 2015 CFMM. All rights reserved.
//
#include "DatasetExtension.hpp"

namespace ISMRMRD {

herr_t walkHdf5Errors(unsigned int n, H5E_error2_t const* desc, void* clientData) {
    (void)n;
    (void)clientData;
    ismrmrd_push_error(desc->file_name, static_cast<int>(desc->line), desc->func_name, ISMRMRD_HDF5ERROR, desc->desc);
    return 0;
}

int generateError(std::string const& error, bool runtime = false) {
    H5Ewalk2(H5E_DEFAULT, H5E_WALK_UPWARD, walkHdf5Errors, nullptr);
    int retValue = ISMRMRD_PUSH_ERR(ISMRMRD_FILEERROR, error.c_str());

    if (runtime)
        throw std::runtime_error(error);

    return retValue;
}

static bool linkExists(ISMRMRD_Dataset const* pIsmrmrdDataset, char const* linkPath) {
    return (H5Lexists(pIsmrmrdDataset->fileid, linkPath, H5P_DEFAULT) > 0);
}

int DatasetExt::openSpace(std::string const& name, size_t rank, hsize_t* dims, hid_t& datasetId, hid_t& dataType,
                          hid_t& memspace, hid_t& dataspaceId) {
    herr_t h5status;

    datasetId = H5Dopen(dset_.fileid, makeDataPath(name).c_str(), H5P_DEFAULT);
    if (datasetId < 0)
        return generateError("Failed to get dataset");

    dataspaceId = H5Dget_space(datasetId);
    if (dataspaceId < 0) {
        H5Dclose(datasetId);
        return generateError("Failed to get dataspace");
    }

    int dimensions = H5Sget_simple_extent_ndims(dataspaceId);
    if (dimensions < 0 || static_cast<int>(rank) != dimensions) {
        H5Sclose(dataspaceId);
        H5Dclose(datasetId);
        if (rank != dimensions)
            return generateError("Number of dimensions mismatch");
        else
            return generateError("Failed to get number of dimensions");
    }
    std::vector<hsize_t> dataDims(rank);
    dimensions = H5Sget_simple_extent_dims(dataspaceId, dataDims.data(), nullptr);
    if (dimensions < 0) {
        H5Sclose(dataspaceId);
        H5Dclose(datasetId);
        return generateError("Failed to get dimensions");
    }

    for (int i = 0; i < dimensions; i++) {
        if (dims[i] > dataDims[i]) {
            H5Sclose(dataspaceId);
            H5Dclose(datasetId);
            return ISMRMRD_PUSH_ERR(ISMRMRD_FILEERROR, "Dimension request exceeds size");
        }
    }

    memspace = H5Screate_simple(dimensions, dims, nullptr);
    if (memspace < 0) {
        H5Sclose(dataspaceId);
        H5Dclose(datasetId);
        return generateError("Failed to create memspace");
    }

    h5status = H5Sselect_all(memspace);
    if (h5status < 0) {
        H5Sclose(memspace);
        H5Sclose(dataspaceId);
        H5Dclose(datasetId);
        return generateError("Failed to select memspace");
    }

    dataType = H5Dget_type(datasetId);
    if (H5I_INVALID_HID == dataType) {
        H5Sclose(memspace);
        H5Sclose(dataspaceId);
        H5Dclose(datasetId);
        return generateError("Failed to get dataType");
    }

    return 0;
}

static int createLink(ISMRMRD_Dataset const* pIsmrmrdDataset, char const* linkPath) {
    if (nullptr == pIsmrmrdDataset) {
        return ISMRMRD_PUSH_ERR(ISMRMRD_RUNTIMEERROR, "nullptr Dataset parameter");
    }

    if (!linkExists(pIsmrmrdDataset, linkPath)) {
        hid_t linkId, gid;

        linkId = H5Pcreate(H5P_LINK_CREATE);
        if (linkId == -1)
            return generateError("Unable to create link");

        if (H5Pset_create_intermediate_group(linkId, 1) < 0) {
            H5Pclose(linkId);
            return generateError("Unable to create intermediate group");
        }

        gid = H5Gcreate2(pIsmrmrdDataset->fileid, linkPath, linkId, H5P_DEFAULT, H5P_DEFAULT);

        H5Gclose(gid);
        H5Pclose(linkId);

        if (gid < 0)
            return generateError("Unable to create and link group");
    }
    return ISMRMRD_NOERROR;
}

static hid_t getHdf5TypeXmlHeader() {
    hid_t datatype = H5Tvlen_create(H5T_C_S1);
    return datatype;
}

static hid_t getHdf5TypeImageHeader() {
    hid_t datatype;
    herr_t h5status;
    hsize_t arrayDimensions[1];
    hid_t variableType;

    datatype = H5Tcreate(H5T_COMPOUND, sizeof(ISMRMRD_ImageHeader));
    H5Tinsert(datatype, "version", HOFFSET(ISMRMRD_ImageHeader, version), H5T_NATIVE_UINT16);
    H5Tinsert(datatype, "data_type", HOFFSET(ISMRMRD_ImageHeader, data_type), H5T_NATIVE_UINT16);
    H5Tinsert(datatype, "flags", HOFFSET(ISMRMRD_ImageHeader, flags), H5T_NATIVE_UINT64);
    H5Tinsert(datatype, "measurement_uid", HOFFSET(ISMRMRD_ImageHeader, measurement_uid), H5T_NATIVE_UINT32);
    arrayDimensions[0] = 3;
    variableType = H5Tarray_create2(H5T_NATIVE_UINT16, 1, arrayDimensions);
    H5Tinsert(datatype, "matrix_size", HOFFSET(ISMRMRD_ImageHeader, matrix_size), variableType);
    H5Tclose(variableType);

    variableType = H5Tarray_create2(H5T_NATIVE_FLOAT, 1, arrayDimensions);
    H5Tinsert(datatype, "field_of_view", HOFFSET(ISMRMRD_ImageHeader, field_of_view), variableType);
    H5Tinsert(datatype, "channels", HOFFSET(ISMRMRD_ImageHeader, channels), H5T_NATIVE_UINT16);
    H5Tinsert(datatype, "position", HOFFSET(ISMRMRD_ImageHeader, position), variableType);
    H5Tinsert(datatype, "read_dir", HOFFSET(ISMRMRD_ImageHeader, read_dir), variableType);
    H5Tinsert(datatype, "phase_dir", HOFFSET(ISMRMRD_ImageHeader, phase_dir), variableType);
    H5Tinsert(datatype, "slice_dir", HOFFSET(ISMRMRD_ImageHeader, slice_dir), variableType);
    H5Tinsert(datatype, "patient_table_position", HOFFSET(ISMRMRD_ImageHeader, patient_table_position), variableType);
    H5Tclose(variableType);

    H5Tinsert(datatype, "average", HOFFSET(ISMRMRD_ImageHeader, average), H5T_NATIVE_UINT16);
    H5Tinsert(datatype, "slice", HOFFSET(ISMRMRD_ImageHeader, slice), H5T_NATIVE_UINT16);
    H5Tinsert(datatype, "contrast", HOFFSET(ISMRMRD_ImageHeader, contrast), H5T_NATIVE_UINT16);
    H5Tinsert(datatype, "phase", HOFFSET(ISMRMRD_ImageHeader, phase), H5T_NATIVE_UINT16);
    H5Tinsert(datatype, "repetition", HOFFSET(ISMRMRD_ImageHeader, repetition), H5T_NATIVE_UINT16);
    H5Tinsert(datatype, "set", HOFFSET(ISMRMRD_ImageHeader, set), H5T_NATIVE_UINT16);
    H5Tinsert(datatype, "acquisition_time_stamp", HOFFSET(ISMRMRD_ImageHeader, acquisition_time_stamp),
              H5T_NATIVE_UINT32);
    arrayDimensions[0] = ISMRMRD_PHYS_STAMPS;
    variableType = H5Tarray_create2(H5T_NATIVE_UINT32, 1, arrayDimensions);
    H5Tinsert(datatype, "physiology_time_stamp", HOFFSET(ISMRMRD_ImageHeader, physiology_time_stamp), variableType);
    H5Tclose(variableType);

    H5Tinsert(datatype, "image_type", HOFFSET(ISMRMRD_ImageHeader, image_type), H5T_NATIVE_UINT16);
    H5Tinsert(datatype, "image_index", HOFFSET(ISMRMRD_ImageHeader, image_index), H5T_NATIVE_UINT16);
    H5Tinsert(datatype, "image_series_index", HOFFSET(ISMRMRD_ImageHeader, image_series_index), H5T_NATIVE_UINT16);

    arrayDimensions[0] = ISMRMRD_USER_INTS;
    variableType = H5Tarray_create2(H5T_NATIVE_INT32, 1, arrayDimensions);
    H5Tinsert(datatype, "user_int", HOFFSET(ISMRMRD_ImageHeader, user_int), variableType);
    H5Tclose(variableType);

    arrayDimensions[0] = ISMRMRD_USER_FLOATS;
    variableType = H5Tarray_create2(H5T_NATIVE_FLOAT, 1, arrayDimensions);
    H5Tinsert(datatype, "user_float", HOFFSET(ISMRMRD_ImageHeader, user_float), variableType);
    H5Tclose(variableType);

    h5status = H5Tinsert(datatype, "attribute_string_len", HOFFSET(ISMRMRD_ImageHeader, attribute_string_len),
                         H5T_NATIVE_UINT32);

    /* Clean up */
    if (h5status < 0) {
        ISMRMRD_PUSH_ERR(ISMRMRD_FILEERROR, "Failed get image header data type");
    }

    return datatype;
}

DatasetExt::DatasetExt(char const* filename, char const* groupName, bool create)
    : Dataset(filename, groupName, create), configurationIndex(0), memberEncrypt(false) {}

[[maybe_unused]] int DatasetExt::appendConfiguration(std::string& xml) {
    if (!cipher(xml, true))
        return ISMRMRD_PUSH_ERR(ISMRMRD_FILEERROR, "Failed to encrypt configuration");

    ISMRMRD_Dataset* pIsmrmrdDataset = &dset_;

    hid_t dataset, dataspace, props, filespace, memspace, datatype;
    herr_t h5status;
    hsize_t hdfDims[] = {1}, extDims[] = {1}, offset[] = {0}, maxDims[] = {H5S_UNLIMITED}, chunkDims[] = {1};
    int rank = 1;

    /* The path to the xml string */
    std::string path(dset_.groupname);
    path.append("/config");

    datatype = getHdf5TypeXmlHeader();

    /* Check the path and find rank */
    if (linkExists(pIsmrmrdDataset, path.c_str())) {
        /* open dataset */
        dataset = H5Dopen2(pIsmrmrdDataset->fileid, path.c_str(), H5P_DEFAULT);
        dataspace = H5Dget_space(dataset);

        h5status = H5Sget_simple_extent_dims(dataspace, hdfDims, maxDims);
        if (h5status < 0)
            return generateError("Failed to get extent dimensions");

        /* extend it by one */
        hdfDims[0] += 1;
        h5status = H5Dset_extent(dataset, hdfDims);
        if (h5status < 0)
            return generateError("Failed to set extent");
    } else {
        dataspace = H5Screate_simple(rank, hdfDims, maxDims);
        props = H5Pcreate(H5P_DATASET_CREATE);
        /* enable chunking so that the dataset is extensible */
        h5status = H5Pset_chunk(props, rank, chunkDims);
        if (h5status < 0)
            return generateError("Failed to set chunk");
        /* create */
        dataset =
            H5Dcreate2(pIsmrmrdDataset->fileid, path.c_str(), datatype, dataspace, H5P_DEFAULT, props, H5P_DEFAULT);
        if (dataset < 0)
            return generateError("Failed to create dataset");
        h5status = H5Pclose(props);
        if (h5status < 0)
            return generateError("Failed to close property list");
    }

    /* Select the last block */
    offset[0] = hdfDims[0] - 1;
    filespace = H5Dget_space(dataset);
    h5status = H5Sselect_hyperslab(filespace, H5S_SELECT_SET, offset, nullptr, extDims, nullptr);
    if (h5status < 0)
        return generateError("Failed to select hyperslab");
    memspace = H5Screate_simple(rank, extDims, nullptr);

    hvl_t data;
    data.len = xml.size();
    data.p = (void*)xml.data();

    /* Write it */
    /* since this is a 1 element array we can just pass the pointer to the header */
    h5status = H5Dwrite(dataset, datatype, memspace, filespace, H5P_DEFAULT, &data);
    if (h5status < 0)
        return generateError("Failed to write dataset");

    /* Clean up */
    h5status = H5Sclose(dataspace);
    if (h5status < 0)
        return generateError("Failed to close dataspace");
    h5status = H5Sclose(filespace);
    if (h5status < 0)
        return generateError("Failed to close filespace");
    h5status = H5Sclose(memspace);
    if (h5status < 0)
        return generateError("Failed to close memspace");
    h5status = H5Dclose(dataset);
    if (h5status < 0)
        return generateError("Failed to close dataset");

    return ISMRMRD_NOERROR;
}

int DatasetExt::nextConfiguration(std::string& xml) {
    ISMRMRD_Dataset* pIsmrmrdDataset = &dset_;

    hid_t dataset, filespace, memspace, datatype;
    hsize_t hdfDims[] = {0}, offset[] = {configurationIndex}, count[] = {1};
    herr_t h5status;

    std::string path(dset_.groupname);
    path.append("/config");

    // Open the dataset
    dataset = H5Dopen2(pIsmrmrdDataset->fileid, path.c_str(), H5P_DEFAULT);

    // Check that the path was successfully opened
    if (dataset < 0)
        return ISMRMRD_HDF5ERROR;

    // Determine the rank of the items
    filespace = H5Dget_space(dataset);
    int rank = H5Sget_simple_extent_ndims(filespace);

    // Config xml strings must be rank one
    if (rank != 1)
        return ISMRMRD_HDF5ERROR;

    // Get the number of items in the vector (rank-1 array)
    h5status = H5Sget_simple_extent_dims(filespace, hdfDims, nullptr);
    if (h5status < 0)
        return generateError("Failed to get extent dimensions");

    // Make sure they are within bounds
    if (configurationIndex >= *hdfDims) {
        return ISMRMRD_HDF5ERROR;
    }

    // Read the data from the file into a temporary buffer
    hvl_t data;
    datatype = getHdf5TypeXmlHeader();
    h5status = H5Sselect_hyperslab(filespace, H5S_SELECT_SET, offset, nullptr, count, nullptr);
    if (h5status < 0)
        return generateError("Failed to select hyperslab");
    memspace = H5Screate_simple(rank, count, nullptr);
    h5status = H5Dread(dataset, datatype, memspace, filespace, H5P_DEFAULT, &data);
    if (h5status < 0)
        return generateError("Failed to read from dataset.");

    /* Unpack */
    xml.assign(static_cast<char const*>(data.p), data.len);

    // Free memory allocated for string
    H5Dvlen_reclaim(memspace, filespace, H5P_DEFAULT, &data);

    // Close up everything so that
    h5status = H5Tclose(memspace);
    if (h5status < 0)
        return generateError("Failed to close XML header HDF5 memspace.");
    h5status = H5Tclose(filespace);
    if (h5status < 0)
        return generateError("Failed to close XML header HDF5 filespace.");
    h5status = H5Tclose(datatype);
    if (h5status < 0)
        return generateError("Failed to close XML header HDF5 datatype.");
    h5status = H5Dclose(dataset);
    if (h5status < 0)
        return generateError("Failed to close XML header HDF5 dataset.");

    if (!cipher(xml, false))
        return ISMRMRD_PUSH_ERR(ISMRMRD_FILEERROR, "Failed to decrypt configuration");

    return ISMRMRD_NOERROR;
}

int DatasetExt::configuration(std::string& xml) {
    configurationIndex = 0;
    return nextConfiguration(xml);
}

#pragma clang diagnostic push
#pragma ide diagnostic ignored "OCUnusedGlobalDeclarationInspection"

void printHeader(std::ostream& os, ISMRMRD::ISMRMRD_ImageHeader& header) {
    size_t i;

    os << "version                " << header.version << "\n";
    os << "data_type              " << header.data_type << "\n";
    os << "flags                  " << header.flags << "\n";
    os << "measurement_uid        " << header.measurement_uid << "\n";
    os << "matrix_size            " << header.matrix_size[0] << ", " << header.matrix_size[1] << ", "
       << header.matrix_size[2] << "\n";
    os << "field_of_view          " << header.field_of_view[0] << ", " << header.field_of_view[1] << ", "
       << header.field_of_view[2] << "\n";
    os << "channels               " << header.channels << "\n";
    os << "position               " << header.position[0] << ", " << header.position[1] << ", " << header.position[2]
       << "\n";
    os << "slice_dir              " << header.slice_dir[0] << ", " << header.slice_dir[1] << ", " << header.slice_dir[2]
       << "\n";
    os << "patient_table_position " << header.patient_table_position[0] << ", " << header.patient_table_position[1]
       << ", " << header.patient_table_position[2];
    os << "average                " << header.average << "\n";
    os << "slice                  " << header.slice << "\n";
    os << "phase                  " << header.phase << "\n";
    os << "repetition             " << header.repetition << "\n";
    os << "set                    " << header.set << "\n";
    os << "acquisition_time_stamp " << header.acquisition_time_stamp << "\n";

    os << "physiology_time_stamp  " << header.physiology_time_stamp[0];
    for (i = 1; i < ISMRMRD_PHYS_STAMPS; i++)
        os << ", " << header.physiology_time_stamp[i];
    os << "\n";

    os << "image_type             " << header.image_type << "\n";
    os << "image_index            " << header.image_index << "\n";
    os << "image_series_index     " << header.image_series_index << "\n";

    os << "user_int               " << header.user_int[0];
    for (i = 1; i < ISMRMRD_USER_INTS; i++)
        os << ", " << header.user_int[i];
    os << "\n";

    os << "user_float             " << header.user_float[0];
    for (i = 1; i < ISMRMRD_USER_FLOATS; i++)
        os << ", " << header.user_float[i];
    os << "\n";

    os << "attribute_string_len   " << header.attribute_string_len << "\n";
}

#pragma clang diagnostic pop

uint16_t DatasetExt::datatype(std::string const& varname) const {
    ISMRMRD::ISMRMRD_ImageHeader header;
    if (this->readImageHeader(varname, 0, header))
        return header.data_type;
    else
        return 0;
}

int DatasetExt::readImageHeader(std::string const& varname, uint32_t index,
                                ISMRMRD::ISMRMRD_ImageHeader& header) const {
    ISMRMRD_Dataset const* pIsmrmrdDataset = &dset_;
    hid_t dataset, filespace, memspace, datatype;
    hsize_t hdfDims[] = {0}, offset[] = {index}, count[] = {1};
    herr_t h5status;

    std::string path(makeGroupPath(varname));
    path.append("/header");

    // Open the dataset
    dataset = H5Dopen2(pIsmrmrdDataset->fileid, path.c_str(), H5P_DEFAULT);

    // Check that the path was successfully opened
    if (dataset < 0)
        return 0;

    // Determine the rank of the items
    filespace = H5Dget_space(dataset);
    int rank = H5Sget_simple_extent_ndims(filespace);

    // Image headers must be rank one
    if (rank != 1)
        return 0;

    // Get the number of items in the vector (rank-1 array)
    h5status = H5Sget_simple_extent_dims(filespace, hdfDims, nullptr);
    if (h5status < 0) {
        generateError("Failed to get extent dimensions");
        return 0;
    }
    if (index > hdfDims[0])
        return 0;

    datatype = getHdf5TypeImageHeader();
    h5status = H5Sselect_hyperslab(filespace, H5S_SELECT_SET, offset, nullptr, count, nullptr);
    if (h5status < 0) {
        generateError("Failed to select hyperslab");
        return 0;
    }
    memspace = H5Screate_simple(rank, count, nullptr);
    h5status = H5Dread(dataset, datatype, memspace, filespace, H5P_DEFAULT, &header);
    if (h5status < 0) {
        generateError("Failed to read from dataset.");
        return 0;
    }

    // Close up everything so that
    h5status = H5Tclose(datatype);
    if (h5status < 0) {
        generateError("Failed to close Image header HDF5 datatype.");
        return 0;
    }
    h5status = H5Dclose(dataset);
    if (h5status < 0) {
        generateError("Failed to close Image header HDF5 dataset.");
        return 0;
    }
    H5Tclose(filespace);
    H5Tclose(memspace);
#if 0
      // There is an issue with HDF5 that causes closes to filespace and memspace to not
      return a zero status
      h5status = H5Tclose(filespace);
      if (h5status < 0) {
         generate_error("Failed to close Image header HDF5 filespace.");
         return 0;
      }
      h5status = H5Tclose(memspace);
      if (h5status < 0) {
         generate_error("Failed to close Image header HDF5 memspace.");
         return 0;
      }
#endif

    return 1;
}

hid_t DatasetExt::convertTypeIsmrmrdToHdf(ISMRMRD_DataTypes dataType) {
    hid_t datatype = -1;
    herr_t h5status;

    switch (dataType) {
    case ISMRMRD::ISMRMRD_USHORT:
        datatype = H5Tcopy(H5T_NATIVE_UINT16);
        break;
    case ISMRMRD::ISMRMRD_SHORT:
        datatype = H5Tcopy(H5T_NATIVE_INT16);
        break;
    case ISMRMRD::ISMRMRD_UINT:
        datatype = H5Tcopy(H5T_NATIVE_UINT32);
        break;
    case ISMRMRD::ISMRMRD_INT:
        datatype = H5Tcopy(H5T_NATIVE_INT32);
        break;
    case ISMRMRD::ISMRMRD_FLOAT:
        datatype = H5Tcopy(H5T_NATIVE_FLOAT);
        break;
    case ISMRMRD::ISMRMRD_DOUBLE:
        datatype = H5Tcopy(H5T_NATIVE_DOUBLE);
        break;
    case ISMRMRD::ISMRMRD_CXFLOAT:
        datatype = H5Tcreate(H5T_COMPOUND, sizeof(complex_float_t));
        h5status = H5Tinsert(datatype, "real", 0, H5T_NATIVE_FLOAT);
        if (h5status < 0) {
            generateError("Failed insert real complex float data type");
            H5Tclose(datatype);
            datatype = -1;
        }
        h5status = H5Tinsert(datatype, "imag", sizeof(float), H5T_NATIVE_FLOAT);
        if (h5status < 0) {
            generateError("Failed insert imag complex float data type");
            H5Tclose(datatype);
            datatype = -1;
        }
        break;
    case ISMRMRD::ISMRMRD_CXDOUBLE:
        datatype = H5Tcreate(H5T_COMPOUND, sizeof(complex_double_t));
        h5status = H5Tinsert(datatype, "real", 0, H5T_NATIVE_DOUBLE);
        if (h5status < 0) {
            generateError("Failed insert real complex double data type");
            H5Tclose(datatype);
            datatype = -1;
        }
        h5status = H5Tinsert(datatype, "imag", sizeof(double), H5T_NATIVE_DOUBLE);
        if (h5status < 0) {
            generateError("Failed insert imag complex double data type");
            H5Tclose(datatype);
            datatype = -1;
        }
        break;
    }

    if (datatype == -1)
        ISMRMRD_PUSH_ERR(ISMRMRD_TYPEERROR, "Failed to get HDF5 data type.");

    return datatype;
}

void DatasetExt::ensureExists(std::string const& varname) { createLink(&dset_, varname.c_str()); }

int DatasetExt::allocate(std::string const& varname, size_t dimensions, hsize_t* dims,
                         ISMRMRD::ISMRMRD_DataTypes dataType) {
    ISMRMRD_Dataset* pIsmrmrdDataset = &dset_;

    hid_t spaceId, topGroupId, middleGroupId, dataset;
    herr_t h5status;

    hid_t datatype = convertTypeIsmrmrdToHdf(dataType);

    if (datatype < 0)
        return ISMRMRD_PUSH_ERR(ISMRMRD_HDF5ERROR, "Unable to covert data type");

    std::string datasetPath = makeDataPath(varname);

    if (!linkExists(pIsmrmrdDataset, datasetPath.c_str())) {
        spaceId = H5Screate(H5S_SIMPLE);
        if (spaceId < 0) {
            return generateError("Failed to create spaceId");
        }

        h5status = H5Sset_extent_simple(spaceId, static_cast<int>(dimensions), dims, dims);
        if (h5status < 0) {
            H5Sclose(spaceId);
            return generateError("Failed to extend spaceId");
        }

        ensureExists(pIsmrmrdDataset->groupname);
        topGroupId = H5Gopen(pIsmrmrdDataset->fileid, pIsmrmrdDataset->groupname,
                             H5P_DEFAULT); // this group should be created in Dataset constructor

        if (topGroupId < 0) {
            H5Sclose(spaceId);
            return generateError("Failed to get topGroupId");
        }

        ensureExists(makeGroupPath(varname));
        middleGroupId = H5Gopen(topGroupId, varname.c_str(),
                                H5P_DEFAULT); // this group would otherwise be created by first append

        if (middleGroupId < 0) {
            H5Gclose(topGroupId);
            H5Sclose(spaceId);
            return generateError("Failed to get middleGroupId");
        }

        dataset = H5Dcreate(middleGroupId, "data", datatype, spaceId, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);

        H5Gclose(middleGroupId);
        H5Gclose(topGroupId);
        H5Sclose(spaceId);
    } else {
        return ISMRMRD_PUSH_ERR(ISMRMRD_HDF5ERROR, "Dataset already exists in file.");
    }

    // Check that the path was successfully opened
    if (dataset < 0)
        return ISMRMRD_PUSH_ERR(ISMRMRD_HDF5ERROR, "Could not open dataset");

    if (H5Dclose(dataset) < 0)
        generateError("Failed to close dataset");
    if (H5Tclose(datatype) < 0)
        generateError("Failed to close datatype");

    return ISMRMRD_NOERROR;
}

// equivalent to make_path() in ismrmrd, but simpler to use
std::string DatasetExt::makeGroupPath(std::string const& var) const {
    std::string groupPath = "/";
    groupPath.append(dset_.groupname);
    groupPath.append("/");
    groupPath.append(var);
    return groupPath;
}

std::string DatasetExt::makeDataPath(std::string const& var) {
    std::string dataPath = makeGroupPath(var);
    ensureExists(dataPath);
    dataPath.append("/data");
    return dataPath;
}

bool DatasetExt::getDimensions(std::string const& var, std::vector<hsize_t>& dims) {
    ensureExists(makeGroupPath(var));
    hid_t datasetId = H5Dopen(dset_.fileid, makeDataPath(var).c_str(), H5P_DEFAULT);

    if (datasetId < 0) {
        generateError("Failed to get dataset");
        return false;
    }

    hid_t dataspaceId = H5Dget_space(datasetId);

    if (dataspaceId < 0) {
        generateError("Failed to get dataset");
        return false;
    }

    if (H5Sselect_all(dataspaceId) < 0) {
        H5Dclose(datasetId);
        generateError("Failed to select dataspace");
        return false;
    }

    int dimensions = H5Sget_simple_extent_ndims(dataspaceId);

    if (dimensions >= 0) {
        // Get the dimensions on success
        dims.resize(static_cast<size_t>(dimensions));
        dimensions = H5Sget_simple_extent_dims(dataspaceId, dims.data(), nullptr);
    }

    // Close the opened items
    if (H5Sclose(dataspaceId) < 0)
        generateError("Failed to close dataspace");
    if (H5Dclose(datasetId) < 0)
        generateError("Failed to close dataset");

    if (dimensions < 0) {
        generateError("Failed to get dimensions");
        return false;
    }

    return true;
}

void DatasetExt::writeHeader(std::string& xmlstring) {
    if (!cipher(xmlstring, true))
        throw std::runtime_error("Failed Header encryption");
    Dataset::writeHeader(xmlstring);
}

#pragma clang diagnostic push
#pragma ide diagnostic ignored "HidingNonVirtualFunction"

void DatasetExt::readHeader(std::string& xmlstring) {
    if (!linkExists(&dset_, makeGroupPath("xml").c_str())) {
        xmlstring = "";
        return;
    }

    Dataset::readHeader(xmlstring);
    if (!cipher(xmlstring, false))
        throw std::runtime_error("Failed Header decryption");
}

#pragma clang diagnostic pop

void DatasetExt::encryption(std::string const& passphrase, std::string const& authentication) {
    memberPassphrase = passphrase;
    memberAuthentication = authentication;
    ISMRMRD::DatasetExt::encryption(!passphrase.empty());
}

void DatasetExt::encryption(bool encrypt) { memberEncrypt = encrypt; }

bool DatasetExt::cipher(std::string& msg, bool encrypt) {
    if (memberEncrypt && msg.empty()) {
        return Gadgetron::Network::Cryptography::cipher(encrypt, memberPassphrase, memberAuthentication, msg);
    }
    return true;
}

} // namespace ISMRMRD
