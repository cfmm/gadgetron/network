//
//  DatasetExtension.h
//  network
//
//  Created by Martyn Klassen on 2015-04-22.
//  Copyright (c) 2015 CFMM. All rights reserved.
//

#ifndef NETWORK_DATASET_EXTENSION_H
#define NETWORK_DATASET_EXTENSION_H

#include "Cryptography.hpp"

#include <functional>
#include <ismrmrd/dataset.h>
#include <ismrmrd/ismrmrd.h>
#include <ismrmrd/meta.h>

namespace ISMRMRD {

herr_t walkHdf5Errors(unsigned int n, H5E_error2_t const* desc, void* clientData);

class DatasetExt : public ISMRMRD::Dataset {
    // Be careful because ISMRMRD::Dataset does not declare any methods as virtual
    // so overloaded methods are NOT used in parent methods

  public:
    DatasetExt(char const* filename, char const* groupName, bool create = true);
    DatasetExt(DatasetExt const&) = delete;
    DatasetExt& operator=(const DatasetExt&) = delete;
    DatasetExt(DatasetExt&& other) = delete;

    [[maybe_unused]] int appendConfiguration(std::string& xml);

    int nextConfiguration(std::string& xml);

    int configuration(std::string& xml);

    [[nodiscard]] uint16_t datatype(std::string const& varname) const;

    bool getDimensions(std::string const& var, std::vector<hsize_t>&);

    [[nodiscard]] std::string makeGroupPath(std::string const& var) const;

    std::string makeDataPath(std::string const& var);

    void writeHeader(std::string& xmlstring);

#pragma clang diagnostic push
#pragma ide diagnostic ignored "HidingNonVirtualFunction"
    // Ismrmrd choice to make a bunch of functions non-virtual that really should have been virtual

    void readHeader(std::string& xmlstring);

    template <typename T> void appendImage(std::string const& var, Image<T>& image);

    template <typename T> void readImage(std::string const& var, uint32_t index, Image<T>& image);

    template <typename T> [[maybe_unused]] void appendNDArrayData(std::string const& var, ISMRMRD::NDArray<T>& arr);

    // Should be encapsulate for encryption
    // Acquisitions
    [[maybe_unused]] void appendAcquisition(const Acquisition& acq) {
        assertNoEncrypt();
        ISMRMRD::Dataset::appendAcquisition(acq);
    }

    [[maybe_unused]] void readAcquisition(uint32_t index, Acquisition& acq) {
        assertNoEncrypt();
        ISMRMRD::Dataset::readAcquisition(index, acq);
    }

    // Images
    [[maybe_unused]] void appendImage(std::string const& var, ISMRMRD_Image const* im) {
        assertNoEncrypt();
        ISMRMRD::Dataset::appendImage(var, im);
    }

    // NDArrays
    void appendNDArray(std::string const& var, ISMRMRD_NDArray const* arr) {
        assertNoEncrypt();
        ISMRMRD::Dataset::appendNDArray(var, arr);
    }

    template <typename T> void appendNDArray(std::string const& var, ISMRMRD::NDArray<T>& arr) {
        assertNoEncrypt();
        ISMRMRD::Dataset::appendNDArray(var, arr);
    }

    template <typename T> [[maybe_unused]] void readNDArray(std::string const& var, uint32_t index, NDArray<T>& arr) {
        assertNoEncrypt();
        ISMRMRD::Dataset::readNDArray(var, index, arr);
    }

#pragma clang diagnostic pop

    int readImageHeader(std::string const& varname, uint32_t index, ISMRMRD::ISMRMRD_ImageHeader& header) const;

    template <typename T>
    [[maybe_unused]] void strideWrite(std::string const& var, T* src, size_t dimensions, hsize_t* count,
                                      hsize_t* stride, hsize_t* start);

    template <typename T>
    void strideRead(std::string const& var, T* dst, size_t dimensions, hsize_t* count, hsize_t* stride, hsize_t* start);

    int allocate(std::string const& varname, size_t dimensions, hsize_t* dims, ISMRMRD::ISMRMRD_DataTypes dataType);

    void ensureExists(std::string const& varname);

    void encryption(std::string const& passphrase, std::string const& authentication);

    void encryption(bool encrypt);

    [[maybe_unused]] [[nodiscard]] bool encryption() const { return memberEncrypt; }

    [[maybe_unused]] [[nodiscard]] std::string getPath() const { return {dset_.filename}; }

    herr_t flush() { return H5Fflush(dset_.fileid, H5F_SCOPE_GLOBAL); }

  protected:
    static hid_t convertTypeIsmrmrdToHdf(ISMRMRD_DataTypes dataType);

    int openSpace(std::string const& name, size_t rank, hsize_t* dims, hid_t& datasetId, hid_t& dataType,
                  hid_t& memspace, hid_t& dataspaceId);

  private:
    void assertNoEncrypt() const {
        if (memberEncrypt)
            throw std::runtime_error("Encryption enabled. Method not supported");
    }

    bool cipher(std::string& msg, bool encrypt);

    size_t configurationIndex;
    bool memberEncrypt;
    std::string memberPassphrase;
    std::string memberAuthentication;
};

template <typename T> void DatasetExt::appendImage(std::string const& var, Image<T>& image) {
    if (memberEncrypt) {
        // Shuffle the data and collect the salt used with the memberPassphrase
        unsigned char salt[PKCS5_SALT_LEN];
        Gadgetron::Network::Cryptography::shuffle(image.getDataPtr(), image.getNumberOfDataElements(), memberPassphrase,
                                                  salt);
        ISMRMRD::MetaContainer meta;

        std::string attributes;
        image.getAttributeString(attributes);
        if (image.getAttributeStringLength() > 0 && !attributes.empty())
            deserialize(attributes.c_str(), meta);

        // Store the salt
        std::function<void(ISMRMRD::MetaContainer&, char const*, unsigned char*, size_t)> setArray =
            [&](ISMRMRD::MetaContainer& meta, char const* name, unsigned char* value, size_t length) {
                meta.set(name, static_cast<long>(value[0]));
                for (size_t i = 1; i < length; i++)
                    meta.append(name, static_cast<long>(value[i]));
            };

        setArray(meta, "encrypt_salt", salt, sizeof(salt));

        // Store the attribute string encrypted
        std::stringstream sstream;
        ISMRMRD::serialize(meta, sstream);
        attributes = sstream.str();
        if (!cipher(attributes, true))
            throw std::runtime_error("Failed to encrypt attribute string");
        image.setAttributeString(attributes);
    }

    Dataset::appendImage(var, image);
}

#pragma clang diagnostic push
#pragma ide diagnostic ignored "HidingNonVirtualFunction"

template <typename T> void DatasetExt::readImage(std::string const& var, uint32_t index, Image<T>& image) {
    Dataset::readImage(var, index, image);

    const ISMRMRD::ImageHeader& header = image.getHead();

    std::string attributes;
    image.getAttributeString(attributes);
    if (memberEncrypt && header.attribute_string_len > 0 && !attributes.empty()) {
        if (!cipher(attributes, false))
            throw std::runtime_error("Failed to decrypt attribute string");
        image.setAttributeString(attributes);

        MetaContainer meta;
        ISMRMRD::deserialize(attributes.c_str(), meta);

        std::function<bool(ISMRMRD::MetaContainer&, char const*, unsigned char*, size_t)> getArray =
            [&](ISMRMRD::MetaContainer& meta, char const* name, unsigned char* value, size_t length) {
                if (length != meta.length(name))
                    return false;

                for (size_t i = 0; i < length; i++)
                    value[i] = static_cast<unsigned char>(meta.as_long(name, i));

                meta.set(name, 0L);
                return true;
            };

        unsigned char salt[PKCS5_SALT_LEN];
        if (getArray(meta, "encrypt_salt", salt, sizeof(salt)))
            Gadgetron::Network::Cryptography::deshuffle(image.getDataPtr(), image.getNumberOfDataElements(),
                                                        memberPassphrase, salt);
    }
}

#pragma clang diagnostic pop

template <typename T>
[[maybe_unused]] void DatasetExt::appendNDArrayData(std::string const& var, ISMRMRD::NDArray<T>& arr) {
    std::string path = makeDataPath(var);
    appendNDArray(path.c_str(), arr);
}

template <typename T>
[[maybe_unused]] void DatasetExt::strideWrite(std::string const& var, T* src, size_t dimensions, hsize_t* count,
                                              hsize_t* stride, hsize_t* start) {
    herr_t h5status;

    hid_t dataType;
    hid_t datasetId;
    hid_t dataspaceId;
    hid_t memspace;
    if (openSpace(var, dimensions, count, datasetId, dataType, memspace, dataspaceId)) {
        throw std::runtime_error(build_exception_string());
    }

    h5status = H5Sselect_hyperslab(dataspaceId, H5S_SELECT_SET, start, stride, count, nullptr);
    if (h5status < 0) {
        H5Ewalk2(H5E_DEFAULT, H5E_WALK_UPWARD, walkHdf5Errors, nullptr);
        ISMRMRD_PUSH_ERR(ISMRMRD_FILEERROR, "Failed to get hyperslab");
    } else {
        h5status = H5Dwrite(datasetId, dataType, memspace, dataspaceId, H5P_DEFAULT, src);

        if (h5status < 0) {
            H5Ewalk2(H5E_DEFAULT, H5E_WALK_UPWARD, walkHdf5Errors, nullptr);
            ISMRMRD_PUSH_ERR(ISMRMRD_FILEERROR, "Failed to get hyperslab");
        }
    }

    H5Tclose(dataType);
    H5Sclose(memspace);
    H5Sclose(dataspaceId);
    H5Dclose(datasetId);

    if (h5status < 0) {
        throw std::runtime_error(build_exception_string());
    }
}

template <typename T>
void DatasetExt::strideRead(std::string const& var, T* dst, size_t dimensions, hsize_t* count, hsize_t* stride,
                            hsize_t* start) {
    herr_t h5status;

    hid_t dataType;
    hid_t datasetId;
    hid_t dataspaceId;
    hid_t memspace;
    if (openSpace(var, dimensions, count, datasetId, dataType, memspace, dataspaceId)) {
        throw std::runtime_error(build_exception_string());
    }

    h5status = H5Sselect_hyperslab(dataspaceId, H5S_SELECT_SET, start, stride, count, nullptr);
    if (h5status < 0) {
        H5Ewalk2(H5E_DEFAULT, H5E_WALK_UPWARD, walkHdf5Errors, nullptr);
        ISMRMRD_PUSH_ERR(ISMRMRD_FILEERROR, "Failed to get hyperslab");
    } else {
        h5status = H5Dread(datasetId, dataType, memspace, dataspaceId, H5P_DEFAULT, dst);

        if (h5status < 0) {
            H5Ewalk2(H5E_DEFAULT, H5E_WALK_UPWARD, walkHdf5Errors, nullptr);
            ISMRMRD_PUSH_ERR(ISMRMRD_FILEERROR, "Failed to get hyperslab");
        }
    }

    H5Tclose(dataType);
    H5Sclose(memspace);
    H5Sclose(dataspaceId);
    H5Dclose(datasetId);

    if (h5status < 0) {
        throw std::runtime_error(build_exception_string());
    }
}

} /* namespace ISMRMRD */

#endif /* defined(NETWORK_DATASET_EXTENSION_H) */
