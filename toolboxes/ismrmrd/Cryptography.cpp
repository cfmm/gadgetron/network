//
//  CryptographySingleton.cpp
//  network
//
//  Created by Martyn Klassen on 2015-10-09.
//  Copyright © 2015 CFMM. All rights reserved.
//

#include "Cryptography.hpp"

#if OPENSSL_VERSION_NUMBER < 0x30000000L
#error("OPENSSL 3.x is required")
#endif

#include <boost/archive/iterators/base64_from_binary.hpp>
#include <boost/archive/iterators/binary_from_base64.hpp>
#include <boost/archive/iterators/insert_linebreaks.hpp>
#include <boost/archive/iterators/remove_whitespace.hpp>
#include <boost/archive/iterators/transform_width.hpp>
#include <log.h>
#include <memory>

namespace Gadgetron // NOLINT(modernize-concat-nested-namespaces)
{
namespace Network {
namespace Cryptography {

using EVP_CIPHER_CTX_free_ptr = std::unique_ptr<EVP_CIPHER_CTX, decltype(&::EVP_CIPHER_CTX_free)>;

bool cipher(bool encrypt, const std::string& passphrase, const std::string& aad, unsigned char* salt,
            unsigned char* tag, unsigned char* src, std::size_t srcLen) {
    static CRYPTO_ONCE once = CRYPTO_ONCE_STATIC_INIT;
    static CRYPTO_RWLOCK* lock(nullptr);

    // TODO This lock is lost and never deallocated. It should probably use a std::unique like
    // EVP_CIPHER_CTX_free_ptr
    if (!CRYPTO_THREAD_run_once(&once, []() { lock = CRYPTO_THREAD_lock_new(); }) || lock == nullptr) {
        GERROR("Failed to obtain cryptographic lock\n");
        return false;
    }

    int result;
    if (encrypt)
        result = CRYPTO_THREAD_write_lock(lock);
    else
        result = CRYPTO_THREAD_read_lock(lock);

    if (!result)
        return false;

    int ivLength = 12;
    int len;

    EVP_CIPHER_CTX_free_ptr ctx(EVP_CIPHER_CTX_new(), ::EVP_CIPHER_CTX_free);

    // Salt and tag must not be nullptr
    if (!salt || !tag ||
        // Create the salt value if required
        (encrypt && (RAND_bytes(salt, PKCS5_SALT_LEN) <= 0))) {
        GERROR("Failed to initialize cryptography context\n");
        CRYPTO_THREAD_unlock(lock);
        return false;
    }

    // Set the cipher and digest to be used
    const EVP_CIPHER* cipher = EVP_aes_256_gcm();
    const EVP_MD* digest = EVP_md5();

    // Allocate space for key, iv, and salt
    unsigned char key[EVP_MAX_KEY_LENGTH];
    unsigned char iv[EVP_MAX_IV_LENGTH];

    bool returnValue(false);

    // Generate a key and iv
    if (0 == EVP_BytesToKey(cipher, digest, salt, reinterpret_cast<const unsigned char*>(passphrase.c_str()),
                            static_cast<int>(passphrase.length()), 1, key, iv)) {
        GERROR("Failed to create key and iv\n");
        goto CleanUp;
    }

    // Initialize the cipher
    if (1 != EVP_CipherInit_ex(ctx.get(), cipher, nullptr, nullptr, nullptr, encrypt)) {
        GERROR("Failed to initialize cipher\n");
        goto CleanUp;
    }

    // Set the iv length
    if (1 != EVP_CIPHER_CTX_ctrl(ctx.get(), EVP_CTRL_GCM_SET_IVLEN, ivLength, nullptr)) {
        GERROR("Failed to set iv length\n");
        goto CleanUp;
    }

    if (!encrypt && (1 != EVP_CIPHER_CTX_ctrl(ctx.get(), EVP_CTRL_GCM_SET_TAG, TAG_LENGTH, tag))) {
        GERROR("Failed to set decryption tag\n");
        goto CleanUp;
    }

    // Set the key and iv
    if (1 != EVP_CipherInit_ex(ctx.get(), nullptr, nullptr, key, iv, encrypt)) {
        GERROR("Failed to set key and iv\n");
        goto CleanUp;
    }

    // Add the authentication data
    if (1 != EVP_CipherUpdate(ctx.get(), nullptr, &len, reinterpret_cast<const unsigned char*>(aad.c_str()),
                              static_cast<int>(aad.length()))) {
        GERROR("Failed to set authentication data\n");
        goto CleanUp;
    }

    if (1 != EVP_CipherUpdate(ctx.get(), src, &len, src, static_cast<int>(srcLen))) {
        GERROR("Failed to apply cipher\n");
        goto CleanUp;
    }

    if (len != srcLen) {
        GERROR("Cipher text output length does not match input length\n");
        goto CleanUp;
    }

    if ((1 != EVP_CipherFinal_ex(ctx.get(), src, &len)) || (len != 0)) {
        GERROR("Failed to finalize cipher context\n");
        goto CleanUp;
    }

    if (encrypt && (1 != EVP_CIPHER_CTX_ctrl(ctx.get(), EVP_CTRL_GCM_GET_TAG, TAG_LENGTH, tag))) {
        GERROR("Failed to get encryption tag\n");
        goto CleanUp;
    }

    returnValue = true;
CleanUp:
    flush(std::cout);
    CRYPTO_THREAD_unlock(lock);
    return returnValue;
}

bool cipher(bool encrypt, const std::string& passphrase, const std::string& aad, std::string& msg) {
    using namespace boost::archive::iterators;

    typedef transform_width<binary_from_base64<remove_whitespace<std::string::const_iterator>>, 8, 6> ItBinaryType;
    typedef insert_linebreaks<base64_from_binary<transform_width<std::string::const_iterator, 6, 8>>, 72> ItBase64Type;

    if (!encrypt) {
        // base64 decode
        std::string::const_iterator::difference_type padChars = count(msg.begin(), msg.end(), '=');
        std::replace(msg.begin(), msg.end(), '=', 'A'); // replace '=' by base64 encoding of '\0'
        std::string result(ItBinaryType(msg.begin()), ItBinaryType(msg.end())); // decode
        result.erase(result.end() - padChars, result.end());                    // erase padding '\0' characters
        msg = result;
    }

    size_t extra = PKCS5_SALT_LEN + TAG_LENGTH;
    if (encrypt) {
        msg.append(extra, '\0');
    }
    auto src = reinterpret_cast<unsigned char*>(&(msg.front()));
    size_t srcLen = msg.length() - extra;
    unsigned char* salt = src + srcLen;
    unsigned char* tag = salt + PKCS5_SALT_LEN;

    if (cipher(encrypt, passphrase, aad, salt, tag, src, srcLen)) {
        if (encrypt) {
            // base64 encode
            auto writePadChars = static_cast<std::string::const_iterator::difference_type>((3 - msg.length() % 3) % 3);
            std::string base64(ItBase64Type(msg.begin()), ItBase64Type(msg.end()));
            msg = base64;
            msg.append(static_cast<size_t>(writePadChars), '=');
        } else {
            msg.erase(srcLen);
        }
        return true;
    }
    return false;
}

bool encrypt(const std::string& passphrase, const std::string& aad, std::string& msg) {
    return Cryptography::cipher(true, passphrase, aad, msg);
}

bool decrypt(const std::string& passphrase, const std::string& aad, std::string& msg) {
    return Cryptography::cipher(false, passphrase, aad, msg);
}

} // namespace Cryptography
} // namespace Network
} // namespace Gadgetron
