//
//  CryptographySingleton.hpp
//  network
//
//  Created by Martyn Klassen on 2015-10-09.
//  Copyright © 2015 CFMM. All rights reserved.
//

#pragma once
#ifndef NETWORK_CRYPTOGRAPHY_H
#define NETWORK_CRYPTOGRAPHY_H

#include "ReversibleRandomNumber.hpp"
#include <cassert>
#include <cstring>
#include <iostream>
#include <openssl/bio.h>
#include <openssl/evp.h>
#include <openssl/rand.h>
#include <openssl/sha.h>
#include <random>
#include <stdexcept>

#define TAG_LENGTH 16

namespace Gadgetron::Network::Cryptography { // NOLINT(*-identifier-naming)

bool cipher(bool encrypt, const std::string& passphrase, const std::string& aad, std::string& msg);

bool encrypt(const std::string& passphrase, const std::string& aad, std::string& msg);

bool decrypt(const std::string& passphrase, const std::string& aad, std::string& msg);

template <typename T> T genKey(std::string const& passphrase, unsigned char* salt, bool newSalt) {
    if (newSalt) {
        if (RAND_bytes(salt, PKCS5_SALT_LEN) <= 0)
            throw std::runtime_error("Unable to generate salt");
    }
    size_t len(PKCS5_SALT_LEN + passphrase.length());
    unsigned char salted[len];
    unsigned char md[SHA_DIGEST_LENGTH];
    memcpy(salted, salt, PKCS5_SALT_LEN);
    memcpy(salted + PKCS5_SALT_LEN, passphrase.data(), passphrase.length());
    SHA1(salted, len, md);
    static_assert(sizeof(T) < SHA_DIGEST_LENGTH, "SHA digest is smaller than unsigned int");
    return *(reinterpret_cast<T*>(md));
}

template <typename T> void shuffle(T* data, size_t n, const std::string& passphrase, unsigned char* salt) {

    // Need at least two voxels to shuffle
    if (n < 2)
        return;

    typedef rlcg::ReversibleHash RrngType;
    typedef std::uniform_int_distribution<size_t> DistributionType;
    DistributionType::param_type params(0, n - 1);
    DistributionType d;
    auto seed = genKey<RrngType::result_type>(passphrase, salt, true);
    RrngType g(seed);

    T* iterData = data + n;
    for (size_t i = n - 1; i > 0; i--) {
        std::swap(*(--iterData), data[d(g, params)]);
    }
}

template <typename T> void deshuffle(T* data, size_t n, std::string const& passphrase, unsigned char* salt) {

    // Need at least two voxels to shuffle
    if (n < 2)
        return;

    typedef rlcg::ReversibleHash RrngType;
    typedef std::uniform_int_distribution<size_t> DistributionType;
    DistributionType::param_type params(0, n - 1);
    DistributionType d;
    auto seed = genKey<RrngType::result_type>(passphrase, salt, false);
    RrngType g(seed);

    // uniform_int_distribution may call the generator multiple times to get a value within the specified range
    // you have to run the exact same sequence forward and then backward
    // Using discard() does not work and you must always use the same parameters
    for (size_t i = n - 1; i > 0; i--)
        d(g, params);

    g.reverse();

    T* iterData = data;
    for (size_t i = 1; i < n; i++) {
        std::swap(*(++iterData), data[d(g, params)]);
    }
}

} // namespace Gadgetron::Network::Cryptography

#endif /* NETWORK_CRYPTOGRAPHY_H */
