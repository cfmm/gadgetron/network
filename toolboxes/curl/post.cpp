//
// Created by akuurstr on 2021-01-26.
//
#include <algorithm>
#include <curl/curl.h>
#include <initializer_list>
#include <iostream>
#include <log.h>
#include <sstream>
#include <string>
#include <tuple>
#include <vector>

CURLcode checkCurlCode(CURLcode& code, std::string& option, std::string& error) {
    if (code != CURLE_OK) {
        std::stringstream ss;
        ss << "Failed to set " << option << std::endl << error << std::endl;
        // c_str() pointer only lasts as long as string object
        std::string s = ss.str();
        GERROR("%s", s.c_str());
    }
    return code;
}

size_t writeCallback(char* data, size_t size, size_t bytes, std::string* writerData) {
    writerData->append(data, size * bytes);
    return size * bytes;
}

std::tuple<long, std::string, int> cPostData(std::string const& url, std::vector<std::string> const& headers,
                                             std::string const& body, long const timeout) {
    // CURL *curl;
    CURLcode code;
    curl_global_init(CURL_GLOBAL_ALL);
    std::string option;
    std::string error;

    CURL* easyHandle = curl_easy_init();
    if (easyHandle == nullptr) {
        GERROR("Failed to create CURL easy handle\n");
        return std::make_tuple(EXIT_FAILURE, "", code);
    }

    static char errorBuffer[CURL_ERROR_SIZE];
    code = curl_easy_setopt(easyHandle, CURLOPT_ERRORBUFFER, errorBuffer);
    option = "ERRORBUFFER";
    error = std::to_string(code);
    if (checkCurlCode(code, option, error) != CURLE_OK) {
        return std::make_tuple(EXIT_FAILURE, "", code);
    }

    // setup response
    std::string readBuffer;
    code = curl_easy_setopt(easyHandle, CURLOPT_WRITEFUNCTION, writeCallback);
    option = "WRITEFUNCTION";
    error = errorBuffer;
    if (checkCurlCode(code, option, error) != CURLE_OK) {
        return std::make_tuple(EXIT_FAILURE, "", code);
    }

    code = curl_easy_setopt(easyHandle, CURLOPT_WRITEDATA, &readBuffer);
    option = "WRITEDATA";
    error = errorBuffer;
    if (checkCurlCode(code, option, error) != CURLE_OK) {
        return std::make_tuple(EXIT_FAILURE, "", code);
    }

    // URL
    code = curl_easy_setopt(easyHandle, CURLOPT_URL, url.c_str());
    option = "URL";
    error = errorBuffer;
    if (checkCurlCode(code, option, error) != CURLE_OK) {
        return std::make_tuple(EXIT_FAILURE, "", code);
    }

    // http header
    struct curl_slist* header = nullptr;
    for (const std::string& headerPart : headers) {
        header = curl_slist_append(header, headerPart.c_str());
    }
    code = curl_easy_setopt(easyHandle, CURLOPT_HTTPHEADER, header);
    option = "HTTPHEADER";
    error = errorBuffer;
    if (checkCurlCode(code, option, error) != CURLE_OK) {
        return std::make_tuple(EXIT_FAILURE, "", code);
    }

    // http POST body
    // not complicated enough for a json library, using stringstream
    code = curl_easy_setopt(easyHandle, CURLOPT_POSTFIELDS, body.c_str());
    option = "POSTFIELDS";
    error = errorBuffer;
    if (checkCurlCode(code, option, error) != CURLE_OK) {
        return std::make_tuple(EXIT_FAILURE, "", code);
    }

    // http timeout
    code = curl_easy_setopt(easyHandle, CURLOPT_TIMEOUT, timeout);
    option = "TIMEOUT";
    error = errorBuffer;
    if (checkCurlCode(code, option, error) != CURLE_OK) {
        return std::make_tuple(EXIT_FAILURE, "", code);
    }

    // send POST
    // code = curl_easy_setopt(easyHandle, CURLOPT_VERBOSE, 1L);
    // checkCurlCode(code, "VERBOSE", errorBuffer);
    code = curl_easy_perform(easyHandle);
    if (code != CURLE_OK) {
        GERROR("POST failed! %s\n", errorBuffer);
        option = "";
        return std::make_tuple(EXIT_FAILURE, option, code);
    }

    long responseCode;
    code = curl_easy_getinfo(easyHandle, CURLINFO_RESPONSE_CODE, &responseCode);
    if (code != CURLE_OK) {
        GERROR("Failed to setup RESPONSE_CODE! %s\n", errorBuffer);
        option = "";
        return std::make_tuple(EXIT_FAILURE, option, code);
    }

    // clean up
    curl_slist_free_all(header);
    curl_easy_cleanup(easyHandle);
    curl_global_cleanup();
    return std::make_tuple(responseCode, readBuffer, CURLE_OK);
}

template <typename T> bool isIn(const T& v, std::initializer_list<T> lst) {
    return std::find(std::begin(lst), std::end(lst), v) != std::end(lst);
}

std::tuple<long, std::string, int> cPostDataLogged(std::string const& url, std::vector<std::string> const& headers,
                                                   std::string const& body, long const timeout) {
    GDEBUG("Trying HTTP POST to %s\n", url.c_str());
    long httpResponseCode;
    int curlCode;
    std::string responseBody;
    tie(httpResponseCode, responseBody, curlCode) = cPostData(url, headers, body, timeout);
    if (isIn<long>(httpResponseCode, {200, 201, 202})) {
        GDEBUG("POST Response code: %s\n", std::to_string(httpResponseCode).c_str());
        GDEBUG("POST Response: '%s'\n", responseBody.c_str());
    } else {
        GERROR("POST Response code: '%s'\n", std::to_string(httpResponseCode).c_str());
        GERROR("POST Response: '%s'\n", responseBody.c_str());
    }
    return std::make_tuple(httpResponseCode, responseBody, curlCode);
}
