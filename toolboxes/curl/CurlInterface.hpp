//
// Created by akuurstr on 2021-02-09.
//

#ifndef GADGETRON_CURL_INTERFACE_H
#define GADGETRON_CURL_INTERFACE_H

#include "post.hpp"
#include <string>
#include <tuple>
#include <vector>

class CurlInterface {
  public:
    virtual ~CurlInterface() = default;

    virtual std::tuple<long, std::string, int> postData(std::string const& url, std::vector<std::string> const& headers,
                                                        std::string const& body, long const timeout) {
        return cPostData(url, headers, body, timeout);
    };

    [[maybe_unused]] virtual std::tuple<long, std::string, int>
    postData(std::string const& url, std::vector<std::string> const& headers, std::string const& body) {
        return this->postData(url, headers, body, 0);
    };

    virtual std::tuple<long, std::string, int> postDataLogged(std::string const& url,
                                                              std::vector<std::string> const& headers,
                                                              std::string const& body, long const timeout) {
        return cPostDataLogged(url, headers, body, timeout);
    };

    virtual std::tuple<long, std::string, int>
    postDataLogged(std::string const& url, std::vector<std::string> const& headers, std::string const& body) {
        return this->postDataLogged(url, headers, body, 0);
    };
};

#endif // GADGETRON_CURL_INTERFACE_H
