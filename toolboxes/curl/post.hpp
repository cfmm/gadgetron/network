//
// Created by akuurstr on 2021-01-26.
//

#ifndef GADGETRON_POST_HPP
#define GADGETRON_POST_HPP

#include <string>
#include <tuple>
#include <vector>

std::tuple<long, std::string, int> cPostData(std::string const& url, std::vector<std::string> const& headers,
                                             std::string const& body, long timeout = 0);
std::tuple<long, std::string, int> cPostDataLogged(std::string const& url, std::vector<std::string> const& headers,
                                                   std::string const& body, long timeout = 0);

#endif // GADGETRON_POST_HPP
