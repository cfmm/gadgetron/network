//
// Created by Martyn Klassen on 2019-02-26.
//

#ifndef NETWORK_SSH_LIB2_H
#define NETWORK_SSH_LIB2_H

#include <libssh2.h>
#include <mutex>

namespace Gadgetron::Network { // NOLINT(*-identifier-naming)

class SshLib2 {
  private:
    static SshLib2* memberInstance;
    static std::mutex mutex;

    size_t count;

  protected:
    SshLib2() : count(0) {}
    ~SshLib2() = default;

  public:
    SshLib2(SshLib2&) = delete;
    void operator=(SshLib2 const&) = delete;

    static SshLib2* instance() {
        std::lock_guard<std::mutex> lock(mutex);
        if (memberInstance == nullptr) {
            memberInstance = new SshLib2();
        }
        return memberInstance;
    }
#pragma clang diagnostic push
#pragma ide diagnostic ignored "EmptyDeclOrStmtInspection"
    bool initialize() {
        std::lock_guard<std::mutex> lock(mutex);

        if ((0 == count) && (0 != libssh2_init(0)))
            return false;

        count++;
        return true;
    }

    bool shutdown() {
        std::lock_guard<std::mutex> lock(mutex);

        if (count > 0) {
            count--;
            if (0 == count)
                libssh2_exit();
        }

        return true;
    }
#pragma clang diagnostic pop
};

} // namespace Gadgetron::Network

#endif // NETWORK_SSH_LIB2_H
