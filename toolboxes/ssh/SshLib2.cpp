//
// Created by Martyn Klassen on 2023-06-20.
//

#include "SshLib2.hpp"

namespace Gadgetron::Network {
SshLib2* SshLib2::memberInstance{nullptr};
std::mutex SshLib2::mutex;
} // namespace Gadgetron::Network
