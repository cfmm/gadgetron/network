//
// Created by Martyn Klassen on 2023-06-20.
//

#ifndef NETWORK_SSH_COPY_TASK_H
#define NETWORK_SSH_COPY_TASK_H

#include "SshTaskInterface.hpp"

class SshCopyTask : public SshTaskInterface {
  public:
    int process(std::filesystem::path const& filename) override;
    void setRemoveFile(bool remove) { removeFile = remove; }

  protected:
    bool removeFile = false;
};

#endif // NETWORK_SSH_COPY_TASK_H
