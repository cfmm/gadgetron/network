//
//  SshTunnelTask.cpp
//  network
//
//  Created by Martyn Klassen on 2015-11-10.
//  Copyright © 2015 CFMM. All rights reserved.
//

#include "SshTunnelTask.hpp"
#include <boost/asio.hpp>
#include <log.h>

#define BUFFER_SIZE 16384

namespace Gadgetron {

SshTunnelTask::SshTunnelTask(std::string localHost, unsigned short localPort, std::string remoteHost,
                             unsigned short remotePort)
    : memberStop(false), memberReturn(ioContext), channel(nullptr), localHost(std::move(localHost)),
      memberRemoteHost(std::move(remoteHost)), memberRemotePort(remotePort), localPort(localPort) {
    returnBuffer.resize(BUFFER_SIZE);
}

void SshTunnelTask::stop() {
    memberStop = true;
    thread.join();
}

void SshTunnelTask::svc() { ioContext.run(); }

void SshTunnelTask::run() { thread = std::thread(&SshTunnelTask::svc, this); }

bool SshTunnelTask::connect(const std::string& remoteHost, const std::string& remotePort, const std::string& userName,
                            const std::string& privateKey, const std::string& passphrase) {
    // Connect to the ssh server
    SshInterface::connect(remoteHost, remotePort, userName, privateKey, passphrase);

    // The connection is established, start listening on the local port
    boost::asio::ip::tcp::acceptor acceptor(
        ioContext, boost::asio::ip::tcp::endpoint(boost::asio::ip::make_address(localHost), localPort));

    acceptor.async_accept(memberReturn, [this](const boost::system::error_code& error) { this->createChannel(error); });

    return true;
}

void SshTunnelTask::createChannel(const boost::system::error_code& error) {
    // Create a port fowarding channel
    channel = libssh2_channel_direct_tcpip_ex(session, memberRemoteHost.c_str(), memberRemotePort, localHost.c_str(),
                                              localPort);

    if (channel == nullptr) {
        int rc = libssh2_session_last_error(session, nullptr, nullptr, 0);

        if (rc == LIBSSH2_ERROR_EAGAIN) {
            memberSocket.async_wait(boost::asio::ip::tcp::socket::wait_read,
                                    [this](const boost::system::error_code& error) { this->createChannel(error); });
        } else {
            throw std::logic_error("Error opening channel");
        }
    }

    // Allocate memory for the buffers
    returnBuffer.resize(BUFFER_SIZE);
    forwardBuffer.resize(BUFFER_SIZE);

    // Write nothing to the channel, this will start the process of reading from memberReturn and writing to channel
    this->writeChannel(error, forwardBuffer.begin(), forwardBuffer.begin());

    // Read from the channel, this will start the process for reading from the channel and writing to memberReturn
    this->readChannel(error, 0);
}

void SshTunnelTask::writeChannel(const boost::system::error_code& error,
                                 std::vector<char>::iterator const& bytesTransferred,
                                 std::vector<char>::iterator const& iEnd) {
    if (memberStop)
        return;

    // Only try to write if there is something to write
    if (bytesTransferred < iEnd) {
        auto writeBlock = libssh2_channel_write(channel, &(*bytesTransferred), iEnd - bytesTransferred);

        if (LIBSSH2_ERROR_EAGAIN == writeBlock) {
            auto dir = libssh2_session_block_directions(session);
            auto waitType = boost::asio::ip::tcp::socket::wait_write;

            if (dir & LIBSSH2_SESSION_BLOCK_INBOUND)
                waitType = boost::asio::ip::tcp::socket::wait_read;

            // Unable to write, try again
            memberSocket.async_wait(waitType, [this, bytesTransferred, iEnd](const boost::system::error_code& error) {
                this->writeChannel(error, bytesTransferred, iEnd);
            });
            return;
        }

        // An unhandled error writing to the channel
        if (writeBlock < 0) {
            GERROR("Write failed: %d\n", writeBlock);
            memberStop = true;
            return;
        }

        // Move the iterator by the number of bytes written
        auto iNewBegin = bytesTransferred + writeBlock;
        if (iNewBegin < iEnd) {
            // Wait for the socket to available for writing
            memberSocket.async_wait(boost::asio::ip::tcp::socket::wait_write,
                                    [this, iNewBegin, iEnd](const boost::system::error_code& error) {
                                        this->writeChannel(error, iNewBegin, iEnd);
                                    });
            return;
        }
    }

#pragma clang diagnostic push
#pragma ide diagnostic ignored "ConstantConditionsOC"
#pragma ide diagnostic ignored "UnreachableCode"
    // Clang-tidy is wrong, memberStop is not always false and therefore the return is reachable
    if (memberStop)
        return;
#pragma clang diagnostic pop

    // Read some data from memberReturn to be written (forwarded) to channel of memberSocket
    memberReturn.async_read_some(boost::asio::buffer(forwardBuffer),
                                 [this](const boost::system::error_code& error, std::size_t bytesTransferred) {
                                     this->writeChannel(error, bytesTransferred);
                                 });
}

void SshTunnelTask::writeChannel(const boost::system::error_code& error, std::size_t bytesTransferred) {
    if (error) {
        // TODO: Handler error during read_some
    }

    // Transform the read_some callback into a writeChannel callback
    this->writeChannel(error, forwardBuffer.begin(), forwardBuffer.begin() + static_cast<ptrdiff_t>(bytesTransferred));
}

// NOLINTBEGIN(misc-no-recursion)
void SshTunnelTask::readChannel(const boost::system::error_code& error, std::size_t bytesTransferred) {
    // Return immediately, do not attempt to read from channel
    if (memberStop)
        return;

    if (libssh2_channel_eof(channel)) {
        GERROR("Server disconnected\n");
        memberStop = true;
        return;
    }

    auto readLen = libssh2_channel_read(channel, returnBuffer.data(), returnBuffer.size());

    if (LIBSSH2_ERROR_EAGAIN == readLen || 0 == readLen) {
        auto dir = libssh2_session_block_directions(session);
        auto waitType = boost::asio::ip::tcp::socket::wait_read;

        if (dir & LIBSSH2_SESSION_BLOCK_OUTBOUND)
            waitType = boost::asio::ip::tcp::socket::wait_write;

        // Nothing to read, try again later
        memberSocket.async_wait(waitType,
                                [this](const boost::system::error_code& error) { this->readChannel(error, 0); });
        return;
    } else if (readLen < 0) {
        GERROR("Read failed: %d\n", readLen);
        memberStop = true;
        return;
    }

    // Write the contents of the buffer to memberReturn and then get more data
    boost::asio::async_write(memberReturn, boost::asio::buffer(returnBuffer.data(), readLen),
                             [this](const boost::system::error_code& error, size_t bytesTransferred) {
                                 this->readChannel(error, bytesTransferred);
                             });
}
// NOLINTEND(misc-no-recursion)

void SshTunnelTask::disconnect() {
    GDEBUG("Disconnecting tunnel\n");
    memberStop = true;
    memberReturn.close();

    SshInterface::disconnect();
}
} // namespace Gadgetron
