//
// Created by Martyn Klassen on 2023-06-22.
//

#ifndef NETWORK_SSH_EXECUTE_TASK_H
#define NETWORK_SSH_EXECUTE_TASK_H

#include "SshTaskInterface.hpp"

class SshExecuteTask : public SshTaskInterface {
  public:
    int process(std::filesystem::path const& filename) override;
    virtual void setExecutable(std::string const& exec) { executable = exec; }

  protected:
    std::string executable;
};

#endif // NETWORK_SSH_EXECUTE_TASK_H
