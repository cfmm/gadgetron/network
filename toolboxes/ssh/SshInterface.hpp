// Designed by Olivia Stanley
// Oct 6th 2016

// This cass has all the functions to open and close a remote ssh connection.
// It then calls process which is a pure virtual function and needs to be implemented for every gadget which inherits
// this interface

#ifndef NETWORK_SSHINTERFACE_H
#define NETWORK_SSHINTERFACE_H

#include <SshLib2.hpp>
#include <boost/asio.hpp>
#include <log.h>
#include <netdb.h>

#include "PublicPrivateKey.hpp"

class SshInterface {
  public:
    SshInterface() : session(nullptr), context(), memberSocket(context) {
        if (!Gadgetron::Network::SshLib2::instance()->initialize()) {
            throw std::runtime_error("libssh2 initialization failed");
        }
    }

    virtual ~SshInterface() { Gadgetron::Network::SshLib2::instance()->shutdown(); }

    virtual void disconnect() {
        if (session) {
            libssh2_session_disconnect(session, "Bye");
            libssh2_session_free(session);
        }

        memberSocket.close();
        session = nullptr;
    }

    virtual bool connect(const std::string& remoteHost, const std::string& remotePort, const std::string& userName,
                         const std::string& privateKey, const std::string& password) {
        remote = remoteHost + ":" + remotePort;

        using boost::asio::ip::tcp;
        tcp::resolver resolver(context);

        try {
            boost::asio::connect(memberSocket, resolver.resolve(remoteHost, remotePort));
        } catch (std::exception& e) {
            GERROR("Unable to connect to %s:%s: %s\n", remoteHost.c_str(), remotePort.c_str(), e.what());
            return false;
        }

        /* Create a session instance */
        session = libssh2_session_init();

        if (!session) {
            GERROR("Unable to create libssh2 session\n");
            disconnect();
            return false;
        }

        // SSH runs everything blocking
        libssh2_session_set_blocking(session, 1);

        /* ... start it up. This will trade welcome banners, exchange keys,
         * and setup crypto, compression, and MAC layers
         */
        auto rc = libssh2_session_handshake(session, memberSocket.native_handle());

        if (rc) {
            GERROR("Failure to establish SSH session with error %d\n", rc);
            disconnect();
            return false;
        }

        // Not checking known_hosts

        // Authenticate using public key
        if (!authenticate(userName, privateKey, password)) {
            disconnect();

            GERROR("Authentication to %s@%s on %s with private key %s failed.\n", userName.c_str(), remoteHost.c_str(),
                   remotePort.c_str(), privateKey.c_str());
            return false;
        }

        return true;
    }

  protected:
    LIBSSH2_SESSION* session;
    boost::asio::io_context context;
    boost::asio::ip::tcp::socket memberSocket;

    virtual bool authenticate(const std::string& userName, const std::string& privateKey, const std::string& password) {
        char* userAuthList =
            libssh2_userauth_list(session, userName.c_str(), static_cast<unsigned int>(userName.length()));

        if (nullptr == userAuthList) {
            if (0 == libssh2_userauth_authenticated(session)) {
                GERROR("publickey authorization not supported\n");
                return false;
            } else {
                GDEBUG("SSH_USERAUTH_NONE accepted, assuming publickey\n");
            }
        } else if (!strstr(userAuthList, "publickey")) {
            GERROR("publickey authorization not supported, only \"%s\"\n");
            return false;
        }

        // Authenticate the session
        // This assumes the libssh2 is compiled against openssl
        int rc;
        rc = libssh2_userauth_publickey_fromfile(session, userName.c_str(), nullptr, privateKey.c_str(),
                                                 password.c_str());

        if (rc == LIBSSH2_ERROR_PUBLICKEY_UNVERIFIED) {
            // Probably a borked libssh2 installation using libgcrypt which
            // - does not support generating the public key from the private key
            // - does not support encrypted private keys
            // - does not support unencrypted private keys from memory
            // - is a complete piece of garbage
            // Use openssl to generate the public and unencrypted private keys from the encrypted private key
            // Since libssh2_userauth_publickey_frommemory is not supported with libgcrypt, temporary files must be used

            bool status;

            try {
                PublicPrivateKey key(privateKey, password);

                rc = libssh2_userauth_publickey_fromfile(session, userName.c_str(), key.getPublicKeyFile().c_str(),
                                                         key.getPrivateKeyFile().c_str(), nullptr);

                status = validateAuth(rc);
            } catch (std::runtime_error& error) {
                status = false;
            }

            if (!status) {
                rc = libssh2_userauth_password(session, userName.c_str(), password.c_str());
                status = validateAuth(rc);
            }

            return status;
        }

        // Check for error
        return validateAuth(rc);
    }

    std::string remote;

  private:
    bool validateAuth(int rc) {
        if (rc) {
            char* errMsg;
            libssh2_session_last_error(session, &errMsg, nullptr, 0);
            GERROR("Authentication failed with code (see libssh2.h): %d , message: %s\n", rc, errMsg);
            return false;
        }
        return true;
    }
};

#endif // NETWORK_SshInterface_H
