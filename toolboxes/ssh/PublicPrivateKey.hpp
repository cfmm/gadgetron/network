//
// Created by Martyn Klassen on 2019-06-14.
//

#ifndef NETWORK_PUBLIC_PRIVATE_KEYS_H
#define NETWORK_PUBLIC_PRIVATE_KEYS_H

#include <cstdint>
#include <exception>
#include <filesystem>
#include <fstream>
#include <ios>
#include <log.h>
#include <unistd.h>

#include <openssl/bio.h>
#include <openssl/bn.h>
#include <openssl/err.h>
#include <openssl/pem.h>
#include <openssl/rsa.h>

class PublicPrivateKey {

  public:
    PublicPrivateKey(std::string const& privateKeyFile, std::string const& password) {
        bool status = getKey(privateKeyFile, password, privateKey, publicKey);

        // TODO
        // Test if getPrivateKeyFile was unencrypted and do not write a new one if this is the case
        ERR_free_strings();

        if (!status)
            throw std::runtime_error("Unable to obtain public/private key");
    }

    ~PublicPrivateKey() {
        // Delete the temporary key files when object drops out of scope
        std::filesystem::remove(privateKeyFile);
        std::filesystem::remove(publicKeyFile);
    }

    // Create private key file (unencrypted) when requested
    std::filesystem::path const& getPrivateKeyFile() {
        if (!std::filesystem::exists(privateKeyFile))
            createFile(privateKeyFile, privateKey);

        return privateKeyFile;
    }

    // Create public key file when requested
    std::filesystem::path const& getPublicKeyFile() {
        if (!std::filesystem::exists(publicKeyFile))
            createFile(publicKeyFile, publicKey);

        return publicKeyFile;
    }

    std::string const& getPrivateKey() { return privateKey; }

    std::string const& getPublicKey() { return publicKey; }

  private:
    static void createFile(std::filesystem::path& keyFile, std::string const& content) {
        std::string keyFileStr = std::filesystem::temp_directory_path() / "XXXXXX";
        auto file = mkstemp(keyFileStr.data());

        if (file < 0) {
            static std::string msg("Unable to create temporary file that does not exist.");
            GERROR("%s\n", msg.c_str());
            throw std::runtime_error(msg);
        }
        auto writeSize = write(file, content.c_str(), content.size());
        close(file);
        if (writeSize != content.size()) {
            throw std::runtime_error("Incomplete key file written");
        }
        keyFile = std::filesystem::path(keyFileStr);
    }

    static bool getKey(std::string const& privateKeyFilename, const std::string& password, std::string& privateKey,
                       std::string& publicKey) {
        // Read the privateKeyFilename file
        FILE* fp;
        fp = std::fopen(privateKeyFilename.c_str(), "r");
        if (nullptr == fp) {
            GERROR("Unable to open SSH key %s\n", privateKeyFilename.c_str());
            return false;
        }
        EVP_PKEY* key = PEM_read_PrivateKey(fp, nullptr, nullptr, const_cast<char*>(password.c_str()));

        // Close the file down
        fclose(fp);

        // Check for success reading the file
        if (nullptr == key) {
            GERROR("Unable to read private SSH key %s with provided password: %s\n", privateKeyFilename.c_str(),
                   ERR_error_string(ERR_get_error(), nullptr));
            return false;
        }

        BIO* bio = BIO_new(BIO_s_mem());
        if (nullptr == bio) {
            GERROR("Unable to allocate BIO for private key: %s\n", ERR_error_string(ERR_get_error(), nullptr));
            EVP_PKEY_free(key);
            return false;
        }

        // Write the private key to the BIO
        if (!PEM_write_bio_PrivateKey(bio, key, nullptr, nullptr, 0, nullptr, nullptr)) {
            GERROR("Unable to write private key: %s\n", ERR_error_string(ERR_get_error(), nullptr));
            EVP_PKEY_free(key);
            BIO_free_all(bio);
            return false;
        }

        char* data;
        long dataSize = BIO_get_mem_data(bio, &data);
        if (dataSize <= 0) {
            GERROR("Unable to read private key from BIO: %s\n", ERR_error_string(ERR_get_error(), nullptr));
            BIO_free_all(bio);
            return false;
        }

        privateKey.assign(data, static_cast<unsigned long>(dataSize));
        BIO_reset(bio);

        if (!BIO_reset(bio)) {
            GERROR("Unable to reset BIO for public key: %s\n", ERR_error_string(ERR_get_error(), nullptr));
            EVP_PKEY_free(key);
            BIO_free_all(bio);
            return false;
        }

        if (!PEM_write_bio_PUBKEY(bio, key)) {
            GERROR("Unable to write public key\n");
            EVP_PKEY_free(key);
            BIO_free_all(bio);
            return false;
        }

        // Free the key
        EVP_PKEY_free(key);

        dataSize = BIO_get_mem_data(bio, &data);
        if (dataSize <= 0) {
            GERROR("Unable to read public key from BIO: %s\n", ERR_error_string(ERR_get_error(), nullptr));
            BIO_free_all(bio);
            return false;
        }

        publicKey.assign(data, static_cast<unsigned long>(dataSize));
        BIO_free_all(bio);

        return true;
    }

    std::filesystem::path privateKeyFile;
    std::filesystem::path publicKeyFile;
    std::string privateKey;
    std::string publicKey;
};

#endif // NETWORK_PUBLIC_PRIVATE_KEYS_H
