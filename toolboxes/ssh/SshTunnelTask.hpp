//
//  SshTunnelTask.h
//  network
//
//  Created by Martyn Klassen on 2015-11-09.
//  Copyright © 2015 CFMM. All rights reserved.
//

#ifndef NETWORK_SSH_TUNNEL_TASK_H
#define NETWORK_SSH_TUNNEL_TASK_H

#include "SshInterface.hpp"
#include <boost/asio.hpp>
#include <iostream>

namespace Gadgetron {

class SshTunnelTask : private SshInterface {
  public:
    SshTunnelTask(std::string localHost, unsigned short localPort, std::string remoteHost, unsigned short remotePort);

    bool connect(const std::string& remoteHost, const std::string& remotePort, const std::string& userName,
                 const std::string& privateKey, const std::string& passphrase) override;

    ~SshTunnelTask() override { this->stop(); }

    void run();
    void stop();

  protected:
    void disconnect() override;

    virtual void createChannel(const boost::system::error_code& error);
    virtual void writeChannel(const boost::system::error_code& error, std::size_t bytesTransferred);
    virtual void writeChannel(const boost::system::error_code& error,
                              std::vector<char>::iterator const& bytesTransferred,
                              std::vector<char>::iterator const& iEnd);
    virtual void readChannel(const boost::system::error_code& error, std::size_t bytesTransferred);

    void svc();

  protected:
    boost::asio::io_context ioContext;
    LIBSSH2_CHANNEL* channel;
    std::vector<char> forwardBuffer;
    std::vector<char> returnBuffer;

    std::thread thread;

  private:
    bool memberStop;
    std::string localHost;
    unsigned short localPort;
    std::string memberRemoteHost;
    unsigned short memberRemotePort;
    std::string forwardPort;
    std::string forwardHost;

    boost::asio::ip::tcp::socket memberReturn;
};

} // namespace Gadgetron

#endif /* NETWORK_SSH_TUNNEL_TASK_H */
