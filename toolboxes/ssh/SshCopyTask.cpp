//
// Created by Martyn Klassen on 2023-06-20.
//

#include "SshCopyTask.hpp"

int SshCopyTask::process(std::filesystem::path const& filename) {
    LIBSSH2_CHANNEL* channel;
    ssize_t rc;
    FILE* local;
    struct stat fileinfo {};
    char mem[1024];
    char* ptr;
    size_t nRead;
    time_t start;
    size_t total = 0;
    int duration;
    size_t prev;

    std::filesystem::path remoteFile = remoteDirectory / filename.filename();

    // Check that the local file can be opened and get its info
    auto filePath = filename.c_str();

    local = fopen(filePath, "rb");
    if (local == nullptr) {
        GERROR("Can't open local file %s.\n", filePath);
        return -1;
    }

    stat(filePath, &fileinfo);

    /* Send a file via scp. The mode parameter must only have permissions! */
    do {
        channel = libssh2_scp_send(session, remoteFile.c_str(), static_cast<unsigned int>(fileinfo.st_mode) & 0777u,
                                   (unsigned long)fileinfo.st_size);
        if ((!channel) && (libssh2_session_last_errno(session) != LIBSSH2_ERROR_EAGAIN)) {
            char* errMsg;
            libssh2_session_last_error(session, &errMsg, nullptr, 0);
            GERROR("%s\n", errMsg);
            return -1;
        }
    } while (!channel);

    GDEBUG("SCP session waiting to send file of size %i bytes.\n", fileinfo.st_size);
    start = time(nullptr);
    do {
        nRead = fread(mem, 1, sizeof(mem), local);
        if (nRead <= 0) {
            /* end of file */
            break;
        }
        ptr = mem;
        total += nRead;
        prev = 0;
        do {
            rc = libssh2_channel_write(channel, ptr, nRead);
            if (rc < 0) {
                GERROR("ERROR %d total %ld / %d prev %d\n", rc, total, (int)nRead, (int)prev);
                libssh2_channel_close(channel);
                libssh2_channel_free(channel);
                return -1;
            } else {
                prev = nRead;

                /* rc indicates how many bytes were written this time */
                nRead -= rc;
                ptr += rc;
            }
        } while (nRead);
    } while (true);

    duration = (int)(time(nullptr) - start);

    GDEBUG("%ld bytes in %d seconds makes %.1f bytes/sec\n", total, duration, total / (double)duration);

    GDEBUG("Sending EOF\n");
    libssh2_channel_send_eof(channel);
    GDEBUG("Waiting for EOF\n");
    libssh2_channel_wait_eof(channel);

    // Close up shop
    rc = libssh2_channel_close(channel);
    libssh2_channel_free(channel);

    if (rc != 0) {
        GERROR("Closing channel failed\n");
        return -1;
    }

    GINFO("Copied %s to %s:%s\n", filePath, remote.c_str(), remoteFile.c_str());

    // Cleanup the local file that has been copied
    if (removeFile && std::filesystem::remove(filePath)) {
        GINFO("Removed %s\n", filePath);
    }
    return 0;
}
