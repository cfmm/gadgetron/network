//
// Created by Martyn Klassen on 2023-06-22.
//

#include "SshExecuteTask.hpp"

int SshExecuteTask::process(std::filesystem::path const& filename) {
    int exitcode;
    auto* exitSignal = (char*)"none";
    int retCode(0);

    // Set Command
    std::stringstream submitCommand;

    submitCommand << "bash -l -c \'" << executable << " " << filename << "\'";

    /* Exec non-blocking on the remote host */
    auto channel = libssh2_channel_open_session(session);

    if (channel == nullptr) {
        GERROR("Failed to open SSH channel\n");
        return -1;
    }

    // Execute the command
    auto rc = libssh2_channel_exec(channel, submitCommand.str().c_str());

    if (rc) {
        GERROR("Executing %s on %s failed with %d\n", submitCommand.str().c_str(), remote.c_str(), rc);
        libssh2_channel_close(channel);
        libssh2_channel_free(channel);
        return -1;
    }

    // Get and ignore all the stdout
    ssize_t count;
    do {
        char buffer[0x4000];
        count = libssh2_channel_read(channel, buffer, sizeof(buffer));
    } while (count > 0);

    // Default is command not found error
    exitcode = 127;

    // Close up shop
    rc = libssh2_channel_close(channel);

    // Get the exit code
    if (rc == 0) {
        // Documentation is weird
        // exitcode == 0 on failure of call, but command exit status == 0 is success
        // so exitcode == 0 on failure and on success
        exitcode = libssh2_channel_get_exit_status(channel);
        libssh2_channel_get_exit_signal(channel, &exitSignal, nullptr, nullptr, nullptr, nullptr, nullptr);
    }

    libssh2_channel_free(channel);

    if (exitSignal || exitcode) {
        GERROR("Remote command: %s exitcode (%d), signal (%s)\n", submitCommand.str().c_str(), exitcode, exitSignal);
        return -1;
    }

    return retCode;
}
