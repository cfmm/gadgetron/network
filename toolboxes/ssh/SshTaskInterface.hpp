//
// Created by Martyn Klassen on 2019-06-07.
//

#ifndef NETWORK_SSH_TASK_INTERFACE_H
#define NETWORK_SSH_TASK_INTERFACE_H

#include "SshInterface.hpp"
#include <filesystem>

class SshTaskInterface : public SshInterface {
  public:
    virtual int process(std::filesystem::path const& filename) = 0;

    void setWorkingDirectory(std::filesystem::path const& path) { remoteDirectory = path; }

  protected:
    std::filesystem::path remoteDirectory;

    [[maybe_unused]] static int waitSocket(int socketFd, LIBSSH2_SESSION* session) {
        struct timeval timeout {};
        int rc;
        fd_set fd;
        fd_set* writeFd = nullptr;
        fd_set* readFd = nullptr;
        int dir;

        timeout.tv_sec = 10;
        timeout.tv_usec = 0;

        FD_ZERO(&fd);

        FD_SET(socketFd, &fd); // NOLINT(hicpp-signed-bitwise)

        /* now make sure we wait in the correct direction */
        dir = libssh2_session_block_directions(session);

        if (dir & LIBSSH2_SESSION_BLOCK_INBOUND) // NOLINT(hicpp-signed-bitwise)
            readFd = &fd;

        if (dir & LIBSSH2_SESSION_BLOCK_OUTBOUND) // NOLINT(hicpp-signed-bitwise)
            writeFd = &fd;

        rc = select(socketFd + 1, readFd, writeFd, nullptr, &timeout);

        return rc;
    }
};

#endif // NETWORK_SSH_TASK_INTERFACE_H
