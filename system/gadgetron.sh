#!/bin/sh

### BEGIN INIT INFO
# Provides:          gadgetron
# Required-Start:    $remote_fs $syslog
# Required-Stop:     $remote_fs $syslog
# Default-Start:     2 3 4 5
# Default-Stop:      0 1 6
# Short-Description: Gadgetron server
### END INIT INFO

# Author: Martyn Klassen <lmklassen@gmail.com>

# /etc/init.d/gadgetron: start and stop the gadgetron service

PATH=/sbin:/usr/sbin:/bin:/usr/bin:/usr/local/sbin:/usr/local/bin
LD_LIBRARY_PATH=/usr/local/lib
DESC="Gadgetron MRI reconstruction"
NAME=gadgetron
PIDFILE=/var/run/$NAME.pid
SCRIPTNAME=/etc/init.d/$NAME

DAEMON=/usr/local/bin/gadgetron
DAEMON_OPTS="-p 9002"
DAEMON_USER=gadgetron:gadgetron
DAEMON_LOG=/var/log/gadgetron/gadgetron.log

# Setup external logging
export GADGETRON_LOG_FILE=$DAEMON_LOG

# Exit if the package is not installed
[ -x "$DAEMON" ] || exit 0

# Read configuration variable file if it is present
[ -r /etc/default/$NAME ] && . /etc/default/$NAME

# Load the rcS variable and functions
. /lib/init/vars.sh
. /lib/lsb/init-functions

do_start()
{
   # Return
   #   0 if daemon has been started
   #   1 if daemon was already running
   #   2 if daemon could not be started

   start-stop-daemon --start \
                     --quiet \
                     --name gadgetron \
                     --background \
                     --chuid ${DAEMON_USER} \
                     --make-pidfile \
                     --pidfile ${PIDFILE} \
                     --exec ${DAEMON} \
                     --test > /dev/null \
      || return 1
   start-stop-daemon --start \
                     --oknodo \
                     --quiet \
                     --name gadgetron \
                     --background \
                     --chuid ${DAEMON_USER} \
                     --make-pidfile \
                     --pidfile ${PIDFILE} \
                     --exec ${DAEMON} -- ${DAEMON_OPTS} \
      || return 2
}

do_stop()
{
   # Return
   #   0 if daemon has been stopped
   #   1 if daemon was already stopped
   #   2 if daemon could not be stopped
   #   other if a failure occurred
   start-stop-daemon --stop \
                     --quiet \
                     --retry=TERM/30/KILL/5 \
                     --pidfile $PIDFILE \
                     --name $NAME
   RETVAL="$?"
   [ "$RETVAL" = 2 ] && return 2

   # Allow time for process to shutdown
   sleep 5

   start-stop-daemon --stop --quiet --oknodo --retry=0/30/KILL/5 --exec $DAEMON
   [ "$?" = 2 ] && return 2
   # Many daemons don't delete their pidfiles when they exit.
   rm -f $PIDFILE
   return "$RETVAL"
}
case "$1" in
  start)
        [ "$VERBOSE" != no ] && log_daemon_msg "Starting $DESC" "$NAME"
        do_start
        case "$?" in
                0|1) [ "$VERBOSE" != no ] && log_end_msg 0 ;;
                2) [ "$VERBOSE" != no ] && log_end_msg 1 ;;
        esac
        ;;
  stop)
        [ "$VERBOSE" != no ] && log_daemon_msg "Stopping $DESC" "$NAME"
        do_stop
        case "$?" in
                0|1) [ "$VERBOSE" != no ] && log_end_msg 0 ;;
                2) [ "$VERBOSE" != no ] && log_end_msg 1 ;;
        esac
        ;;
  status)
        status_of_proc "$DAEMON" "$NAME" && exit 0 || exit $?
        ;;
  restart|force-reload)
        #
        # If the "reload" option is implemented then remove the
        # 'force-reload' alias
        #
        log_daemon_msg "Restarting $DESC" "$NAME"
        do_stop
        case "$?" in
          0|1)
                do_start
                case "$?" in
                        0) log_end_msg 0 ;;
                        1) log_end_msg 1 ;; # Old process is still running
                        *) log_end_msg 1 ;; # Failed to start
                esac
                ;;
          *)
                # Failed to stop
                log_end_msg 1
                ;;
        esac
        ;;
  *)
        echo "Usage: $SCRIPTNAME {start|stop|status|restart|force-reload}" >&2
        exit 3
        ;;
esac

