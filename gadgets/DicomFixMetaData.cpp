//
//  DicomFixMetaData.cpp
//  network
//
//  Created by Sahar Rabinoviz on 2015-08-14.
//  Copyright (c) 2015 CFMM. All rights reserved.
//

#include "DicomFixMetaData.hpp"
#include "mri_core_data.h"

namespace Gadgetron {
void DicomFixMetaData::process(
    Core::InputChannel<DcmFileFormat, Core::optional<std::string>, Core::optional<ISMRMRD::MetaContainer>>& in,
    Core::OutputChannel& out) {
    auto rSpace = this->header.encoding[0].reconSpace;
    auto pixelSpacingX = rSpace.fieldOfView_mm.x / static_cast<float>(rSpace.matrixSize.x);
    auto pixelSpacingY = rSpace.fieldOfView_mm.y / static_cast<float>(rSpace.matrixSize.y);
    // When calculating distance between slices DicomFinishGadget doesn't consider number kspace2
    auto sliceSpacing = rSpace.fieldOfView_mm.z / static_cast<float>(rSpace.matrixSize.z);

    for (auto data : in) {

        auto file = std::get<DcmFileFormat>(data);

        bool res = true;
        res &= removeBadParameters(file);
        res &= addUserParameters(file);
        res &= fixBadParameters(file, pixelSpacingY, pixelSpacingX);
        res &= handleMultipleFrames(file, sliceSpacing);

        auto meta = std::get<Core::optional<ISMRMRD::MetaContainer>>(data);

        if (meta.has_value()) {
            res &= processMetaContainer(file, meta.value());
        }

        if (!res)
            GERROR("Error processing metadata\n");

        out.push(std::move(data));
    }
}

bool DicomFixMetaData::removeBadParameters(DcmFileFormat& file) {
    DcmTagKey key;
    DcmDataset* dataset = file.getDataset();

    // Window Center
    key.set(0x0028, 0x1050);
    dataset->remove(key);

    // Window Width
    key.set(0x0028, 0x1051);
    dataset->remove(key);

    return true;
}

// Would rather have this be lambda in addUserParameters, but can't have templated lambda in C++14
/*!
    Searches for index instance of name inside vector, returns true if found that instance, false otherwise
    \param name     name to search for inside vector.name
    \param index    ith instance of of the corresponding name to find
    \param val      reference to mutable variable that will be populated with the value
 */
template <typename RET, typename T> bool getter(const char* name, size_t index, std::vector<T>& vector, RET& val) {
    size_t currentIndex = 0;
    for (auto& param : vector) {
        if (strcmp(param.name.c_str(), name) == 0 && index == currentIndex) {
            val = param.value;
            return true;
        }
    }

    GDEBUG("Error on finding %dth instance of %s\n", index + 1, name);
    return false;
};

// TODO all info is available in proc_config do it there, (current issue saving the dcmFile meta data + not writing over
// it)
bool DicomFixMetaData::addUserParameters(DcmFileFormat& dcmFile) {
    OFCondition status;
    DcmTagKey key;
    DcmDataset* dataset = dcmFile.getDataset();

    std::vector<DicomKeySet> identifiers = {DICOM_NUMBER_PHASE_ENCODE_STEPS,
                                            DICOM_NUMBER_AVERAGES,
                                            DICOM_OPERATOR_NAME,
                                            DICOM_IMAGED_NUCLEUS,
                                            DICOM_RF_COIL_NAME,
                                            DICOM_PERCENT_SAMPLING};

    auto params = header.userParameters.get();

    std::function<size_t(const char*)> appearances = [&](const char* name) {
        size_t count = 0;

        for (auto& param : params.userParameterLong) {
            if (strcmp(param.name.c_str(), name) == 0) {
                count++;
            }
        }
        for (auto& param : params.userParameterDouble) {
            if (strcmp(param.name.c_str(), name) == 0) {
                count++;
            }
        }
        for (auto& param : params.userParameterString) {
            if (strcmp(param.name.c_str(), name) == 0) {
                count++;
            }
        }

        return count;
    };

    size_t bufferSize = 1024;
    std::function<bool(char*, const char*, MetaType, size_t)> valueGetter = [&](char* buffer, const char* name,
                                                                                MetaType t, size_t i) {
        bool validResult;
        switch (t) {
            {
            case T_DOUBLE:
                double r(0.0);
                validResult = getter<double, ISMRMRD::UserParameterDouble>(name, i, params.userParameterDouble, r);
                snprintf(buffer, bufferSize, "%f", r);
                break;
            }
            {
            case T_LONG:
                long r(0);
                validResult = getter<long, ISMRMRD::UserParameterLong>(name, i, params.userParameterLong, r);
                snprintf(buffer, bufferSize, "%ld", r);
                break;
            }
            {
            case T_STRING:
                std::string r;
                validResult = getter<std::string, ISMRMRD::UserParameterString>(name, i, params.userParameterString, r);
                snprintf(buffer, bufferSize, "%s", r.c_str());
                break;
            }
            { // Getting error "switch case is in protected scope" w/o these curly brackets
            default:
                GDEBUG("Error bad meta type given for param  %s.\n", name);
                return false;
            }
        }
        return validResult;
    };

    addDicomKeys(identifiers, dataset, appearances, valueGetter);

    return true;
}

bool DicomFixMetaData::fixBadParameters(DcmFileFormat& dcmFile, float pixelSpacingY, float pixelSpacingX) {
    OFCondition status;
    DcmTagKey key;
    size_t bufferSize = 1024;
    auto buf = new char[bufferSize];
    DcmDataset* dataset = dcmFile.getDataset();

    key.set(0x0028, 0x0030);
    snprintf(buf, bufferSize, "%.6f\\%.6f", pixelSpacingY, pixelSpacingX);
    WRITE_DCM_STRING(key, buf);
    delete[] buf;
    return true;
}

bool DicomFixMetaData::handleMultipleFrames(DcmFileFormat& dcmFile, float sliceSpacing) {
    size_t numberFrames = header.encoding[0].encodingLimits.kspace_encoding_step_2->maximum;

    if (numberFrames <= 1) {
        return true;
    }

    OFCondition status;
    DcmTagKey key;
    size_t bufferSize = 1024;
    auto buf = new char[bufferSize];
    DcmDataset* dataset = dcmFile.getDataset();

    key.set(0x0018, 0x0088); // their calculation doesn't consider slices (only slabs)
    snprintf(buf, bufferSize, "%f", sliceSpacing);
    WRITE_DCM_STRING(key, buf);

    delete[] buf;

    return true;
}

bool DicomFixMetaData::processMetaContainer(DcmFileFormat& dcmFile, ISMRMRD::MetaContainer& metaContainer) {
    DcmDataset* dataset = dcmFile.getDataset();

    std::vector<DicomKeySet> identifiers = {DICOM_KEY_IMAGE_SCALE_OFFSET, DICOM_KEY_IMAGE_SCALE_RATIO};

    std::function<size_t(const char*)> appearances = [&](const char* x) { return metaContainer.length(x); };

    size_t bufferSize = 1024;
    std::function<bool(char*, const char*, MetaType, size_t)> valueGetter = [&](char* buffer, const char* name,
                                                                                MetaType t, size_t i) {
        switch (t) {
            {
            case T_DOUBLE:
                snprintf(buffer, bufferSize, "%f", metaContainer.value(name, i).as_double());
                break;
            }
            {
            case T_LONG:
                snprintf(buffer, bufferSize, "%ld", metaContainer.value(name, i).as_long());
                break;
            }
            {
            case T_STRING:
                snprintf(buffer, bufferSize, "%s", metaContainer.value(name, i).as_str());
                break;
            }
            { // Getting error "switch case is in protected scope" w/o these curly brackets
            default:
                GDEBUG("Error bad meta type given for param  %s.\n", name);
                return false;
            }
        }
        return true;
    };

    addDicomKeys(identifiers, dataset, appearances, valueGetter);

    return true;
}

bool DicomFixMetaData::addDicomKeys(std::vector<DicomKeySet>& identifiers, DcmDataset* dataset,
                                    std::function<size_t(const char*)>& appearances,
                                    std::function<bool(char*, const char*, MetaType, size_t)>& valueGetter) {
    OFCondition status;
    DcmTagKey key;
    size_t bufferSize = 1024;
    auto buf = new char[bufferSize];

    // TODO test multiple values
    // add extra parameters from meta data that are found
    for (auto& k : identifiers) {
        size_t num = appearances(k.name.c_str());

        if (num == 0) {
            GDEBUG("Failed to find parameters named %s\n", k.name.c_str());
            delete[] buf;
            return false;
        }

        // set key
        key.set(k.k1, k.k2);

        // build buffer
        std::string buffer;
        for (size_t i = 0; i < num; i++) {
            if (!valueGetter(buf, k.name.c_str(), k.expected, i)) {
                delete[] buf;
                return false;
            }
            buffer.append(buf);
            buffer.append("\\");
        }
        // remove last '\'
        if (buffer.size() > 1)
            buffer.pop_back();

        if (!buffer.empty())
            WRITE_DCM_STRING(key, buffer.c_str());
    }
    delete[] buf;

    return true;
}
GADGETRON_GADGET_EXPORT(DicomFixMetaData)
} // namespace Gadgetron
