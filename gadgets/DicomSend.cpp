//
//  DicomSend.cpp
//  network
//
//  Created by Martyn Klassen on 2015-05-05.
//  Copyright (c) 2015 CFMM. All rights reserved.
//

#include "DicomSend.hpp"
#include "SshTunnelTask.hpp"

namespace Gadgetron {

// Begin EVIL code
// DcmSCU does not permit direct access to the T_ASC_Parameters
// DcmSCU does not provide methods for setting the user identity
// DcmSCU is clearly deficient and short of reimplementing, the
// only options is to use EVIL code that provides access to the
// private m_params member.
typedef T_ASC_Parameters* DcmSCU::*ParamPtr;

#pragma clang diagnostic push
#pragma ide diagnostic ignored "NotImplementedFunctions"
#pragma ide diagnostic ignored "OCUnusedStructInspection"
#pragma ide diagnostic ignored "MemberVisibility"
#pragma ide diagnostic ignored "OCUnusedGlobalDeclarationInspection"

template <typename tag, ParamPtr ptr> struct AccessBypass {
    friend ParamPtr steal(tag) { return ptr; }
};

struct ParamAccess {
    friend ParamPtr steal(ParamAccess);
};

template struct AccessBypass<ParamAccess, &DcmSCU::m_params>;
#pragma clang diagnostic pop
// End EVIL code

void DicomSend::process(
    Core::InputChannel<DcmFileFormat, Core::optional<std::string>, Core::optional<ISMRMRD::MetaContainer>>& in,
    Core::OutputChannel& out) {
    // TODO Open the tunnel
    std::string const* peerHost = &peerHostName;
    auto iPeerPort = peerPort;

    SshTunnelTask* pTunnel = nullptr;
    if (!(forwardHost.empty() || forwardPort.empty() || username.empty())) {
        pTunnel = new SshTunnelTask(localHost, localPort, peerHostName, peerPort);

        pTunnel->connect(forwardHost, forwardPort, username, privateKey, passphrase);

        pTunnel->run();
        peerHost = &localHost;
        iPeerPort = localPort;
    }

    DcmSCU* scu;

    bool bTLS = !(dcmCertificateFiles.empty());
    bool tlsAuth = !(dcmPrivateKey.empty() || dcmCertFile.empty());

    if (bTLS || tlsAuth)
        scu = new DcmTLSSCU;
    else
        scu = new DcmSCU;

    if (!callingAETitle.empty())
        scu->setAETitle(callingAETitle);
    scu->setPeerAETitle(peerAETitle);
    scu->setPeerHostName(*peerHost);
    scu->setPeerPort(iPeerPort);

    if (bTLS || tlsAuth) {
        auto pSCU = dynamic_cast<DcmTLSSCU*>(scu);

        pSCU->setTLSProfile(TSP_Profile_BCP195);

        if (bTLS) {
            const std::vector<std::string>& certFiles = dcmCertificateFiles;
            auto iterFirst = certFiles.cbegin();
            auto iterEnd = certFiles.cend();
            std::cout << certFiles.size() << "\n";
            while (iterFirst != iterEnd) {
                std::cout << iterFirst->c_str() << "\n";
                pSCU->addTrustedCertFile(*iterFirst);
                iterFirst++;
            }
            // Configure a TLS relationship
            pSCU->setPeerCertVerification(DCV_requireCertificate);
        } else {
            pSCU->setPeerCertVerification(DCV_ignoreCertificate);
        }

        if (tlsAuth)
            pSCU->enableAuthentication(dcmPrivateKey, dcmCertFile, dcmPassphrase.c_str());
    }

    OFList<OFString> ts;
    ts.emplace_back(UID_LittleEndianExplicitTransferSyntax);
    ts.emplace_back(UID_BigEndianExplicitTransferSyntax);
    ts.emplace_back(UID_LittleEndianImplicitTransferSyntax);
    scu->addPresentationContext(UID_MRImageStorage, ts);

    auto result = scu->initNetwork();
    if (result.bad()) {
        GERROR("dcmtk unable to initialize network: %s\n", result.text());
        return;
    }

    // EVIL code used to access m_params
    // This MUST occur after initNetwork which allocates m_params
    // and before association negotiation which uses m_params
    if (!(dcmUsername.empty() || dcmPassword.empty())) {
#pragma clang diagnostic push
#pragma ide diagnostic ignored "RedundantCast"
        result = ASC_setIdentRQUserPassword(static_cast<T_ASC_Parameters*>(scu->*steal(ParamAccess())), dcmUsername,
                                            dcmPassword);
#pragma clang diagnostic pop
        if (result.bad()) {
            GERROR("dcmtk unable to set user identity: %\n", result.text());
            return;
        }
    }

    result = scu->negotiateAssociation();
    if (result.bad()) {
        GERROR("dcmtk unable to negotiate association: %s\n", result.text());
        return;
    }

    char uid[100];
    std::string defaultStudyInstanceUID = dcmGenerateUniqueIdentifier(uid, SITE_STUDY_UID_ROOT);

    for (auto data : in) {

        DcmDataset* dataset = get<DcmFileFormat>(data).getDataset();

        // Make sure that StudyInstanceUID is set to a valid value
        DcmTag tagKey;
        const char* pValue;
        tagKey.set(0x0020, 0x000D);
        result = dataset->findAndGetString(tagKey, pValue);
        if (result.bad() || pValue == nullptr || (strncmp(pValue, "XXXXXXXX", 8) != 0)) {
            dataset->putAndInsertString(tagKey, defaultStudyInstanceUID.c_str());
        }

        E_TransferSyntax transferSyntax;
        OFString transferUID;
        if (dataset->hasRepresentation(EXS_LittleEndianExplicit, nullptr)) {
            transferSyntax = EXS_LittleEndianExplicit;
            transferUID = UID_LittleEndianExplicitTransferSyntax;
        } else if (dataset->hasRepresentation(EXS_BigEndianExplicit, nullptr)) {
            transferSyntax = EXS_BigEndianExplicit;
            transferUID = UID_BigEndianExplicitTransferSyntax;
        } else if (dataset->hasRepresentation(EXS_LittleEndianImplicit, nullptr)) {
            transferSyntax = EXS_LittleEndianImplicit;
            transferUID = UID_LittleEndianImplicitTransferSyntax;
        } else {
            GERROR("dcmtk found no valid representation\n");
            break;
        }

        T_ASC_PresentationContextID presID = scu->findPresentationContextID(UID_MRImageStorage, transferUID);
        if (0 == presID) {
            GERROR("dcmtk unable to find presentation context\n");
            break;
        }

        // Set the transfer syntax
        dataset->chooseRepresentation(transferSyntax, nullptr);

        Uint16 rspStatusCode;

        result = scu->sendSTORERequest(presID, nullptr, dataset, rspStatusCode);

        if (result.bad()) {
            GERROR("dcmtk store request status (%d) failed to send image to server: %s\n", rspStatusCode,
                   result.text());
            break;
        }

        out.push(std::move(data));
    }

    scu->closeAssociation(DCMSCU_RELEASE_ASSOCIATION);
    delete scu;

    if (pTunnel) {
        pTunnel->stop();
    }
}
GADGETRON_GADGET_EXPORT(DicomSend)
} /* namespace Gadgetron */
