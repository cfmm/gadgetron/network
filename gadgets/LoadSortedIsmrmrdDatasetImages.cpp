//
//  LoadSortedIsmrmrdDatasetImages.cpp
//  network
//
//  Created by Martyn Klassen on 2016-02-19.
//  Copyright © 2016 CFMM. All rights reserved.
//

#include "LoadSortedIsmrmrdDatasetImages.hpp"

namespace Gadgetron {
bool LoadSortedIsmrmrdDatasetImages::configure(ISMRMRD::DatasetExt& dataset) { return this->loadHeaders(dataset); }

bool LoadSortedIsmrmrdDatasetImages::loadHeaders(ISMRMRD::DatasetExt& dataset) {
    ISMRMRD::ISMRMRD_ImageHeader header;
    int res;
    uint32_t iImages = dataset.getNumberOfImages(varname);

    for (uint32_t i = 0; i < iImages; i++) {
        // Read the header
        try {
            res = dataset.readImageHeader(varname, i, header);
        } catch (std::exception& ex) {
            GERROR("Error reading image %d of %d from %s: %s\n", i, iImages, varname.c_str(), ex.what());
            return false;
        }

        if (res) {
            headers.emplace_back(i, header);
        } else
            return -1;
    }

    this->sortHeaders();
    return true;
}

bool LoadSortedIsmrmrdDatasetImages::comparePair(const PairType& pair1, const PairType& pair2) {
    const ISMRMRD::ISMRMRD_ImageHeader& h1(pair1.second);
    const ISMRMRD::ISMRMRD_ImageHeader& h2(pair2.second);

    return (h1.average < h2.average) || (h1.slice < h2.slice) || (h1.contrast < h2.contrast) || (h1.set < h2.set) ||
           (h1.phase < h2.phase) || (h1.repetition < h2.repetition) || (h1.image_index < h2.image_index) ||
           (h1.image_series_index < h2.image_series_index);
}

void LoadSortedIsmrmrdDatasetImages::sortHeaders() { std::stable_sort(headers.begin(), headers.end(), comparePair); }
GADGETRON_GADGET_EXPORT(LoadSortedIsmrmrdDatasetImages)
} // namespace Gadgetron
