#include "SaveIsmrmrdDataset.hpp"
#include "SshGadget.hpp"
#include "exec.hpp"
#include "pugixml.hpp"
#include <csignal>
#include <fcntl.h>
#include <thread>
#include <unistd.h>

namespace Gadgetron {
struct RndGen {
    explicit RndGen(char const* range = "abcdefghijklmnopqrstuvwxyz0123456789")
        : range(range), len(static_cast<int>(std::strlen(range))), randomDevice(), generator(randomDevice()) {}

    char operator()() {
        std::uniform_int_distribution<> distribution(0, len);
        return range[distribution(generator)];
    }

  private:
    char const* range;
    int len;
    std::random_device randomDevice;
    std::mt19937 generator;
};

void makeUnique(std::string& tag) {
    // Get the current time
    std::time_t rawTime;
    std::time(&rawTime);
    std::tm* timeInfo = std::localtime(&rawTime);
    char buffer[1024];

    // Build a custom tag for the mountpoint
    // Use current time to second resolution
    std::strftime(buffer, 1024, "%Y%m%d.%H%M%S.", timeInfo);

    // Append a 10 character random string
    char* ptrBuffer = buffer + strlen(buffer);
    std::generate_n(ptrBuffer, 10, RndGen());
    ptrBuffer[10] = '\0';

    tag = buffer;
}

SaveIsmrmrdDataset::SaveIsmrmrdDataset(const Core::Context& context, const Core::GadgetProperties& properties)
    : Core::ChannelGadget<Core::AnyData>::ChannelGadget(context, properties), context(context) {}

int SaveIsmrmrdDataset::mount(std::string const& remote, std::string const& workingDir) {
    this->unmount();
    mountPath.clear();

    if (privatekey.empty() || mountpoint.empty() || remote.empty() || username.empty()) {
        GDEBUG("Remote mounting parameters are empty\n");
        return 1;
    }
    // Check that the privateKey exists and is readable
#pragma clang diagnostic push
#pragma ide diagnostic ignored "SignedBitwise"
    if (access(privatekey.c_str(), R_OK)) // NOLINT(hicpp-signed-bitwise)
    {
        GERROR("Unable to access private key %s\n", privatekey.c_str());
        return 1;
    }
#pragma clang diagnostic pop
    // Check that the sshfs command exists
    if (!std::filesystem::exists(sshfs_cmd.c_str())) {
        GERROR("SSHFS command %s does not exist\n", sshfs_cmd.c_str());
        return 1;
    }
    // Check the ssh command exists
    if (!std::filesystem::exists(ssh_cmd.c_str())) {
        GERROR("SSH command %s does not exist\n", ssh_cmd.c_str());
        return 1;
    }

    std::string tag;
    makeUnique(tag);

    // Create the mountpoint within the working directory
    mountPath = std::filesystem::path(workingDir + mountpoint + "." + tag);

    if (!std::filesystem::is_directory(mountPath)) {
        if (std::filesystem::exists(mountPath)) {
            std::filesystem::remove(mountPath);
        }
        if (!std::filesystem::create_directory(mountPath)) {
            GERROR("Unable to create %s\n", mountPath.c_str());
            mountPath.clear();
            return 1;
        }
    }

    // Scope the mounting so the unencrypted private key gets deleted on dropping out of scope
    {
        PublicPrivateKey key(privatekey, password);

        // Use system() to create the sshfs mount
        std::stringstream ss;
        ss << sshfs_cmd << " " << username << "@" << remote << " " << mountPath << " -o idmap=user"
           << ",identityfile=" << key.getPrivateKeyFile() << ",StrictHostKeyChecking=no"
           << ",ConnectTimeout=" << sshfs_timeout << ",ssh_command='" << ssh_cmd << "'";

        std::string tmp = ss.str();

        int retCode;
        std::string retStdout;
        tie(retCode, retStdout) = this->execTimeout(tmp, sshfs_timeout);

        if (retCode) {
            GERROR("%s failed with %d\n", ss.str().c_str(), retCode);
            this->unmount();
            std::filesystem::remove(mountPath);
            mountPath.clear();
            return retCode;
        } else
            GINFO("Mounted %s to %s\n", remote.c_str(), mountPath.c_str());
    }

    return 0;
}

std::tuple<int, std::string> SaveIsmrmrdDataset::unmount(std::filesystem::path const& path) {
    std::stringstream cmd;
    cmd << "/bin/fusermount -uz " << path.string();
    return {std::system(cmd.str().c_str()), cmd.str()};
}

void SaveIsmrmrdDataset::unmount() {
    if (!mountPath.empty()) {
        GINFO("Unmounting %s\n", mountPath.c_str());
        int retCode;
        std::string cmd;
        std::tie(retCode, cmd) = unmount(mountPath);
        if (retCode)
            GERROR("%s failed with %d\n", cmd.c_str(), retCode);
        else
            GINFO("Unmounted %s\n", mountPath.c_str());

        // Each instance appends a random string, so the mountpoint needs to be deleted
        std::error_code ec;
        std::filesystem::remove(mountPath, ec);
        if (ec) {
            GERROR("Failed to remove %s: %s\n", mountPath.c_str(), ec.message().c_str());
        }
    }
}

std::string SaveIsmrmrdDataset::getVarname(const Core::optional<ISMRMRD::MetaContainer>& meta,
                                           const ISMRMRD::ImageHeader& header) {
    return varname;
}

std::tuple<int, std::string> SaveIsmrmrdDataset::execTimeout(std::string& cmd, long timeout) {
    return ::execTimeout(cmd, timeout);
}

void SaveIsmrmrdDataset::process(Core::InputChannel<Core::AnyData>& in, Core::OutputChannel& out) {
    makeUniqueFilled(remotemounts);

    if (remotemounts.empty()) {
        GERROR_STREAM("remotemounts parameter is empty");
        throw std::invalid_argument("Bad remotemounts parameter - empty");
    }

    for (auto const& remotemount : remotemounts) {
        if (0 == this->mount(remotemount, context.paths.working_folder.string()))
            break;
    }

    if (mountPath.empty()) {
        GERROR_STREAM("No remote mounts were successful.");
        throw std::invalid_argument("Bad remotemounts parameter - failure");
    }

    relativeFilePath = std::filesystem::path(filename);
    if (uniquename) {
        std::string prefix;
        makeUnique(prefix);
        prefix.append(".");
        relativeFilePath = relativeFilePath.parent_path() / (prefix + relativeFilePath.filename().string());
    }

    // Delete any existing file
    std::filesystem::remove(mountPath / relativeFilePath);

    // Create a dataset to which to save images / acquisitions / waveforms
    // scope dataset object so that it is deleted and closed before unmounting
    {
        // Create the new file
        ISMRMRD::DatasetExt dataset((mountPath / relativeFilePath).c_str(), groupname.c_str(), true);

        dataset.encryption(passphrase, authentication);

        if (uniquename) {
            out.push(std::move(SshFile(mountPath / relativeFilePath)));
        }

        std::stringstream xml;
        serialize(this->header, xml);
        std::string xmlString = xml.str();
        dataset.writeHeader(xmlString);

        for (auto data : in) {
            auto status = visit(
                [this, &dataset](const auto& data) mutable -> bool { return this->appendData(data, dataset); }, data);
            if (!status) {
                GERROR("Failed to append data\n");
            }
            out.push(std::move(data));
        }
        postProcess(dataset);
    }
    GDEBUG("Dataset filepath: %s\n", (mountPath / relativeFilePath).c_str());
    // Unmount shared location
    this->unmount();
    // cleanup should be used for functions that need a gurantee that the dataset has been written to the sshfs
    // server's disk.
    this->cleanup();
}

bool SaveIsmrmrdDataset::appendData(Core::AnyImage const& img, ISMRMRD::DatasetExt& dataset) {
    return visit([this, &dataset](const auto& img) mutable -> bool { return this->appendImage(img, dataset); }, img);
}

bool SaveIsmrmrdDataset::appendData(Core::Acquisition const& acq, ISMRMRD::DatasetExt& dataset) {
    WrappedAcquisition rawAcq(acq);
    try {
        dataset.appendAcquisition(rawAcq);
    } catch (std::exception& ex) {
        GERROR("Failed to append acquisition: %s\n", ex.what());
        return false;
    }
    return true;
}

bool SaveIsmrmrdDataset::appendData(Core::Waveform const& waveform, ISMRMRD::DatasetExt& dataset) {
    WrappedWaveform rawWaveform(waveform);
    try {
        dataset.appendWaveform(rawWaveform);
    } catch (std::exception& ex) {
        GERROR("Failed to append waveform: %s\n", ex.what());
        return false;
    }
    return true;
}

GADGETRON_GADGET_EXPORT(SaveIsmrmrdDataset)
} /* namespace Gadgetron */
