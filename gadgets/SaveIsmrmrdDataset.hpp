#pragma once

#include "Node.h"
#include "cfmm_gadget_exports.hpp"
#include <tuple>

#include "Cryptography.hpp"
#include "DatasetExtension.hpp"
#include "core/Types.hpp"
#include "hoNDArray.h"
#include "mri_core_data.h"
#include <filesystem>
#include <ismrmrd/ismrmrd.h>
#include <ismrmrd/meta.h>
#include <ismrmrd/xml.h>
#include <unordered_set>
#include <vector>

namespace Gadgetron {
class EXPORTCFMMGADGETS SaveIsmrmrdDataset : public Core::ChannelGadget<Core::AnyData> {
  public:
    SaveIsmrmrdDataset(const Core::Context& context, const Core::GadgetProperties& properties);
    void process(Core::InputChannel<Core::AnyData>& in, Core::OutputChannel& out) override;
    static void makeUniqueFilled(std::vector<std::string>& values) {
        std::unordered_set<std::string> s;
        auto end = std::remove_if(values.begin(), values.end(),
                                  [&s](std::string const& i) { return i.empty() || !s.insert(i).second; });

        values.erase(end, values.end());
    }

  protected:
    NODE_PROPERTY(filename, std::string, "Name of ISMRMRD Dataset file", "data.ismrmrd");
    NODE_PROPERTY(groupname, std::string, "Name of group in ISMRMRD Dataset file", "dataset");
    NODE_PROPERTY(varname, std::string, "Name of variable within group in ISMRMRD Dataset file (only for images)",
                  "images");
    NODE_PROPERTY(username, std::string, "Username on remote mount", "");
    std::vector<std::string> remotemounts =
        this->get_property<std::vector<std::string>>("remotemounts", {}, "List of paths in priority order");
    NODE_PROPERTY(mountpoint, std::string, "Directory within workingDirectory on which to mount remote filesystem",
                  "remoteFilesystem");
    NODE_PROPERTY(privatekey, std::string, "Private key for remote sshfs mounting", "");
    NODE_PROPERTY(password, std::string, "Private key password for remote sshfs mounting", "");
    NODE_PROPERTY(ssh_cmd, std::string, "Absolute path to SSH command to use for mounting", "/usr/bin/ssh");
    NODE_PROPERTY(sshfs_cmd, std::string, "Absolute path to SSHFS command to use for mounting", "/usr/bin/sshfs");
    NODE_PROPERTY(sshfs_timeout, long, "Timeout in seconds when connecting to SSH server", 5);

    NODE_PROPERTY(passphrase, std::string, "Encryption passphrase", "");
    NODE_PROPERTY(authentication, std::string, "Data authentication phrase", "gadgetron");
    NODE_PROPERTY(uniquename, bool, "Prefix filename with unique timestamp", false);

  protected:
    Core::Context const& context;
    virtual std::tuple<int, std::string> execTimeout(std::string& cmd, long timeout);
    std::filesystem::path relativeFilePath;
    virtual std::string getVarname(const Core::optional<ISMRMRD::MetaContainer>& meta,
                                   const ISMRMRD::ImageHeader& header);

    bool appendData(Core::AnyImage const& img, ISMRMRD::DatasetExt& dataset);
    static bool appendData(Core::Acquisition const& acq, ISMRMRD::DatasetExt& dataset);
    static bool appendData(Core::Waveform const& waveform, ISMRMRD::DatasetExt& dataset);
    template <typename T> bool appendImage(const Core::Image<T>& image, ISMRMRD::DatasetExt& dataset);

    virtual void postProcess(ISMRMRD::DatasetExt const& dataset) {}
    virtual void cleanup() {}
    std::filesystem::path mountPath;

  private:
    virtual int mount(std::string const& remote, std::string const& workingDir);
    virtual void unmount();
    static std::tuple<int, std::string> unmount(std::filesystem::path const& path);

    template <typename T> class WrappedImage : public ISMRMRD::Image<T> {
      public:
        using ISMRMRD::Image<T>::Image;
        explicit WrappedImage(const Core::Image<T>& image) : ISMRMRD::Image<T>() {
            auto& [hdr, data, meta] = image;

            ISMRMRD::Image<T>::im.head = hdr;
            ISMRMRD::Image<T>::im.data = const_cast<T*>(data.get_data_ptr());
            if (meta) {
                std::stringstream ss;
                ISMRMRD::serialize(*meta, ss);
                auto xml = ss.str();
                ISMRMRD::Image<T>::im.head.attribute_string_len = xml.size();
                ISMRMRD::Image<T>::im.attribute_string = (char*)malloc(xml.size() + 1);
                memcpy(ISMRMRD::Image<T>::im.attribute_string, xml.c_str(), xml.size());
                ISMRMRD::Image<T>::im.attribute_string[xml.size()] = '\0';
            } else {
                ISMRMRD::Image<T>::im.attribute_string = nullptr;
            }
        }

        virtual ~WrappedImage() { ISMRMRD::Image<T>::im.data = nullptr; }
    };

    class WrappedAcquisition : public ISMRMRD::Acquisition {
      public:
        using ISMRMRD::Acquisition::Acquisition;
        explicit WrappedAcquisition(Core::Acquisition const& acq) : ISMRMRD::Acquisition() {
            auto& [hdr, data, traj] = acq;
            ISMRMRD::Acquisition::acq.head = hdr;
            ISMRMRD::Acquisition::acq.data = const_cast<complex_float_t*>(data.get_data_ptr());
            if (traj) {
                ISMRMRD::Acquisition::acq.traj = const_cast<float*>(traj->get_data_ptr());
            } else {
                ISMRMRD::Acquisition::acq.traj = nullptr;
            }
        }

        virtual ~WrappedAcquisition() {
            ISMRMRD::Acquisition::acq.data = nullptr;
            ISMRMRD::Acquisition::acq.traj = nullptr;
        }
    };

    class WrappedWaveform : public ISMRMRD::Waveform {
      public:
        using ISMRMRD::Waveform::Waveform;
        explicit WrappedWaveform(Core::Waveform const& wvfrm) : ISMRMRD::Waveform() {
            auto& [hdr, data] = wvfrm;
            ISMRMRD::Waveform::head = hdr;
            ISMRMRD::Waveform::data = const_cast<uint32_t*>(data.get_data_ptr());
        }

        virtual ~WrappedWaveform() { ISMRMRD::Waveform::data = nullptr; }
    };
};

template <typename T> bool SaveIsmrmrdDataset::appendImage(const Core::Image<T>& image, ISMRMRD::DatasetExt& dataset) {
    WrappedImage<T> rawImage(image);
    auto const& meta = std::get<Core::optional<ISMRMRD::MetaContainer>>(image);
    try {
        dataset.appendImage(getVarname(meta, rawImage.getHead()), rawImage);
    } catch (std::exception& ex) {
        GERROR("Failed to append image: %s\n", ex.what());
        return false;
    }
    return true;
}

} /* namespace Gadgetron */
