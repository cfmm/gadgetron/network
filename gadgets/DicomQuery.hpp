//
//  DicomQuery.h
//  network
//
//  Created by Martyn Klassen on 2015-05-21.
//  Copyright (c) 2015 CFMM. All rights reserved.
//

#pragma once

#include "Node.h"
#include "cfmm_gadget_exports.hpp"
#include <dcmtk/dcmdata/dctag.h>

namespace Gadgetron {

class EXPORTCFMMGADGETS DicomQuery : public Core::GenericChannelGadget {
  public:
    typedef std::map<DcmTag, OFString> DataMap;
    typedef std::vector<DataMap> DataMaps;

    DicomQuery(const Core::Context& context, const Core::GadgetProperties& properties);

    void process(Core::GenericInputChannel& in, Core::OutputChannel& out) final {
        for (auto msg : in)
            out.push(std::move(msg));
    }

  protected:
    std::string peerHostName = this->get_property<std::string>("peerHostName", std::string("server.example.invalid"),
                                                               "Address for C-Find SCP host");
    std::string peerAETitle =
        this->get_property<std::string>("peerAETitle", "aetitle", "Application Entity Title on host");
    std::string callingAETitle =
        this->get_property<std::string>("callingAETitle", CALLINGAET, "Calling Application Entity Title");
    uint16_t peerPort = this->get_property<uint16_t>("peerPort", 11112, "Port on host");
};

} /* namespace Gadgetron */
