//
//  DicomSend.h
//  network
//
//  Created by Martyn Klassen on 2015-05-05.
//  Copyright (c) 2015 CFMM. All rights reserved.
//

#pragma once

#include "cfmm_gadget_exports.hpp"
#include <dcmtk/dcmtls/tlsscu.h>

#include "Node.h"

namespace Gadgetron {

class EXPORTCFMMGADGETS DicomSend
    : public Core::ChannelGadget<DcmFileFormat, Core::optional<std::string>, Core::optional<ISMRMRD::MetaContainer>> {
  public:
    using Core::ChannelGadget<DcmFileFormat, Core::optional<std::string>,
                              Core::optional<ISMRMRD::MetaContainer>>::ChannelGadget;

    void
    process(Core::InputChannel<DcmFileFormat, Core::optional<std::string>, Core::optional<ISMRMRD::MetaContainer>>& in,
            Core::OutputChannel& out) override;

  protected:
    NODE_PROPERTY(peerHostName, std::string, "Address for C-STORE SCP host", "server.invalid.org");
    NODE_PROPERTY(peerPort, uint16_t, "Port on host", 11112);
    NODE_PROPERTY(peerAETitle, std::string, "Application Entity Title on host", "aetitle");
    NODE_PROPERTY(callingAETitle, std::string, "Calling Application Entity Title", CALLINGAET);

    NODE_PROPERTY(dcmCertificateFiles, std::vector<std::string>, "TLS Certificate Files", {});
    NODE_PROPERTY(dcmUsername, std::string, "DICOM User Identity username", "dicom");
    NODE_PROPERTY(dcmPassword, std::string, "DICOM User Identity password", "");
    NODE_PROPERTY(dcmPrivateKey, std::string, "Filename of private key for authentication", "");
    NODE_PROPERTY(dcmCertFile, std::string, "Filename of certificate for authentication", "");
    NODE_PROPERTY(dcmPassphrase, std::string, "Passphrase for private key", "");

    // Properties for handling SSH tunnel
    NODE_PROPERTY(username, std::string, "Username on forwarding host", "");
    NODE_PROPERTY(privateKey, std::string, "Path to private key file for forwarding host authentication", "");
    NODE_PROPERTY(passphrase, std::string, "Passphrase to decrypt private key file", "");
    NODE_PROPERTY(localHost, std::string, "Local host", "127.0.0.1");
    NODE_PROPERTY(localPort, uint16_t, "Local port to forward", 11112);
    NODE_PROPERTY(forwardHost, std::string, "Forwarding host", "tunnel.invalid.org");
    NODE_PROPERTY(forwardPort, std::string, "SSH port on forwarding host", "22");
};

} /* namespace Gadgetron */
