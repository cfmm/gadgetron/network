//
// Created by Martyn Klassen on 2024-06-13.
//
#pragma once

#include "Types.h"

namespace Gadgetron::Core {

using AnyData = std::variant<Waveform, Acquisition, AnyImage>;

}
