//
//  DicomSave.cpp
//  network
//
//  Created by Sahar Rabinoviz on 2015-07-17.
//  Copyright (c) 2015 CFMM. All rights reserved.
//

#include "DicomSave.hpp"

namespace Gadgetron {

void DicomSave::process(
    Core::InputChannel<DcmFileFormat, Core::optional<std::string>, Core::optional<ISMRMRD::MetaContainer>>& in,
    Core::OutputChannel& out) {
    fsPath = std::filesystem::path(dicom_dir);

    if (!std::filesystem::is_directory(fsPath)) {
        if (!std::filesystem::create_directory(fsPath)) {
            GERROR("Unable to create %s\n", fsPath.string().c_str());
            return;
        }
    }

    for (auto data : in) {
        auto file = get<DcmFileFormat>(data);
        auto filename = get<Core::optional<std::string>>(data);

        if (filename.has_value() && !filename.value().empty()) {
            std::filesystem::path fname(fsPath);
            fname /= filename.value() + ".dcm";

            OFCondition res = file.saveFile(fname.string().c_str(), EXS_LittleEndianExplicit);

            if (res != EC_Normal) {
                GDEBUG("Dicom file failed to save code (%d), reason: (%s)\n", res.code(), res.text());
            }
        }

        out.push(std::move(data));
    }
}
GADGETRON_GADGET_EXPORT(DicomSave)
} // namespace Gadgetron
