
#include "LoadIsmrmrdDatasetImages.hpp"
#include <filesystem>

namespace Gadgetron {

void LoadIsmrmrdDatasetImages::process(Core::InputChannel<Core::AnyImage>& in, Core::OutputChannel& out) {
    if (!onclose) {
        processFile(out);
    }

    for (auto data : in) {
        if (forward)
            out.push(std::move(data));
    }

    if (onclose) {
        processFile(out);
    }
}

bool LoadIsmrmrdDatasetImages::processFile(Core::OutputChannel& out) {
    try {
        std::filesystem::path fname(filename);

        if (std::filesystem::exists(fname)) {
            ISMRMRD::DatasetExt dataset(fname.c_str(), groupname.c_str(), false);
            return loadConfig(dataset) && loadData(dataset, out);
        }

        GERROR_STREAM("Unable to open file " << fname);
    } catch (std::exception& e) {
        GERROR(e.what());
    }
    return false;
}

bool LoadIsmrmrdDatasetImages::loadConfig(ISMRMRD::DatasetExt& data) {
    data.encryption(passphrase, authentication);

    std::string msg;
    try {
        data.readHeader(msg);
    } catch (std::exception& ex) {
        GERROR("Failed to read xml header: %s\n", ex.what());
        return false;
    }

    deserialize(msg.c_str(), const_cast<ISMRMRD::IsmrmrdHeader&>(this->header));
    return true;
}

bool LoadIsmrmrdDatasetImages::loadData(ISMRMRD::DatasetExt& data, Core::OutputChannel& out) {
    // Get the number of images
    uint16_t datatype = data.datatype(varname);

    auto result = true;
    if (datatype != 0 && !this->configure(data)) {
        switch (datatype) {
        case ISMRMRD::ISMRMRD_USHORT:
            result = load<uint16_t>(data, out);
            break;
        case ISMRMRD::ISMRMRD_SHORT:
            result = load<int16_t>(data, out);
            break;
        case ISMRMRD::ISMRMRD_UINT:
            result = load<uint32_t>(data, out);
            break;
        case ISMRMRD::ISMRMRD_INT:
            result = load<int32_t>(data, out);
            break;
        case ISMRMRD::ISMRMRD_FLOAT:
            result = load<float>(data, out);
            break;
        case ISMRMRD::ISMRMRD_DOUBLE:
            result = load<double>(data, out);
            break;
        case ISMRMRD::ISMRMRD_CXFLOAT:
            result = load<complex_float_t>(data, out);
            break;
        case ISMRMRD::ISMRMRD_CXDOUBLE:
            result = load<complex_double_t>(data, out);
            break;
        default:
            result = false;
        }
    }

    if (!result)
        GERROR("Unable to load images\n");

    return result;
}
GADGETRON_GADGET_EXPORT(LoadIsmrmrdDatasetImages)
} /* namespace Gadgetron */
