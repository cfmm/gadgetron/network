#pragma clang diagnostic push
#pragma ide diagnostic ignored "readability-identifier-naming"
#pragma once

#include "Node.h"

#include "SaveIsmrmrdDataset.hpp"
#include "cfmm_gadget_exports.hpp"
#include <map>

namespace Gadgetron {
class EXPORTCFMMGADGETS SaveB1IsmrmrdDatasetImages : public SaveIsmrmrdDataset {
  public:
    using SaveIsmrmrdDataset::SaveIsmrmrdDataset;

    void process(Core::InputChannel<Core::AnyData>& in, Core::OutputChannel& out) override;

  protected:
    NODE_PROPERTY(webapp_url, std::string, "Fieldmap web app url", "");
    // don't use NODE_PROPERTY so that webapp_urls is NOT constant
    // NOLINTNEXTLINE(readability-identifier-naming)
    std::vector<std::string> webapp_urls =
        this->get_property<std::vector<std::string>>("webapp_urls", {}, "Primary and fallback fieldmap web app urls");
    NODE_PROPERTY(webapp_auth_code, std::string, "Fieldmap web app authorization code", "");
    NODE_PROPERTY(http_timeout, long, "CURLOPT_TIMEOUT - maximum time in seconds the request is allowed to take", 5);

    std::string getVarname(const Core::optional<ISMRMRD::MetaContainer>& meta,
                           const ISMRMRD::ImageHeader& header) override;

    void cleanup() override;

    virtual std::tuple<long, std::string, int>
    postResults(std::string const& url, std::vector<std::string> const& headers, std::string const& body);
};
} /* namespace Gadgetron */

#pragma clang diagnostic pop