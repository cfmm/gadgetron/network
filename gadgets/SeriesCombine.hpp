//
//  SeriesCombine.h
//  network
//
//  Created by Martyn Klassen on 2015-05-27.
//  Copyright (c) 2015 CFMM. All rights reserved.
//

#pragma once

#include "cfmm_gadget_exports.hpp"

#include "Node.h"
#include <ismrmrd/ismrmrd.h>

namespace Gadgetron {
namespace looping {
typedef enum LoopDim {
    COL = 0,
    LIN,
    CHA,
    SET,
    ECO,
    PHS,
    REP,
    SEG,
    PAR,
    SLC,
    IDA,
    IDB,
    IDC,
    IDD,
    IDE,
    AVE,
    INVALID_DIM
} LoopDim;
} // namespace looping

#define BIT(n) (1u << static_cast<unsigned int>(n))

[[maybe_unused]] constexpr unsigned long inrNoCol = BIT(looping::COL);
[[maybe_unused]] constexpr unsigned long inrNoLin = BIT(looping::LIN);
[[maybe_unused]] constexpr unsigned long inrNoCha = BIT(looping::CHA);
[[maybe_unused]] constexpr unsigned long inrNoSet = BIT(looping::SET);
[[maybe_unused]] constexpr unsigned long inrNoEco = BIT(looping::ECO);
[[maybe_unused]] constexpr unsigned long inrNoPhs = BIT(looping::PHS);
[[maybe_unused]] constexpr unsigned long inrNoRep = BIT(looping::REP);
[[maybe_unused]] constexpr unsigned long inrNoSeg = BIT(looping::SEG);
[[maybe_unused]] constexpr unsigned long inrNoPar = BIT(looping::PAR);
[[maybe_unused]] constexpr unsigned long inrNoSlc = BIT(looping::SLC);
[[maybe_unused]] constexpr unsigned long inrNoIda = BIT(looping::IDA);
[[maybe_unused]] constexpr unsigned long inrNoIdb = BIT(looping::IDB);
[[maybe_unused]] constexpr unsigned long inrNoIdc = BIT(looping::IDC);
[[maybe_unused]] constexpr unsigned long inrNoIdd = BIT(looping::IDD);
[[maybe_unused]] constexpr unsigned long inrNoIde = BIT(looping::IDE);
[[maybe_unused]] constexpr unsigned long inrNoAve = BIT(looping::AVE);
[[maybe_unused]] constexpr unsigned long inrUseAll = ((1u << (looping::INVALID_DIM)) - 1u);

class EXPORTCFMMGADGETS SeriesCombine : public Core::ChannelGadget<Core::AnyImage> {
  public:
    using Core::ChannelGadget<Core::AnyImage>::ChannelGadget;
    void process(Core::InputChannel<Core::AnyImage>& in, Core::OutputChannel& out) override;

  protected:
    NODE_PROPERTY(seriesCombination, unsigned long, "Flag specifying combined dimensions", inrUseAll);
};

} /* namespace Gadgetron */
