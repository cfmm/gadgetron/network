//
//  cfmm_gadget_exports.h
//  network
//
//  Created by Martyn Klassen on 2015-03-23.
//  Copyright (c) 2015 CFMM. All rights reserved.
//

#pragma once

#if defined(WIN32)
#if defined(__BUILD_CFMM_EXPORTS__)
#define EXPORTCFMMGADGETS __declspec(dllexport)
#else
#define EXPORTCFMMGADGETS __declspec(dllimport)
#endif
#else
#define EXPORTCFMMGADGETS
#endif

// Define some DICOM specific values
#define CALLINGAET "GADGETRON"
