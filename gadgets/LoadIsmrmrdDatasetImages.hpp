#pragma once

#include "Node.h"

#include "cfmm_gadget_exports.hpp"

#include "Cryptography.hpp"
#include "DatasetExtension.hpp"
#include "hoNDArray.h"
#include <ismrmrd/meta.h>

namespace Gadgetron {

class EXPORTCFMMGADGETS LoadIsmrmrdDatasetImages : public Core::ChannelGadget<Core::AnyImage> {
  public:
    using Core::ChannelGadget<Core::AnyImage>::ChannelGadget;
    void process(Core::InputChannel<Core::AnyImage>& in, Core::OutputChannel& out) override;

  protected:
    NODE_PROPERTY(filename, std::string, "Name of ISMRMRD Dataset file", "images.ismrmrd");
    NODE_PROPERTY(groupname, std::string, "Name of group in ISMRMRD Dataset file", "data");
    NODE_PROPERTY(varname, std::string, "Name of variable within group in ISMRMRD Dataset file", "images");
    NODE_PROPERTY(passphrase, std::string, "Encryption passphrase", "");
    NODE_PROPERTY(authentication, std::string, "Data authentication phrase", "gadgetron");
    NODE_PROPERTY(forward, bool, "Forward other received messages", true);
    NODE_PROPERTY(onclose, bool, "Load the data on close", false);

  private:
    template <typename T> bool load(ISMRMRD::DatasetExt& dataset, Core::OutputChannel& out);

  protected:
    virtual bool processFile(Core::OutputChannel& out);

    virtual bool loadConfig(ISMRMRD::DatasetExt& data);

    virtual bool loadData(ISMRMRD::DatasetExt& data, Core::OutputChannel& out);

    virtual bool configure(ISMRMRD::DatasetExt& dataset) { return true; }
};

template <typename T> bool LoadIsmrmrdDatasetImages::load(ISMRMRD::DatasetExt& dataset, Core::OutputChannel& out) {
    // Loop through all the images
    ISMRMRD::Image<T> image;

    uint32_t images = dataset.getNumberOfImages(varname);

    for (uint32_t i = 0; i < images; i++) {
        // Read the image
        try {
            dataset.readImage(varname, i, image);
        } catch (std::exception& ex) {
            GERROR("Error reading image %d of %d from %s: %s\n", i, images, varname.c_str(), ex.what());
            return false;
        }

        // Unfortunately there hoNDArray uses new/delete and ISMRMRD::ImageHeader used malloc/free
        // The two methods cannot be mixed. Although you can overload to malloc/free, the move into
        // the Core::Image results in the overloaded type being moved into an hoNDArray which
        // then tries to use delete. The only solution is to move the memory manually.
        ISMRMRD::ImageHeader& hdr = image.getHead();
        auto data = hoNDArray<T>(hdr.matrix_size[0], hdr.matrix_size[1], hdr.matrix_size[2], hdr.channels);
        memccpy(data.get_data_ptr(), image.getDataPtr(), sizeof(T), data.size());

        if (image.getAttributeStringLength()) {
            ISMRMRD::MetaContainer meta;
            deserialize(image.getAttributeString(), meta);
            out.push(std::move(Core::Image<T>(image.getHead(), std::move(data), std::move(meta))));
        } else {
            out.push(
                std::move(Core::Image<T>(image.getHead(), std::move(data), std::optional<ISMRMRD::MetaContainer>())));
        }
    }

    return true;
}

} /* namespace Gadgetron */
