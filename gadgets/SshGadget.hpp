//
// Created by Martyn Klassen on 2019-03-01.
//

#pragma once

#include "Node.h"

#include "cfmm_gadget_exports.hpp"
#include <SshTaskInterface.hpp>

namespace Gadgetron {
class SshFile {
  public:
    explicit SshFile(std::string filename) : fileName(std::move(filename)) {}
    std::string fileName;
};

template <class SshTask> class EXPORTCFMMGADGETS SshGadget : public Core::ChannelGadget<SshFile> {
    static_assert(std::is_base_of<SshTaskInterface, SshTask>::value, "SshTask must be a subclass of SshTaskInterface");

  public:
    using Core::ChannelGadget<SshFile>::ChannelGadget;

  protected:
    void process(Core::InputChannel<SshFile>& in, Core::OutputChannel& out) override {
        SshTask task;

        task.setWorkingDirectory(remoteDirectory);
        if (!task.connect(remoteHost, remotePort, userName, privateKey, password))
            return;

        for (auto file : in) {
            task.process(file.fileName);
            out.push(std::move(file));
        }

        task.disconnect();
    }

    [[maybe_unused]] virtual void configure(SshTask& task) = 0;

  protected:
    NODE_PROPERTY(remoteHost, std::string, "Remote host", "server.invalid.org");
    NODE_PROPERTY(remotePort, std::string, "SSH port on remote host", "22");
    NODE_PROPERTY(userName, std::string, "Username on remote host", "");
    NODE_PROPERTY(privateKey, std::string, "Path to private key file for remote host authentication", "");
    NODE_PROPERTY(password, std::string, "Passphrase to decrypt private key file", "");
    NODE_PROPERTY(remoteDirectory, std::string, "Directory on remote host", "/tmp");
};

} /* namespace Gadgetron */
