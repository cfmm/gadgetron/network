//
// Created by akuurstr on 06/11/23.
//
#include "CopyFile.hpp"
#include "JobSubmit.hpp"
#include "Node.h"

namespace Gadgetron {
GADGETRON_GADGET_EXPORT(JobSubmit)
GADGETRON_GADGET_EXPORT(CopyFile)
} // namespace Gadgetron
