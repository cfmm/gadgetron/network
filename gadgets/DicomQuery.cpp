//
//  DicomQuery.cpp
//  network
//
//  Created by Martyn Klassen on 2015-05-21.
//  Copyright (c) 2015 CFMM. All rights reserved.
//

#include "DicomQuery.hpp"
#include "pugixml.hpp"
#include <ismrmrd/xml.h>

#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wconversion"
#pragma clang diagnostic ignored "-Wdeprecated-register"

#include <dcmtk/dcmdata/dcdeftag.h>
#include <dcmtk/dcmnet/dccfuidh.h>
#include <dcmtk/dcmnet/dfindscu.h>

#pragma clang diagnostic pop

#define TIMEOUT 5

/*
 Maximum 64 characters
 0000000000111111111122222222223333333333444444444455555555556666
 0123456789012345678901234567890123456789012345678901234567890123

 Siemens StudyInstanceUID
 "1.3.12.2.1107.5." SYSTEM_TYPE (X.XX) "." SERIAL_NUMBER (XXXXX) ".300000" YYMMDD?????????????????

 Example StudyInstanceUID
 1.3.12.2.1107.5.2.34.18932.30000015031013175378100000007
 1.3.12.2.1107.5.2.43.67007.30000015051115314639000000010

 Siemens SeriesInstanceUID
 "1.3.12.2.1107.5." SYSTEM_TYPE (X.XX) "." SERIAL_NUMBER (XXXXX) "." YYmmddHHMMSSuuuuuu????? ".0.0.0"

 Example SeriesInstanceUID
 1.3.12.2.1107.5.2.34.18932.2015031013574474917808040.0.0.0
                            YYYYMMDDhhmmssuuuuuuXXXXX

 Siemens SOPInstanceUID
 "1.3.12.2.1107.5." SYSTEM_TYPE (X.XX) "." SERIAL_NUMBER (XXXXX) "." YYmmddHHMMSSuuuuuu?????

 Example SOPInstanceUID
 1.3.12.2.1107.5.2.43.67007.2015051117013250194498317
                            YYYYMMDDhhmmssuuuuuuXXXXX

 */

namespace Gadgetron {

static bool cmpDataMap(const DicomQuery::DataMap& first, const DicomQuery::DataMap& second) {
    // DICOM requires YYYYMMDD format for dates and HHMMSS.mmm for time
    // Therefore alphabetical sorting on date and then time will work

    // Compare date first
    int res = first.at(DCM_StudyDate).compare(second.at(DCM_StudyDate));

    // If dates are equal compare times
    if (!res)
        res = first.at(DCM_StudyTime).compare(second.at(DCM_StudyTime));

    return (res < 0);
}

static void populateHeader(ISMRMRD::IsmrmrdHeader& h, const DicomQuery::DataMap& dataMap, const DicomQuery::DataMap&) {
    auto studyInfo = ISMRMRD::StudyInformation();
    if (h.studyInformation)
        studyInfo = *h.studyInformation;

    if (!studyInfo.studyDate.is_present())
        studyInfo.studyDate = dataMap.at(DCM_StudyDate);

    if (!studyInfo.studyTime.is_present())
        studyInfo.studyTime = dataMap.at(DCM_StudyTime);

    if (!studyInfo.studyID.is_present())
        studyInfo.studyID = dataMap.at(DCM_StudyID);

    if (!studyInfo.accessionNumber.is_present()) {
        char* end;
        studyInfo.accessionNumber = std::strtol(dataMap.at(DCM_AccessionNumber).c_str(), &end, 10);
    }

    if (!studyInfo.referringPhysicianName)
        studyInfo.referringPhysicianName = dataMap.at(DCM_ReferringPhysicianName);

    if (!studyInfo.studyDescription.is_present())
        studyInfo.studyDescription = dataMap.at(DCM_StudyDescription);

    if (!studyInfo.studyInstanceUID.is_present())
        studyInfo.studyInstanceUID = dataMap.at(DCM_StudyInstanceUID);

    h.studyInformation = studyInfo;

    ISMRMRD::MeasurementInformation measInfo = ISMRMRD::MeasurementInformation();
    if (h.measurementInformation)
        measInfo = *h.measurementInformation;

#if 0
      // Currently these are not defined in ISMRMRD Library
      // Activate once they are added to the ISMRMRD Library
      
      if (!measInfo.performingPhysicianName.is_present())
         measInfo.performingPhysicianName = seriesInfo.at(DCM_PerformingPhysicianName).c_str();
      
      if (!measInfo.operatorsName.is_present())
         measInfo.operatorsName = seriesInfo.at(DCM_OperatorsName).c_str();
#endif

    h.measurementInformation = measInfo;
}

class DcmFindSCUCallbackQuery : public DcmFindSCUCallback {
  public:
    enum CbType { PATIENT, STUDY, SERIES, IMAGE };

    DcmFindSCUCallbackQuery(DcmFindSCUCallbackQuery::CbType type, DicomQuery::DataMaps* dataMaps)
        : cbType(type), dataMaps(dataMaps) {}

    void callback(T_DIMSE_C_FindRQ* request, int& responseCount, T_DIMSE_C_FindRSP* rsp,
                  DcmDataset* responseIdentifiers) override {
        OFString info;

        // Add a new map
        dataMaps->emplace_back();

        // Get the reference to the new map
        DicomQuery::DataMap& dataMap = dataMaps->back();

        // Collect data based on the type of query
        switch (cbType) {
        case PATIENT:
            // DICOM standard requires these fields
            responseIdentifiers->findAndGetOFString(DCM_PatientName, info);
            dataMap[DCM_PatientName] = info;
            responseIdentifiers->findAndGetOFString(DCM_PatientID, info);
            dataMap[DCM_PatientID] = info;

            // Optional parameters
            responseIdentifiers->findAndGetOFString(DCM_PatientSex, info);
            dataMap[DCM_PatientSex] = info;
            responseIdentifiers->findAndGetOFString(DCM_PatientBirthDate, info);
            dataMap[DCM_PatientBirthDate] = info;
            responseIdentifiers->findAndGetOFString(DCM_IssuerOfPatientID, info);
            dataMap[DCM_IssuerOfPatientID] = info;

            // Non-attributes
            responseIdentifiers->findAndGetOFString(DCM_NumberOfPatientRelatedStudies, info);
            dataMap[DCM_NumberOfPatientRelatedStudies] = info;
            responseIdentifiers->findAndGetOFString(DCM_NumberOfPatientRelatedSeries, info);
            dataMap[DCM_NumberOfPatientRelatedSeries] = info;
            responseIdentifiers->findAndGetOFString(DCM_NumberOfPatientRelatedInstances, info);
            dataMap[DCM_NumberOfPatientRelatedInstances] = info;

            break;
        case STUDY:
            // Dicom standard requires these fields
            responseIdentifiers->findAndGetOFString(DCM_StudyDate, info);
            dataMap[DCM_StudyDate] = info;
            responseIdentifiers->findAndGetOFString(DCM_StudyTime, info);
            dataMap[DCM_StudyTime] = info;
            responseIdentifiers->findAndGetOFString(DCM_AccessionNumber, info);
            dataMap[DCM_AccessionNumber] = info;
            responseIdentifiers->findAndGetOFString(DCM_StudyID, info);
            dataMap[DCM_StudyID] = info;
            responseIdentifiers->findAndGetOFString(DCM_StudyInstanceUID, info);
            dataMap[DCM_StudyInstanceUID] = info;

            // Optional parameters
            responseIdentifiers->findAndGetOFString(DCM_ReferringPhysicianName, info);
            dataMap[DCM_ReferringPhysicianName] = info;
            responseIdentifiers->findAndGetOFString(DCM_StudyDescription, info);
            dataMap[DCM_StudyDescription] = info;

            // Non-attributes
            responseIdentifiers->findAndGetOFString(DCM_NumberOfStudyRelatedInstances, info);
            dataMap[DCM_NumberOfStudyRelatedInstances] = info;
            responseIdentifiers->findAndGetOFString(DCM_NumberOfStudyRelatedSeries, info);
            dataMap[DCM_NumberOfStudyRelatedSeries] = info;
            responseIdentifiers->findAndGetOFString(DCM_ModalitiesInStudy, info);
            dataMap[DCM_ModalitiesInStudy] = info;
            responseIdentifiers->findAndGetOFString(DCM_SOPClassesInStudy, info);
            dataMap[DCM_SOPClassesInStudy] = info;

            break;
        case SERIES:
            // Dicom standard requires these fields
            responseIdentifiers->findAndGetOFString(DCM_Modality, info);
            dataMap[DCM_Modality] = info;
            responseIdentifiers->findAndGetOFString(DCM_SeriesNumber, info);
            dataMap[DCM_SeriesNumber] = info;
            responseIdentifiers->findAndGetOFString(DCM_SeriesInstanceUID, info);
            dataMap[DCM_SeriesInstanceUID] = info;

            // Optional parameters
            responseIdentifiers->findAndGetOFString(DCM_PerformingPhysicianName, info);
            dataMap[DCM_PerformingPhysicianName] = info;
            responseIdentifiers->findAndGetOFString(DCM_OperatorsName, info);
            dataMap[DCM_OperatorsName] = info;
            responseIdentifiers->findAndGetOFString(DCM_SeriesDescription, info);
            dataMap[DCM_SeriesDescription] = info;
            responseIdentifiers->findAndGetOFString(DCM_SeriesDate, info);
            dataMap[DCM_SeriesDate] = info;
            responseIdentifiers->findAndGetOFString(DCM_InstitutionName, info);
            dataMap[DCM_InstitutionName] = info;

            // Non-attributes
            responseIdentifiers->findAndGetOFString(DCM_NumberOfSeriesRelatedInstances, info);
            dataMap[DCM_NumberOfSeriesRelatedInstances] = info;

        case IMAGE:
            // Dicom standard requires these fields
            responseIdentifiers->findAndGetOFString(DCM_InstanceNumber, info);
            dataMap[DCM_InstanceNumber] = info;
            responseIdentifiers->findAndGetOFString(DCM_SOPInstanceUID, info);
            dataMap[DCM_SOPInstanceUID] = info;

            // Non-attributes
            responseIdentifiers->findAndGetOFString(DCM_AlternateRepresentationSequence, info);
            dataMap[DCM_AlternateRepresentationSequence] = info;

            break;
        }
    }

  private:
    CbType cbType;
    DicomQuery::DataMaps* dataMaps;
};

DicomQuery::DicomQuery(const Core::Context& context, const Core::GadgetProperties& properties)
    : Core::GenericChannelGadget(context, properties) {
    if (context.header.userParameters.has_value()) {
        auto userParameters = context.header.userParameters.get();
        auto parameterStrings = userParameters.userParameterString;

        for (auto const& result : parameterStrings) {
            if (result.name == "peerHost")
                const_cast<std::string&>(peerHostName) = result.value;

            if (result.name == "peerAETitle")
                const_cast<std::string&>(peerAETitle) = result.value;
        }
        for (auto const& result : userParameters.userParameterLong) {
            if (result.name == "peerPort")
                const_cast<uint16_t&>(peerPort) = result.value;
        }
    }

    ISMRMRD::IsmrmrdHeader h = context.header;

    DataMaps dataMaps;
    DataMap studyMap;
    DataMap seriesMap;

    // Must have PatientID in the header
    bool fakeStudy = true;
    bool fakeSeries = true;
    if (h.subjectInformation.is_present() && h.subjectInformation->patientID.is_present()) {

        DcmFindSCU findSCU;

#if 0
         dcmtk::log4cplus::Logger log =  dcmtk::log4cplus::Logger::getRoot();
         log.setLogLevel(dcmtk::log4cplus::OFF_LOG_LEVEL);
#endif

        findSCU.initializeNetwork(TIMEOUT);

        OFList<OFString> overrideKeys;

        // Lookup Study information for the patient
        overrideKeys.emplace_back("QueryRetrieveLevel=STUDY");
        overrideKeys.emplace_back(("PatientID=" + *h.subjectInformation->patientID).c_str());

        // List the information that we want to retrieve
        overrideKeys.emplace_back("StudyDate");
        overrideKeys.emplace_back("StudyTime");
        overrideKeys.emplace_back("AccessionNumber");
        overrideKeys.emplace_back("StudyID");
        overrideKeys.emplace_back("StudyInstanceUID");
        overrideKeys.emplace_back("NumberOfStudyRelatedSeries");

        // These might not exist
        overrideKeys.emplace_back("ReferringPhysicianName");
        overrideKeys.emplace_back("StudyDescription");

        const char* abstractSyntax = UID_FINDPatientRootQueryRetrieveInformationModel;

        auto callBack = new DcmFindSCUCallbackQuery(DcmFindSCUCallbackQuery::STUDY, &dataMaps);

        OFCondition result;

        result = findSCU.performQuery(peerHostName.c_str(), peerPort, callingAETitle.c_str(), peerAETitle.c_str(),
                                      abstractSyntax, EXS_Unknown, DIMSE_BLOCKING, 0, ASC_DEFAULTMAXPDU, OFFalse,
                                      OFFalse, 1, FEM_none, -1, &overrideKeys, callBack);

        // Clear the memory for the callBack
        delete callBack;

        if (result.good() && !dataMaps.empty()) {
            // Sort the studies from first to last according to date/time
            std::sort(dataMaps.begin(), dataMaps.end(), cmpDataMap);

            studyMap = dataMaps.back();
            fakeStudy = false;

            // Check if there are any other series already in existence
            char* end;
            if (std::strtol(studyMap[DCM_NumberOfStudyRelatedSeries].c_str(), &end, 10) > 0) {

                // There are some series specific values that would also be useful
                // This series, of course, does not yet exist, but we can look up the required
                // Values from other series, if they exist.
                overrideKeys.clear();
                overrideKeys.emplace_back("QueryRetrieveLevel=SERIES");
                overrideKeys.emplace_back("PatientID=" + studyMap[DCM_PatientID]);
                overrideKeys.emplace_back("StudyInstanceUID=" + studyMap[DCM_StudyInstanceUID]);
                overrideKeys.emplace_back("SeriesInstanceUID");

                // Add the tags that are desired
                overrideKeys.emplace_back("OperatorsName");
                overrideKeys.emplace_back("PerformingPhysicianName");

                // Clear the results
                dataMaps.clear();

                // Create a new callback
                callBack = new DcmFindSCUCallbackQuery(DcmFindSCUCallbackQuery::SERIES, &dataMaps);

                result =
                    findSCU.performQuery(peerHostName.c_str(), peerPort, callingAETitle.c_str(), peerAETitle.c_str(),
                                         abstractSyntax, EXS_Unknown, DIMSE_BLOCKING, 0, ASC_DEFAULTMAXPDU, OFFalse,
                                         OFFalse, 1, FEM_none, -1, &overrideKeys, callBack);

                // Release the callBack
                delete callBack;

                if (result.good() && !dataMaps.empty()) {
                    // Find a series with both fields of interest not null
                    // Otherwise use the first with a Performing physician name
                    // Otherwise use the first with an operator's name
                    auto iterStart = dataMaps.begin();
                    auto iterEnd = dataMaps.end();
                    DataMap* pSeriesMap = nullptr;
                    while (iterStart < iterEnd) {

                        if (pSeriesMap == nullptr && !iterStart->at(DCM_PerformingPhysicianName).empty())
                            pSeriesMap = &(*iterStart);

                        if (pSeriesMap == nullptr && !iterStart->at(DCM_OperatorsName).empty())
                            pSeriesMap = &(*iterStart);

                        if (!iterStart->at(DCM_OperatorsName).empty() &&
                            !iterStart->at(DCM_PerformingPhysicianName).empty()) {
                            pSeriesMap = &(*iterStart);
                            break;
                        }

                        iterStart++;
                    }

                    if (pSeriesMap != nullptr)
                        seriesMap = *pSeriesMap;
                    else
                        seriesMap = dataMaps.front();

                    fakeSeries = false;
                } else {
                    GINFO("No series found: %s\n", result.text());
                    GDEBUG("Search for StudyUID: %s\n", studyMap[DCM_StudyInstanceUID].c_str());
                }
            } else {
                GINFO("No series in StudyUID: %s\n", studyMap[DCM_StudyInstanceUID].c_str());
            }
        } else {
            GINFO("No studies found: %s\n", result.text());
            GDEBUG("Search for PatientID: %s\n", (*h.subjectInformation->patientID).c_str());
        }

        findSCU.dropNetwork();
    } else {
        GINFO("Missing patient information for DICOM Query\n");
    }

    if (fakeStudy) {
        char buffer[100];

        // Get the current time
        std::time_t rawTime;
        std::time(&rawTime);
        std::tm* timeInfo = std::localtime(&rawTime);

        // Populate with fake study data
        // Use today's date
        std::strftime(buffer, 100, "%Y%m%d", timeInfo);
        studyMap[DCM_StudyDate] = buffer;
        // Use current time to second resolution
        std::strftime(buffer, 100, "%H%M%S.000", timeInfo);
        studyMap[DCM_StudyTime] = buffer;

        // Use 0 for the StudyID and AccessionNumber
        studyMap[DCM_StudyID] = "0";
        studyMap[DCM_AccessionNumber] = "0";

        // Generate a unique StudyInstanceID
        studyMap[DCM_StudyInstanceUID] = dcmGenerateUniqueIdentifier(buffer, SITE_STUDY_UID_ROOT);

        // Leave the study description and referring physician name blank
        studyMap[DCM_StudyDescription] = "";
        studyMap[DCM_ReferringPhysicianName] = "";

        GINFO("Using fake Study Instance UID: %s\n", studyMap[DCM_StudyInstanceUID].c_str());
    } else {
        GINFO("Using StudyUID (%s) for PatientID (%s)\n", studyMap[DCM_StudyInstanceUID].c_str(),
              (*h.subjectInformation->patientID).c_str());
        DcmUIDHandler check(studyMap[DCM_StudyInstanceUID]);
        if (!check.isValidUID()) {
            char buffer[100];
            GINFO("Replacing StudyInstanceUID %s with %s\n", check.c_str(),
                  dcmGenerateUniqueIdentifier(buffer, SITE_STUDY_UID_ROOT));
            studyMap[DCM_StudyInstanceUID] = buffer;
        }
        if (studyMap[DCM_StudyID].empty()) {
            GINFO("Replacing empty StudyID with %s\n", "0");
            studyMap[DCM_StudyID] = "0";
        }
        if (studyMap[DCM_AccessionNumber].empty()) {
            GINFO("Replacing empty StudyID with %s\n", "0");
            studyMap[DCM_AccessionNumber] = "0";
        }
    }

    if (fakeSeries) {
        seriesMap[DCM_PerformingPhysicianName] = "";
        seriesMap[DCM_OperatorsName] = "";

        GINFO("Using fake series data\n");
    }

    // Populate the header with the selects study and series data
    populateHeader(h, studyMap, seriesMap);

    // Serialize the header with the new/modified data
    std::stringstream xml;
    try {
        serialize(h, xml);
    } catch (...) {
        GERROR("Failed to serialize ISMRMRDHeader\n");
    }

    // Serialization does not null terminate the string
    // But deserialize expects a null terminated C-string
    xml << '\0';

    // TODO Replace the mb contents with xml
}
GADGETRON_GADGET_EXPORT(DicomQuery)
} /* namespace Gadgetron */
