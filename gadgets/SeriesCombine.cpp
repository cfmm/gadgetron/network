//
//  SeriesCombine.cpp
//  network
//
//  Created by Martyn Klassen on 2015-05-27.
//  Copyright (c) 2015 CFMM. All rights reserved.
//

#include "SeriesCombine.hpp"

namespace Gadgetron {

struct Lookup {
    int slice;
    int phase;
    int contrast;
    int average;
    int repetition;
    int set;
    int partition;
};

inline bool operator<(const Lookup& first, const Lookup& second) {
    return (first.slice < second.slice || first.phase < second.phase || first.contrast < second.contrast ||
            first.average < second.average || first.repetition < second.repetition || first.set < second.set ||
            first.partition < second.partition);
}

template <typename T> ISMRMRD::ImageHeader& getHeader(Core::Image<T>& image) {
    return std::get<ISMRMRD::ImageHeader>(image);
}

void SeriesCombine::process(Core::InputChannel<Core::AnyImage>& in, Core::OutputChannel& out) {
    std::map<Lookup, uint16_t> seriesID;
    uint16_t nextID = 0;

    // By default, ICE assigns a separate series index to every loop
    // Here all images that are in the same series should get the same image_series_index
    for (auto image : in) {
        auto& header = visit([](auto& arg) mutable -> ISMRMRD::ImageHeader& { return getHeader(arg); }, image);

        Lookup idx{};
        unsigned long combineFlag = seriesCombination;

        idx.slice = header.slice;           // SLC
        idx.phase = header.phase;           // PHS
        idx.contrast = header.contrast;     // ECO
        idx.average = header.average;       // AVE
        idx.repetition = header.repetition; // REP
        idx.set = header.set;               // SET
        idx.partition = header.user_int[0]; // PAR

        if (combineFlag & inrNoSlc)
            idx.slice = 0;
        if (combineFlag & inrNoPhs)
            idx.phase = 0;
        if (combineFlag & inrNoEco)
            idx.contrast = 0;
        if (combineFlag & inrNoAve)
            idx.average = 0;
        if (combineFlag & inrNoRep)
            idx.repetition = 0;
        if (combineFlag & inrNoSet)
            idx.set = 0;
        if (combineFlag & inrNoPar)
            idx.partition = 0;

        const auto [it, success] = seriesID.insert({idx, nextID});
        if (success)
            nextID++;
        header.image_series_index = it->second;

        out.push(std::move(image));
    }
}
GADGETRON_GADGET_EXPORT(SeriesCombine)
} /* namespace Gadgetron */
