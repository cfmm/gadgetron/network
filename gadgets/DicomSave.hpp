//
//  DicomSave.h
//  network
//
//  Created by Sahar Rabinoviz on 2015-07-17.
//  Copyright (c) 2015 CFMM. All rights reserved.
//

#pragma once

#include "cfmm_gadget_exports.hpp"

#include "Node.h"
#include <dcmtk/dcmdata/dcfilefo.h>
#include <filesystem>

namespace Gadgetron {

class EXPORTCFMMGADGETS DicomSave
    : public Core::ChannelGadget<DcmFileFormat, Core::optional<std::string>, Core::optional<ISMRMRD::MetaContainer>> {
  public:
    using Core::ChannelGadget<DcmFileFormat, Core::optional<std::string>,
                              Core::optional<ISMRMRD::MetaContainer>>::ChannelGadget;

    void
    process(Core::InputChannel<DcmFileFormat, Core::optional<std::string>, Core::optional<ISMRMRD::MetaContainer>>& in,
            Core::OutputChannel& out) override;

  protected:
    NODE_PROPERTY(dicom_dir, std::string, "DICOM Directory Name", "dicom");

  private:
    std::filesystem::path fsPath;
};

} /* namespace Gadgetron */
