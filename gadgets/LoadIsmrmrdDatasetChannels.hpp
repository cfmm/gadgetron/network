//  Created by Tim Whelan on 2017-09-26.
#pragma once

#include "LoadIsmrmrdDatasetImages.hpp"

#include <ismrmrd/ismrmrd.h>
#include <ismrmrd/meta.h>
#include <ismrmrd/xml.h>

namespace Gadgetron {

class EXPORTCFMMGADGETS LoadIsmrmrdDatasetChannels : public LoadIsmrmrdDatasetImages {
  public:
    using LoadIsmrmrdDatasetImages::LoadIsmrmrdDatasetImages;

  protected:
    bool loadData(ISMRMRD::DatasetExt& dataset, Core::OutputChannel& out) override;

    template <typename T> bool loadChannels(ISMRMRD::DatasetExt& dataset, Core::OutputChannel& out);
};

template <typename T>
bool LoadIsmrmrdDatasetChannels::loadChannels(ISMRMRD::DatasetExt& dataset, Core::OutputChannel& out) {
    ISMRMRD::IsmrmrdHeader hdr;
    std::string xml;
    hsize_t numEchos;
    try {
        dataset.readHeader(xml);
        ISMRMRD::deserialize(xml.c_str(), hdr);
        numEchos = hdr.encoding[0].encodingLimits.contrast().maximum +
                   1; // number of echos is one more than the highest number (0-based)
    } catch (std::exception& ex) {
        GERROR("Failed to read xml header: %s\n", ex.what());
        return false;
    }
    // Want this to be possible without ImageHeaders or relying on xml to be correct, but trusting xml for now
    std::vector<hsize_t> dims;

    if (!dataset.getDimensions(varname, dims))
        return false;

    hsize_t numChan = dims[1];

    hsize_t numSlices = dims[0] / numEchos;

    hsize_t start[5] = {0, 0, 0, 0, 0}; /* Start of hyperslab */ // changes each channel

    hsize_t count[5] = {numSlices, 1, 1, dims[3], dims[4]}; /* Block count */

    hsize_t stride[5] = {numEchos, 1, 1, 1, 1};

    for (hsize_t ch = 0; ch < numChan; ch++) {
        for (hsize_t e = 0; e < numEchos; e++) {
            if (e == 0)
                GINFO("Channel = %d starting\n", ch);
            auto msg = new GadgetContainerMessage<hoNDArray<T>>();
            auto meta = new GadgetContainerMessage<ISMRMRD::MetaContainer>;
            hoNDArray<T>* channelSlices = msg->getObjectPtr();

            try {
                channelSlices->create(dims[4], dims[3], numSlices);
            } catch (std::runtime_error& err) {
                GEXCEPTION(err, "Couldn't create hoNDArray in Load Gadget.\n");
                return false;
            }

            start[1] = ch; // change offset (count and stride do not change)
            start[0] = e;  // change offset (remember stride will skip other echos)

            dataset.strideRead(varname, channelSlices->get_data_ptr(), static_cast<int>(dims.size()), count, stride,
                               start);

            for (unsigned long long dim : dims)
                meta->getObjectPtr()->append("Dimensions", (long int)dim);

            for (unsigned long long n : stride)
                meta->getObjectPtr()->append("stride", (long int)n);

            for (unsigned long long n : count)
                meta->getObjectPtr()->append("count", (long int)n);

            for (unsigned long long n : start)
                meta->getObjectPtr()->append("start", (long int)n);

            start[0]++; // after writing channel/echo pair, advance to next echo

            if (start[0] == numEchos) // this echo is done, advance to next channel
            {
                start[1]++;
                start[0] = 0;
            }

            ISMRMRD::NDArray<T> dummy; // Currently the best way I can find to convert the type
            meta->getObjectPtr()->set("data_type", (long int)dummy.getDataType());
            msg->cont(meta);

            out.push(std::move(Core::Image<T>()));
        }
    }
    return true;
}

} /* namespace Gadgetron */
