#include "SaveB1IsmrmrdDatasetImages.hpp"
#include "post.hpp"
#include <algorithm>
#include <curl/curl.h>
#include <sstream>
#include <string>
#include <vector>

namespace Gadgetron {
std::tuple<long, std::string, int> SaveB1IsmrmrdDatasetImages::postResults(std::string const& url,
                                                                           std::vector<std::string> const& headers,
                                                                           std::string const& body) {
    long httpResponseCode;
    int curlCode;
    std::string responseBody;
    tie(httpResponseCode, responseBody, curlCode) = cPostDataLogged(url, headers, body, http_timeout);
    return std::make_tuple(httpResponseCode, responseBody, curlCode);
}

std::string SaveB1IsmrmrdDatasetImages::getVarname(const Core::optional<ISMRMRD::MetaContainer>& meta,
                                                   const ISMRMRD::ImageHeader& header) {
    if (meta.has_value()) {
        const auto& metaval = meta.value();
        if (metaval.length("Image Type") > 0) {
            return metaval.as_str("Image Type");
        }
    }
    return SaveIsmrmrdDataset::getVarname(meta, header);
}

void SaveB1IsmrmrdDatasetImages::process(Core::InputChannel<Core::AnyData>& in, Core::OutputChannel& out) {
    makeUniqueFilled(webapp_urls);
    SaveIsmrmrdDataset::process(in, out);
}

void SaveB1IsmrmrdDatasetImages::cleanup() {
    // using postProcess(dataset) to send an http post to the fieldmap server informing it of the new
    // dataset introduces a race condition between when the post is received and when the file is written to the
    // fieldmap server disk by sshfs.  Often the fieldmap server complains that the new dataset does not exist or
    // is missing data. Attempts to remove the race condition failed.
    //
    // using dataset.flush() flushes the internal HDF5 buffers then asks the operating system to flush the
    // system buffers for the open files, but errors continued intermittently. Attempts to use sshfs
    // -odirect_io,cache=no,sshfs_sync to avoid delays with sshfs caching continued to fail intermittently.
    // Attempts to use syncfs to ask the os to sync the filesystem also continued to failed intermittently.
    // Instead, we use a virtual cleanup function after the drive is unmounted.
    std::string coil = "Unknown";
    std::string system = "Unknown";

    auto coilname =
        header.customXML.child("customXML").child("siemens").child("YAPS").child("tTransmittingCoil").text().get();
    if (std::strcmp(coilname, "") != 0) {
        coil = coilname;
        GDEBUG("Coil set to %s\n", coil.c_str());
    }

    auto value = this->header.acquisitionSystemInformation->systemModel;
    if (value.has_value()) {
        system = value.value();
        GDEBUG("System set to %s\n", system.c_str());
    }

    if (coil == "Unknown") {
        GDEBUG("Unable to parse xml for TransmittingCoil, coil not set!\n");
    }
    if (system == "Unknown") {
        GDEBUG("Unable to parse xml for systemModel, system not set!\n");
    }

    std::vector<std::string> headers;
    headers.emplace_back("Authorization: " + webapp_auth_code);
    headers.emplace_back("Content-Type: application/json");

    std::stringstream jsonSs;
    jsonSs << R"({"system": ")" << system << R"(", "coilname": ")" << coil << R"(", "path": ")"
           << relativeFilePath.string() << R"("})";
    // c_str() pointer only lasts as long as string object
    std::string jsonStr = jsonSs.str();

    std::set<long> validHttpCodes{200, 201, 202};
    for (auto const& url : webapp_urls) {
        std::tuple<long, std::string, int> result = postResults(url, headers, jsonStr);
        if (std::get<2>(result) == CURLE_OK && validHttpCodes.find(std::get<0>(result)) != validHttpCodes.end())
            break;
    }
}
GADGETRON_GADGET_EXPORT(SaveB1IsmrmrdDatasetImages)
} /* namespace Gadgetron */
