//
//  LoadSortedIsmrmrdDatasetImages.hpp
//  network
//
//  Created by Martyn Klassen on 2016-02-19.
//  Copyright © 2016 CFMM. All rights reserved.
//

#pragma once

#include "LoadIsmrmrdDatasetImages.hpp"

namespace Gadgetron {

class EXPORTCFMMGADGETS LoadSortedIsmrmrdDatasetImages : public LoadIsmrmrdDatasetImages {
  public:
    using LoadIsmrmrdDatasetImages::LoadIsmrmrdDatasetImages;

  protected:
    typedef std::pair<uint32_t, ISMRMRD::ISMRMRD_ImageHeader> PairType;
    typedef std::vector<PairType> HeadersType;
    HeadersType headers;

    virtual void sortHeaders();

    bool configure(ISMRMRD::DatasetExt& dataset) override;

  private:
    bool loadHeaders(ISMRMRD::DatasetExt& dataset);

    static bool comparePair(const PairType& pair1, const PairType& pair2);
};
} // namespace Gadgetron
