//
//  CopyFile.h
//  network
//
//  Created by Olivia Stanley from JobSubmit by Martyn Klassen on 2016-10-03.
//  Copyright (c) 2013 CFMM. All rights reserved.
//

#pragma once

#include "SshCopyTask.hpp"
#include "SshGadget.hpp"

namespace Gadgetron {

class EXPORTCFMMGADGETS CopyFile : public SshGadget<SshCopyTask> {
  public:
    using SshGadget<SshCopyTask>::SshGadget;

  protected:
    void configure(SshCopyTask& task) override { task.setRemoveFile(cleanupLocal); }

    NODE_PROPERTY(cleanupLocal, bool, "Delete localFile after copy", true);
};

} /* namespace Gadgetron */
