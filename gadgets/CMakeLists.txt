find_package(DCMTK REQUIRED)
if (NOT TARGET DCMTK::dcmdata)
   add_library(DCMTK::dcmdata INTERFACE IMPORTED)
   target_link_libraries(DCMTK::dcmdata INTERFACE dcmdata)
endif ()
if (NOT TARGET DCMTK::ofstd)
   add_library(DCMTK::ofstd INTERFACE IMPORTED)
   target_link_libraries(DCMTK::ofstd INTERFACE ofstd)
endif ()
if (NOT TARGET DCMTK::oflog)
   add_library(DCMTK::oflog INTERFACE IMPORTED)
   target_link_libraries(DCMTK::oflog INTERFACE oflog)
endif ()
if (NOT TARGET DCMTK::dcmnet)
   add_library(DCMTK::dcmnet INTERFACE IMPORTED)
   target_link_libraries(DCMTK::dcmnet INTERFACE dcmnet)
endif ()
if (NOT TARGET DCMTK::dcmtls)
   add_library(DCMTK::dcmtls INTERFACE IMPORTED)
   target_link_libraries(DCMTK::dcmtls INTERFACE dcmtls)
endif ()

find_package(OpenSSL REQUIRED)

include_directories(
      ${CMAKE_CURRENT_SOURCE_DIR}/../gadgets
      ${CMAKE_CURRENT_SOURCE_DIR}/../communication/protocol
      ${CMAKE_CURRENT_SOURCE_DIR}/../toolboxes/ismrmrd
      ${CMAKE_CURRENT_SOURCE_DIR}/../toolboxes/ssh
      ${CMAKE_CURRENT_SOURCE_DIR}/../toolboxes/curl
      ${CMAKE_CURRENT_SOURCE_DIR}/../toolboxes/exec
      ${CMAKE_SOURCE_DIR}/apps/gadgetron
      ${CMAKE_SOURCE_DIR}/gadgets/mri_core
      ${CMAKE_SOURCE_DIR}/gadgets/dicom
      ${CMAKE_SOURCE_DIR}/toolboxes/gadgettools
      ${CMAKE_SOURCE_DIR}/toolboxes/mri_core
      ${CMAKE_SOURCE_DIR}/toolboxes/core
      ${CMAKE_SOURCE_DIR}/toolboxes/core/cpu
      ${CMAKE_SOURCE_DIR}/toolboxes/core/cpu/image
      ${CMAKE_BINARY_DIR}/apps/gadgetron
      ${CMAKE_BINARY_DIR}/toolboxes/core
      ${ACE_INCLUDE_DIR}
      ${ISMRMRD_INCLUDE_DIR}
      ${Boost_INCLUDE_DIR}
      ${DCMTK_root_INCLUDE_DIR}
      ${OPENSSL_INCLUDE_DIR}
)

set(cfmm_gadgets_header_files
      LoadIsmrmrdDatasetImages.hpp
      LoadSortedIsmrmrdDatasetImages.hpp
      LoadIsmrmrdDatasetChannels.hpp
      CopyFile.hpp
      DicomFixMetaData.hpp
      DicomQuery.hpp
      DicomSave.hpp
      DicomSend.hpp
      JobSubmit.hpp
      SaveIsmrmrdDataset.hpp
      SaveB1IsmrmrdDatasetImages.hpp
      SeriesCombine.hpp
      SshGadget.hpp
)

set(cfmm_gadgets_src_files
      LoadIsmrmrdDatasetImages.cpp
      LoadSortedIsmrmrdDatasetImages.cpp
      LoadIsmrmrdDatasetChannels.cpp
      SaveIsmrmrdDataset.cpp
      SaveB1IsmrmrdDatasetImages.cpp
      DicomSend.cpp
      DicomQuery.cpp
      SeriesCombine.cpp
      DicomSave.cpp
      DicomFixMetaData.cpp
      HeaderGadgetExports.cpp
)

add_library(cfmm_gadgets SHARED
      cfmm_gadget_exports.hpp
      ${cfmm_gadgets_src_files}
      ${cfmm_gadgets_header_files}
)

set_target_properties(cfmm_gadgets PROPERTIES
      VERSION ${GADGETRON_VERSION_STRING}
      SOVERSION ${GADGETRON_SOVERSION})

if (CMAKE_BUILD_TYPE MATCHES Debug)
   set_target_properties(cfmm_gadgets PROPERTIES COMPILE_FLAGS "-O1")
endif (CMAKE_BUILD_TYPE MATCHES Debug)

target_link_libraries(cfmm_gadgets
      cfmm_toolbox_ismrmrd
      cfmm_toolbox_ssh
      cfmm_toolbox_post
      cfmm_toolbox_exec
      gadgetron_toolbox_log
      gadgetron_dicom
      gadgetron_mricore
      ${OPENSSL_CRYPTO_LIBRARY}
      ${OPENSSL_SSL_LIBRARY}
      ${Boost_LIBRARIES}
      DCMTK::ofstd
      DCMTK::oflog
      DCMTK::dcmnet
      DCMTK::dcmdata
      DCMTK::dcmtls
      ISMRMRD::ISMRMRD
      z
      m
      pthread
)

install(FILES
      cfmm_gadget_exports.hpp
      ${cfmm_gadgets_header_files}
      DESTINATION ${GADGETRON_INSTALL_INCLUDE_PATH} COMPONENT main)

install(TARGETS cfmm_gadgets DESTINATION lib COMPONENT main)

