//
//  JobSubmit.h
//  network
//
//  Created by Martyn Klassen on 2015-04-27.
//  Copyright (c) 2015 CFMM. All rights reserved.
//

#pragma once

#include "SshExecuteTask.hpp"
#include "SshGadget.hpp"

namespace Gadgetron {

class EXPORTCFMMGADGETS JobSubmit : public SshGadget<SshExecuteTask> {
  public:
    using SshGadget<SshExecuteTask>::SshGadget;

  protected:
    void configure(SshExecuteTask& task) override { task.setExecutable(programName); }

  protected:
    NODE_PROPERTY(programName, std::string, "Name of program to execute on remote host", "ls");
};

} /* namespace Gadgetron */
