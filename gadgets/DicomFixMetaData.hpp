//
//  DicomFixMetaData.h
//  network
//
//  Created by Sahar Rabinoviz on 2015-08-14.
//  Copyright (c) 2015 CFMM. All rights reserved.
//

#pragma once

#include "Node.h"
#include "cfmm_gadget_exports.hpp"
#include "mri_core_def.h"
#include <cstdio>
#include <dcmtk/dcmdata/dcfilefo.h>
#include <dcmtk/ofstd/oftypes.h>
#include <ismrmrd/xml.h>
#include <sys/stat.h>

#define NUMBER_PHASE_ENCODE_STEPS "num_phase_encode_steps"
#define NUMBER_AVERAGES "num_averages"
#define OPERATOR_NAME "operator_name"
#define IMAGED_NUCLEUS "imaged_nucleus"
#define RF_COIL_NAME "rf_coil_name"
#define PERCENT_SAMPLING "percent_sampling"

#define DICOM_KEY_IMAGE_SCALE_OFFSET DicomKeySet(GADGETRON_IMAGE_SCALE_OFFSET, 0x0028, 0x1052, T_DOUBLE)
#define DICOM_KEY_IMAGE_SCALE_RATIO DicomKeySet(GADGETRON_IMAGE_SCALE_RATIO, 0x0028, 0x1053, T_DOUBLE)
#define DICOM_NUMBER_PHASE_ENCODE_STEPS DicomKeySet(NUMBER_PHASE_ENCODE_STEPS, 0x0018, 0x0089, T_LONG)
#define DICOM_NUMBER_AVERAGES DicomKeySet(NUMBER_AVERAGES, 0x0018, 0x0083, T_LONG)
#define DICOM_OPERATOR_NAME DicomKeySet(OPERATOR_NAME, 0x0008, 0x1070, T_STRING)
#define DICOM_IMAGED_NUCLEUS DicomKeySet(IMAGED_NUCLEUS, 0x0018, 0x0085, T_STRING)
#define DICOM_RF_COIL_NAME DicomKeySet(RF_COIL_NAME, 0x0018, 0x1250, T_STRING)
#define DICOM_PERCENT_SAMPLING DicomKeySet(PERCENT_SAMPLING, 0x0018, 0x0093, T_DOUBLE)

namespace Gadgetron {

typedef enum { T_STRING = 0, T_DOUBLE = 1, T_LONG = 2 } MetaType;

struct DicomKeySet {
    DicomKeySet(std::string n, Uint16 key1, Uint16 key2, MetaType type)
        : name(std::move(n)), k1(key1), k2(key2), expected(type) {}

    std::string name;
    Uint16 k1, k2;
    MetaType expected;
};

// Writes a DICOM string value at the given location in the header
// Saves keystrokes
// clang format on delete[] (s);
#define WRITE_DCM_STRING(k, s)                                                                                         \
    do {                                                                                                               \
        status = dataset->putAndInsertString(k, s);                                                                    \
        if (!status.good()) {                                                                                          \
            GDEBUG("Failed to insert DICOM field (0x%04X,0x%04X) at "                                                  \
                   "line %u, reason %s\n",                                                                             \
                   (k).getGroup(), (k).getElement(), __LINE__, status.text());                                         \
            delete[] (s);                                                                                              \
            return false;                                                                                              \
        }                                                                                                              \
    } while (0)

class EXPORTCFMMGADGETS DicomFixMetaData
    : public Core::ChannelGadget<DcmFileFormat, Core::optional<std::string>, Core::optional<ISMRMRD::MetaContainer>> {
  public:
    using Core::ChannelGadget<DcmFileFormat, Core::optional<std::string>,
                              Core::optional<ISMRMRD::MetaContainer>>::ChannelGadget;

    void
    process(Core::InputChannel<DcmFileFormat, Core::optional<std::string>, Core::optional<ISMRMRD::MetaContainer>>& in,
            Core::OutputChannel& out) override;

  protected:
    virtual bool removeBadParameters(DcmFileFormat& dcmFile);

    virtual bool addUserParameters(DcmFileFormat& dcmFile);

    virtual bool fixBadParameters(DcmFileFormat& dcmFile, float pixelSpacingY, float pixelSpacingX);

    virtual bool handleMultipleFrames(DcmFileFormat& dcmFile, float sliceSpacing);

    virtual bool processMetaContainer(DcmFileFormat& dcmFile, ISMRMRD::MetaContainer& metaContainer);

    /*!
     \param identifiers     Vector of DicomKeySet to be added to dataset meta
     \param dataset         Dicom dataset where meta data will be added
     \param appearances     lambda Function input is parameter name; output number of values
     \param valueGetter     lambda Function input is (char *) buffer, (const char *) parameter name, (MetaType) expected
     output type, (size_t) instance of parameter; output is if parameter is found.
     */
    static bool addDicomKeys(std::vector<DicomKeySet>& identifiers, DcmDataset* dataset,
                             std::function<size_t(const char*)>& appearances,
                             std::function<bool(char*, const char*, MetaType, size_t)>& valueGetter);
};

} /* namespace Gadgetron */
