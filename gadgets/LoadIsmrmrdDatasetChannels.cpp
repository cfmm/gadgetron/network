#include "LoadIsmrmrdDatasetChannels.hpp"

namespace Gadgetron {
bool LoadIsmrmrdDatasetChannels::loadData(ISMRMRD::DatasetExt& dataset, Core::OutputChannel& out) {
    // Get the number of images
    uint16_t datatype = dataset.datatype(varname);

    int result = true;
    if (!this->configure(dataset)) {
        switch (datatype) {
        case ISMRMRD::ISMRMRD_USHORT:
            result = loadChannels<uint16_t>(dataset, out);
            break;
        case ISMRMRD::ISMRMRD_SHORT:
            result = loadChannels<int16_t>(dataset, out);
            break;
        case ISMRMRD::ISMRMRD_UINT:
            result = loadChannels<uint32_t>(dataset, out);
            break;
        case ISMRMRD::ISMRMRD_INT:
            result = loadChannels<int32_t>(dataset, out);
            break;
        case ISMRMRD::ISMRMRD_FLOAT:
            result = loadChannels<float>(dataset, out);
            break;
        case ISMRMRD::ISMRMRD_DOUBLE:
            result = loadChannels<double>(dataset, out);
            break;
        case ISMRMRD::ISMRMRD_CXFLOAT:
            result = loadChannels<complex_float_t>(dataset, out);
            break;
        case ISMRMRD::ISMRMRD_CXDOUBLE:
            result = loadChannels<complex_double_t>(dataset, out);
            break;
        default:
            result = false;
        }
    }
    if (result)
        GERROR("Unable to load images\n");
    return result;
}
GADGETRON_GADGET_EXPORT(LoadIsmrmrdDatasetChannels)
} // namespace Gadgetron
