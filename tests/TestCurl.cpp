#pragma clang diagnostic push
#pragma ide diagnostic ignored "cert-err58-cpp"
//
// Created by akuurstr on 2021-02-04.
//

#include "CurlInterface.hpp"
#include "helpers.hpp"
#include "gmock/gmock.h"
#include <chrono>
#include <sstream>
#include <thread>

namespace {

class TestCurl : public ::testing::Test {

  protected:
    TestCurl()
        : root(resolvePath("network/tests/data")), baseUrl("http://127.0.0.1"), port(6000), postPath("/test/path"),
          body("test body"), httpResponseCode(404), curlCode(0)

    {
        headers.emplace_back("Authorization: 1234");
    }

    ~TestCurl() override = default;

    std::string root;
    std::string baseUrl;
    int port;
    std::string postPath;
    std::vector<std::string> headers;
    std::string body;

    long httpResponseCode;
    int curlCode;
    std::string responseBody;
    CurlInterface curl;
};

class TestCurlWithServer : public TestCurl {
  protected:
    TestCurlWithServer() {
        // run http server
        std::stringstream cmd;
        cmd << root << "/http_server_TestCurl.py " << port << " &";
        serverResult = std::system(cmd.str().c_str());
        using namespace std::chrono_literals;
        std::this_thread::sleep_for(1s);

        std::stringstream ssUrl;
        ssUrl << baseUrl << ":" << port << postPath;
        fullUrl = ssUrl.str();
    }

    int serverResult;
    std::string fullUrl;
};

TEST_F(TestCurl, wrong_url) {
    // wrong url
    Gadgetron::GadgetronLogger::instance()->disableLogLevel(Gadgetron::GadgetronLogLevel::GADGETRON_LOG_LEVEL_ERROR);
    tie(httpResponseCode, responseBody, curlCode) = curl.postDataLogged("", headers, body);
    Gadgetron::GadgetronLogger::instance()->enableLogLevel(Gadgetron::GadgetronLogLevel::GADGETRON_LOG_LEVEL_ERROR);
    EXPECT_NE(curlCode, 0) << "Malformed url '' received curlCode: " << curlCode;
}

TEST_F(TestCurlWithServer, post) {
    ASSERT_EQ(serverResult, 0);
    // correct
    tie(httpResponseCode, responseBody, curlCode) = curl.postDataLogged(fullUrl, headers, body);
    EXPECT_TRUE((httpResponseCode == 200) || (httpResponseCode == 201) || (httpResponseCode == 202))
        << "Expected success HTTP Post response code, received code:" << httpResponseCode;
}

TEST_F(TestCurlWithServer, header) {
    ASSERT_EQ(serverResult, 0);
    // wrong header
    std::vector<std::string> badHeaders = headers;
    badHeaders[0] = "";
    Gadgetron::GadgetronLogger::instance()->disableLogLevel(Gadgetron::GadgetronLogLevel::GADGETRON_LOG_LEVEL_ERROR);
    tie(httpResponseCode, responseBody, curlCode) = curl.postDataLogged(fullUrl, badHeaders, body);
    Gadgetron::GadgetronLogger::instance()->enableLogLevel(Gadgetron::GadgetronLogLevel::GADGETRON_LOG_LEVEL_ERROR);
    EXPECT_EQ(httpResponseCode, 400) << "Expected http response 400 when incorrect header sent";
}

TEST_F(TestCurlWithServer, body) {
    ASSERT_EQ(serverResult, 0);
    // wrong body
    std::string badBody;
    Gadgetron::GadgetronLogger::instance()->disableLogLevel(Gadgetron::GadgetronLogLevel::GADGETRON_LOG_LEVEL_ERROR);
    tie(httpResponseCode, responseBody, curlCode) = curl.postDataLogged(fullUrl, headers, badBody);
    Gadgetron::GadgetronLogger::instance()->enableLogLevel(Gadgetron::GadgetronLogLevel::GADGETRON_LOG_LEVEL_ERROR);
    EXPECT_EQ(httpResponseCode, 400) << "Expected http response 400 when body is '" << badBody << "' instead of '"
                                     << body << "'";
}

TEST_F(TestCurl, port) {
    // wrong port
    int badPort = port + 1;
    std::stringstream ssUrl;
    ssUrl << baseUrl << ":" << badPort << postPath;
    Gadgetron::GadgetronLogger::instance()->disableLogLevel(Gadgetron::GadgetronLogLevel::GADGETRON_LOG_LEVEL_ERROR);
    tie(httpResponseCode, responseBody, curlCode) = curl.postDataLogged(ssUrl.str(), headers, body);
    Gadgetron::GadgetronLogger::instance()->enableLogLevel(Gadgetron::GadgetronLogLevel::GADGETRON_LOG_LEVEL_ERROR);
    EXPECT_NE(curlCode, 0) << "Malformed url '" << ssUrl.str() << "' received curlCode: " << curlCode;
}

TEST_F(TestCurlWithServer, path) {
    ASSERT_EQ(serverResult, 0);
    // wrong route
    std::string badUrl = fullUrl;
    badUrl.append("/wrong");
    Gadgetron::GadgetronLogger::instance()->disableLogLevel(Gadgetron::GadgetronLogLevel::GADGETRON_LOG_LEVEL_ERROR);
    tie(httpResponseCode, responseBody, curlCode) = curl.postDataLogged(badUrl, headers, body);
    Gadgetron::GadgetronLogger::instance()->enableLogLevel(Gadgetron::GadgetronLogLevel::GADGETRON_LOG_LEVEL_ERROR);
    EXPECT_EQ(httpResponseCode, 400) << "Expected http response 400 when url is '" << badUrl << "' instead of '"
                                     << fullUrl << "'";
}

} // namespace

#pragma clang diagnostic pop
