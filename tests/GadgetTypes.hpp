//
// Created by akuurstr on 06/11/23.
//

#ifndef NETWORK_GADGET_TYPES_H
#define NETWORK_GADGET_TYPES_H
#include "SaveB1IsmrmrdDatasetImages.hpp"

#include "CopyFile.hpp"
#include "DicomFixMetaData.hpp"
#include "DicomQuery.hpp"
#include "DicomSave.hpp"
#include "DicomSend.hpp"
#include "JobSubmit.hpp"
#include "LoadIsmrmrdDatasetChannels.hpp"
#include "LoadSortedIsmrmrdDatasetImages.hpp"
#include "SaveIsmrmrdDataset.hpp"
#include "SeriesCombine.hpp"
#include <gtest/gtest.h>
namespace Gadgetron::Tests { // NOLINT(*-identifier-naming)
using Types = testing::Types<Gadgetron::JobSubmit, Gadgetron::CopyFile, Gadgetron::DicomSend, Gadgetron::DicomSave,
                             Gadgetron::DicomQuery, Gadgetron::DicomFixMetaData, Gadgetron::SeriesCombine,
                             Gadgetron::SaveIsmrmrdDataset, Gadgetron::SaveB1IsmrmrdDatasetImages,
                             Gadgetron::LoadSortedIsmrmrdDatasetImages, Gadgetron::LoadIsmrmrdDatasetImages,
                             Gadgetron::LoadIsmrmrdDatasetChannels>;
}
#endif // NETWORK_GADGET_TYPES_H
