#!/usr/bin/env python3

import json
import sys
from http.server import BaseHTTPRequestHandler, HTTPServer


class webserverHandlerBase(BaseHTTPRequestHandler):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.post_url = None
        self.post_headers = None
        self.post_body = None

    def check_post(self):
        return True

    def do_POST(self):
        self.post_url = self.path
        self.post_headers = self.headers
        content_len = int(self.headers.get('Content-Length'))
        self.post_body = self.rfile.read(content_len)

        if self.check_post():
            self.send_response(201)
        else:
            self.send_response(400)
        self.send_header('Content-type', 'text/html')
        self.end_headers()

        payload = {'post_url': self.post_url,
                   'post_headers': dict(self.post_headers),
                   'post_body': self.post_body.decode('utf-8'),
                   }
        response_body = json.dumps(payload).encode('utf-8')

        self.wfile.write(response_body)


def main(webserverHandler):
    num_args = len(sys.argv)
    if num_args > 2:
        print("usage: http_server.py [port]")
        sys.exit(1)
    if num_args == 1:
        port = 80
    else:
        port = int(sys.argv[1])

    with HTTPServer(('', port), webserverHandler) as httpd:
        print("serving at port", port)
        httpd.timeout = 10
        httpd.handle_request()


if __name__ == "__main__":
    main(webserverHandlerBase)
