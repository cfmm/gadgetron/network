#!/usr/bin/env python3
import socket
import sys
import threading
import traceback
from cryptography.hazmat.primitives import serialization
from paramiko import ServerInterface, RSAKey, Transport, SSHException
from paramiko.common import OPEN_SUCCEEDED, OPEN_FAILED_ADMINISTRATIVELY_PROHIBITED, AUTH_FAILED, AUTH_SUCCESSFUL
from paramiko.util import log_to_file


class Server(ServerInterface):

    def __init__(self, username, public_key):
        self.username = username
        self.public_key = public_key
        self.event = threading.Event()

    def check_channel_request(self, kind, channel_id):
        if kind == "session":
            return OPEN_SUCCEEDED
        return OPEN_FAILED_ADMINISTRATIVELY_PROHIBITED

    def check_auth_password(self, username, password):
        # Password authentication always fails
        return AUTH_FAILED

    def check_auth_publickey(self, username, key):
        if (username == self.username) and (key == self.public_key):
            return AUTH_SUCCESSFUL
        return AUTH_FAILED

    def get_allowed_auths(self, username):
        return "publickey"  # ""password,publickey"

    def check_channel_shell_request(self, channel):
        self.event.set()
        return True

    def check_channel_pty_request(self, channel, term, width, height, pixel_width, pixel_height, modes):
        return True


def main(public_key, username, port=2200, host_key=None):
    if host_key is None:
        host_key = RSAKey.generate(4096)

    # now connect
    try:
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        sock.bind(("", port))
    except Exception as e:
        print("*** Bind failed: " + str(e))
        traceback.print_exc()
        sys.exit(1)

    try:
        sock.listen(100)
        print("Listening for connection ...")
        client, addr = sock.accept()
    except Exception as e:
        print("*** Listen/accept failed: " + str(e))
        traceback.print_exc()
        sys.exit(1)

    print("Got a connection!")

    try:
        t = Transport(client, gss_kex=False)
        t.add_server_key(host_key)
        server = Server(username, public_key)
        try:
            t.start_server(server=server)
        except SSHException:
            print("*** SSH negotiation failed.")
            sys.exit(1)

        # wait for auth
        chan = t.accept(20)
        if chan is None:
            print("*** No channel.")
            sys.exit(1)
        print("Authenticated!")

        server.event.wait(10)
        if not server.event.is_set():
            print("*** Client never asked for a shell.")
            sys.exit(1)

        chan.send("Username: ")
        f = chan.makefile("rU")
        username = f.readline().strip("\r\n")
        chan.send("\r\nI don't like you, " + username + ".\r\n")
        chan.close()

    except Exception as e:
        print("*** Caught exception: " + str(e.__class__) + ": " + str(e))
        traceback.print_exc()
        # noinspection PyBroadException
        try:
            t.close()
        except:
            pass
        sys.exit(1)


if __name__ == '__main__':
    # setup logging
    log_to_file("server.log")
    host_key = RSAKey.generate(4096)
    with open(sys.argv[1], 'r') as fp:
        pem = fp.read()
    rsa = serialization.load_pem_private_key(pem.encode(), None)
    pem_key = rsa.private_bytes(
        encoding=serialization.Encoding.PEM,
        format=serialization.PrivateFormat.TraditionalOpenSSL,
        encryption_algorithm=serialization.NoEncryption()  # No encryption
    )


    def myfakefile(keystring):
        myfakefile.readlines = lambda: keystring.split("\n")
        return myfakefile


    paramiko_key = RSAKey.from_private_key(myfakefile(pem_key.decode('ascii')))
    port = int(sys.argv[2])
    username = sys.argv[3]
    main(paramiko_key, username, port, host_key)
