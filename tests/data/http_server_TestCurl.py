#!/usr/bin/env python3

from http_server import webserverHandlerBase, main


class webserverHandlerTestCurl(webserverHandlerBase):
    def check_post(self):
        return (self.post_url == '/test/path' and
                'Authorization' in self.post_headers.keys() and
                self.post_headers['Authorization'] == '1234' and
                self.post_body.decode('utf-8') == 'test body')


if __name__ == "__main__":
    main(webserverHandlerTestCurl)
