#pragma clang diagnostic push
#pragma ide diagnostic ignored "cert-err58-cpp"
//
// Created by Martyn Klassen on 2019-02-28.
//

#include "H5Cpp.h"
#include "SaveB1IsmrmrdDatasetImages.hpp"
#include "generate_private_key.hpp"
#include "helpers.hpp"
#include "pugixml.hpp"
#include "gmock/gmock.h"

namespace {

class MockB1Gadget : public Gadgetron::SaveB1IsmrmrdDatasetImages {
  public:
    MockB1Gadget(const Gadgetron::Core::Context& context, const Gadgetron::Core::GadgetProperties& properties)
        : SaveB1IsmrmrdDatasetImages::SaveB1IsmrmrdDatasetImages(context, properties) {
        ON_CALL(*this, execTimeout).WillByDefault(testing::Return(std::tuple<int, std::string>{0, ""}));
    }

    MOCK_METHOD((std::tuple<int, std::string>), execTimeout, (std::string&, long));
    MOCK_METHOD(void, unmount, ());
    MOCK_METHOD3(postResults,
                 std::tuple<long, std::string, int>(std::string const& url, std::vector<std::string> const& vheader,
                                                    std::string const& body));

    auto getFilePath() { return mountPath / relativeFilePath; }

    ~MockB1Gadget() override { std::filesystem::remove_all(mountPath); }
};

class TestB1Save : public GadgetTester {
  protected:
    TestB1Save() {
        properties.insert({"filename", "test.ismrmrd"});
        properties.insert({"b1_protocolnames", "tfl_rfmap\nbss_rfmap"});
        properties.insert({"b0_protocolnames", "AdjGre"});
        properties.insert({"groupname", "data"});
        properties.insert({"varname", "images"});
        properties.insert({"passphrase", "secret"});
        properties.insert({"webapp_urls", "127.0.0.1:5000/api/fieldmap"});
        properties.insert({"webapp_auth_code", "some_random_string"});

        properties.insert({"remotemounts", "TestRemote"});
        properties.insert({"privatekey", "test_key.pem"});
        properties.insert({"username", "test_user"});
        properties.insert({"mountpoint", "fakeRemoteMount"});

        generatePrivateKey(properties.at("privatekey").c_str());
    };

    ~TestB1Save() override { std::filesystem::remove(properties.at("privatekey")); }

    Core::GadgetProperties properties;
};

TEST_F(TestB1Save, image) {
    using ::testing::NiceMock;
    NiceMock<MockB1Gadget> gadget(context, properties);

    std::string magName = "B1 Magnitude";
    std::string phaseName = "B1 Phase";
    std::string maskName = "Mask";

    Core::Image<float> mag = this->makeImage<float>();
    auto& metaMag = std::get<Core::optional<ISMRMRD::MetaContainer>>(mag);
    metaMag->append("Image Type", magName.c_str());

    // phase ISMRMRD image
    Core::Image<float> phase = this->makeImage<float>();
    auto& metaPhase = std::get<Core::optional<ISMRMRD::MetaContainer>>(phase);
    metaPhase->append("Image Type", phaseName.c_str());

    // mask ISMRMRD image
    Core::Image<float> mask = this->makeImage<float>();
    auto& metaMask = std::get<Core::optional<ISMRMRD::MetaContainer>>(mask);
    metaMask->append("Image Type", maskName.c_str());

    Gadgetron::GadgetronLogger::instance()->disableLogLevel(GadgetronLogLevel::GADGETRON_LOG_LEVEL_ERROR);
    runGadget(gadget, std::vector<Core::AnyImage>{std::move(mag), std::move(phase), std::move(mask)});
    auto createdFile = gadget.getFilePath();
    Gadgetron::GadgetronLogger::instance()->enableLogLevel(GadgetronLogLevel::GADGETRON_LOG_LEVEL_ERROR);

    // check that file is created with proper naming
    ASSERT_TRUE(std::filesystem::exists(createdFile));
    H5::H5File file(createdFile.c_str(), H5F_ACC_RDONLY);
    ASSERT_EQ(file.getNumObjs(), 1);
    ASSERT_EQ(file.getObjnameByIdx(0), properties.at("groupname"));

    H5::Group group = file.openGroup(properties.at("groupname"));
    ASSERT_EQ(group.getNumObjs(), 4);
    ASSERT_EQ(group.getObjnameByIdx(0), magName);
    ASSERT_EQ(group.getObjnameByIdx(1), phaseName);
    ASSERT_EQ(group.getObjnameByIdx(2), maskName);
    ASSERT_EQ(group.getObjnameByIdx(3), "xml");
}

TEST_F(TestB1Save, test_B1_protocol) {
    std::string protocol("tfl_rfmap");
    std::string coil("testCoil");

    context.header.acquisitionSystemInformation->systemModel.set("testSystem");
    context.header.measurementInformation->protocolName.set(protocol);
    context.header.customXML.append_child("customXML")
        .append_child("siemens")
        .append_child("YAPS")
        .append_child("tTransmittingCoil")
        .append_child(pugi::node_pcdata)
        .set_value(coil.c_str());

    using ::testing::NiceMock;
    NiceMock<MockB1Gadget> gadget(context, properties);

    using ::testing::AllOf;
    using ::testing::Contains;
    using ::testing::HasSubstr;
    EXPECT_CALL(gadget, postResults(HasSubstr(properties.at("webapp_urls")),
                                    Contains(HasSubstr(properties.at("webapp_auth_code"))),
                                    AllOf(HasSubstr(context.header.acquisitionSystemInformation->systemModel.value()),
                                          HasSubstr(coil), HasSubstr(properties.at("filename")))));

    runGadget(gadget, this->makeImage<float>());
}

TEST_F(TestB1Save, test_B0_protocol) {
    std::string protocol("AdjGre");
    std::string coil("testCoil");

    context.header.acquisitionSystemInformation->systemModel.set("testSystem");
    context.header.measurementInformation->protocolName.set(protocol);
    context.header.customXML.append_child("customXML")
        .append_child("siemens")
        .append_child("YAPS")
        .append_child("tTransmittingCoil")
        .append_child(pugi::node_pcdata)
        .set_value(coil.c_str());

    using ::testing::NiceMock;
    NiceMock<MockB1Gadget> gadget(context, properties);

    using ::testing::AllOf;
    using ::testing::Contains;
    using ::testing::HasSubstr;
    EXPECT_CALL(gadget, postResults(HasSubstr(properties.at("webapp_urls")),
                                    Contains(HasSubstr(properties.at("webapp_auth_code"))),
                                    AllOf(HasSubstr(context.header.acquisitionSystemInformation->systemModel.value()),
                                          HasSubstr(coil), HasSubstr(properties.at("filename")))));
    runGadget(gadget, this->makeImage<float>());
}

TEST_F(TestB1Save, test_unknown_protocol) {
    std::string coil("testCoil");

    context.header.acquisitionSystemInformation->systemModel.set("testSystem");
    context.header.customXML.append_child("customXML")
        .append_child("CFMM")
        .append_child("TransmittingCoil")
        .append_child(pugi::node_pcdata)
        .set_value(coil.c_str());

    using ::testing::NiceMock;
    NiceMock<MockB1Gadget> gadget(context, properties);

    Gadgetron::GadgetronLogger::instance()->disableLogLevel(GadgetronLogLevel::GADGETRON_LOG_LEVEL_ERROR);
    runGadget(gadget, this->makeImage<float>());
    Gadgetron::GadgetronLogger::instance()->enableLogLevel(GadgetronLogLevel::GADGETRON_LOG_LEVEL_ERROR);
}

class TestUniquenessLogic : public TestB1Save, public testing::WithParamInterface<std::vector<std::string>> {};

TEST_P(TestUniquenessLogic, TestUniquenessLogic) {
    using ::testing::NiceMock;
    NiceMock<MockB1Gadget> gadget(context, properties);
    std::vector<std::string> vectorSet = GetParam();
    std::vector<std::string> expected = {"a", "b", "c"};
    MockB1Gadget::makeUniqueFilled(vectorSet);
    ASSERT_EQ(vectorSet, expected);
}

std::vector<std::string> uniquenessTests[] = {
    {"a", "b", "c"}, {"a", "a", "b", "c"}, {"a", "a", "b", "a", "c"}, {"a", "b", "c", ""}};

INSTANTIATE_TEST_SUITE_P(TestUniquenessLogic, TestUniquenessLogic, testing::ValuesIn(uniquenessTests));

} // namespace
