//
// Created by Martyn Klassen on 2019-02-28.
//

#include "gmock/gmock.h"
// GadgetTypes must be included before helpers.
#include "GadgetTypes.hpp"
#include "helpers.hpp"

namespace {
using namespace Gadgetron;
template <typename T>

struct TestGadgetFixture : public GadgetTester {};

TYPED_TEST_SUITE_P(TestGadgetFixture);

TYPED_TEST_P(TestGadgetFixture, test_gadget) {
    if (!(typeid(TypeParam) == typeid(SaveIsmrmrdDataset) || typeid(TypeParam) == typeid(SaveB1IsmrmrdDatasetImages))) {
        Core::GadgetProperties properties;
        auto gadget = TypeParam(this->context, properties);
        Gadgetron::GadgetronLogger::instance()->disableLogLevel(
            Gadgetron::GadgetronLogLevel::GADGETRON_LOG_LEVEL_ERROR);
        this->runGadget(gadget);
        Gadgetron::GadgetronLogger::instance()->enableLogLevel(Gadgetron::GadgetronLogLevel::GADGETRON_LOG_LEVEL_ERROR);
    }
}

REGISTER_TYPED_TEST_SUITE_P(TestGadgetFixture, test_gadget);

INSTANTIATE_TYPED_TEST_SUITE_P(TestPrefix, TestGadgetFixture, Gadgetron::Tests::Types);
} // namespace
