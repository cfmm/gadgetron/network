#include "../serialization/GadgetronDeserializers.hpp"
#include "../serialization/GadgetronSerializers.hpp"
#include "connection/Writers.h"
#include "gadgetron_protocol/Any.hpp"
#include "io/primitives.h"
#include "readers/AcquisitionReader.h"
#include "readers/ImageReader.h"
#include "readers/WaveformReader.h"
#include <gtest/gtest.h>

using namespace Gadgetron;
using namespace Gadgetron::Core;

TEST(SerDesTest, ConfigFile) {
    gadgetron_protocol::ConfigFile filename("filename.config");
    gadgetron_protocol::any msg(filename);
    Gadgetron::Connection::ConfigFileSerializer ser;
    std::stringstream ss;
    ser.serialize(ss, msg);

    // ConfigReferenceHandler in gadgetron/apps/gadgetron/connection/ConfigConnection.cpp
    auto buffer = IO::read<std::array<char, 1024>>(ss);
    std::string received(buffer.data());

    EXPECT_EQ(received, filename);
}

TEST(SerDesTest, ConfigFileError) {
    std::array<char, 1025> longFilename; // NOLINT(*-pro-type-member-init)
    for (int i = 0; i < 1024; i++) {
        longFilename[i] = 'a';
    }
    longFilename[1024] = '\0';
    gadgetron_protocol::ConfigFile filename(longFilename.data());
    gadgetron_protocol::any msg(filename);
    Gadgetron::Connection::ConfigFileSerializer ser;
    std::stringstream ss;
    EXPECT_THROW({ ser.serialize(ss, msg); }, std::runtime_error);
}

TEST(SerDesTest, ConfigScript) {
    gadgetron_protocol::ConfigScript scriptData("some script data");
    gadgetron_protocol::any msg(scriptData);
    Gadgetron::Connection::ConfigScriptSerializer ser;
    std::stringstream ss;
    ser.serialize(ss, msg);

    // ConfigStringHandler in gadgetron/apps/gadgetron/connection/ConfigConnection.cpp
    auto received = IO::read_string_from_stream<uint32_t>(ss);

    EXPECT_EQ(received, scriptData);
}

TEST(SerDesTest, Header) {
    ISMRMRD::IsmrmrdHeader h;
    // required header components
    h.encoding.emplace_back();
    ISMRMRD::MeasurementInformation mi;
    mi.patientPosition = "required";
    h.measurementInformation = mi;

    gadgetron_protocol::any msg(h);
    Gadgetron::Connection::HeaderSerializer ser;
    std::stringstream ss;
    ser.serialize(ss, msg);

    // HeaderHandler in gadgetron/apps/gadgetron/connection/HeaderConnection.cpp
    auto received = IO::read_string_from_stream<uint32_t>(ss);
    ISMRMRD::IsmrmrdHeader header{};
    ISMRMRD::deserialize(received.c_str(), header);

    EXPECT_EQ(header, h);
}

TEST(SerDesTest, Query) {
    std::stringstream ss;

    gadgetron_protocol::Query q("query");
    Gadgetron::Connection::QuerySerializer ser;
    EXPECT_EQ(ser.getId(), Core::MessageID::QUERY);
    auto msg = gadgetron_protocol::any(q);
    ser.serialize(ss, msg);

    // apps/gadgetron/connection/Handlers.cpp ln 69
    auto reserved = IO::read<uint64_t>(ss);
    (void)reserved;
    auto corrId = IO::read<uint64_t>(ss);
    auto query = IO::read_string_from_stream<uint64_t>(ss);
    EXPECT_EQ(corrId, q.correlationId);
    EXPECT_EQ(query, q.query);
}

TEST(SerDesTest, Response) {
    std::stringstream ss;

    auto w = Server::Connection::Writers::ResponseWriter();
    uint64_t corrId = 0;
    std::string response = "response";
    auto r = Core::Response(corrId, response);
    w.serialize(ss, r);

    // gadgetron writers always put message id on stream
    Core::IO::read<uint16_t>(ss);

    Gadgetron::Connection::ResponseDeserializer des;
    auto msg = des.deserialize(ss);
    auto receivedResponse = std::any_cast<gadgetron_protocol::Response&>(msg);
    EXPECT_EQ(receivedResponse.correlationId, corrId);
    EXPECT_EQ(receivedResponse.response, response);
}

TEST(SerDesTest, Image) {
    ISMRMRD::ISMRMRD_ImageHeader hdrBase;
    ISMRMRD::ismrmrd_init_image_header(&hdrBase);
    uint16_t matrixSize[] = {5, 5, 1};
    uint16_t nChannels = 1;
    // ISMRMRD has short but gadgetron reader doesn't support short, use anything but short!!
    // uint16_t dataType = ISMRMRD::ISMRMRD_SHORT;
    uint16_t dataType = ISMRMRD::ISMRMRD_USHORT;
    hdrBase.matrix_size[0] = matrixSize[0];
    hdrBase.matrix_size[1] = matrixSize[1];
    hdrBase.matrix_size[2] = matrixSize[2];
    hdrBase.channels = nChannels;
    hdrBase.data_type = dataType;
    hdrBase.measurement_uid = 1234;
    auto hdr = ISMRMRD::ImageHeader(hdrBase);

    ISMRMRD::MetaContainer meta;
    meta.append("Map Type", "B1");
    std::stringstream ss;
    ISMRMRD::serialize(meta, ss);
    std::string attributeString = ss.str();

    ISMRMRD::Image<uint16_t> testImg;
    testImg.setHead(hdr);
    testImg.setAttributeString(attributeString);

    ss.str("");
    Gadgetron::Connection::ImageSerializer ser;
    Gadgetron::Connection::ImageDeserializer des;

    auto msg = gadgetron_protocol::any(testImg);
    Core::IO::write<uint16_t>(ss, ser.getId());
    ser.serialize(ss, msg);
    auto ssCopy = ss.str();

    ASSERT_EQ(Core::IO::read<uint16_t>(ss), Core::MessageID::GADGET_MESSAGE_ISMRMRD_IMAGE);

    auto receivedMessage = des.deserialize(ss);
    auto receivedImage = gadgetron_protocol::any_cast<gadgetron_protocol::WrappedImage<uint16_t>>(receivedMessage);

    ASSERT_EQ(testImg.getHead(), receivedImage.getHead());
    auto attr1 = static_cast<std::string>(testImg.getAttributeString());
    auto attr2 = static_cast<std::string>(receivedImage.getAttributeString());
    ASSERT_EQ(attr1, attr2);

    EXPECT_TRUE(0 == std::memcmp(testImg.getDataPtr(), receivedImage.getDataPtr(), testImg.getDataSize()));

    // since our serializer/deserializer does not use gadgetron reader/writer, double check gadgetron can read our
    // serialization
    ss.str(ssCopy);
    Core::IO::read<uint16_t>(ss);
    Core::Readers::ImageReader rdr;
    auto gadMsg = rdr.read(ss);

    auto dataPtrMsg = reinterpret_cast<Core::TypedMessageChunk<hoNDArray<uint16_t>>*>(gadMsg.messages()[1].get())
                          ->data.get_data_ptr();
    EXPECT_TRUE(0 == std::memcmp(testImg.getDataPtr(), dataPtrMsg, testImg.getDataSize()));
    auto [gadHdr, gadData, gadMeta] = Core::force_unpack<Core::Image<uint16_t>>(std::move(gadMsg));
    auto dataPtrUnpacked = gadData.get_data_ptr();

    // if this fails in the future then our image, acquisition, and waveform serializers and deserializers should
    // change to use gadgetron's writers & readers
    ASSERT_NE(dataPtrMsg, dataPtrUnpacked);

    ASSERT_EQ(gadHdr, testImg.getHead());
    std::stringstream ss3;
    ISMRMRD::serialize(gadMeta.value(), ss3);
    ASSERT_EQ(attr1, ss3.str());
    EXPECT_TRUE(0 == std::memcmp(testImg.getDataPtr(), dataPtrUnpacked, testImg.getDataSize()));
}

TEST(SerDesTest, Acquisition) {
    ISMRMRD::Acquisition testAcq(100, 1, 1);
    ISMRMRD::AcquisitionHeader hdr = testAcq.getHead();
    hdr.measurement_uid = 1;
    testAcq.setHead(hdr);

    std::stringstream ss;

    Gadgetron::Connection::AcquisitionSerializer ser;
    Gadgetron::Connection::AcquisitionDeserializer des;

    auto msg = gadgetron_protocol::any(testAcq);
    Core::IO::write<uint16_t>(ss, ser.getId());
    ser.serialize(ss, msg);
    auto ssCopy = ss.str();

    ASSERT_EQ(Core::IO::read<uint16_t>(ss), Core::MessageID::GADGET_MESSAGE_ISMRMRD_ACQUISITION);

    auto receivedMessage = des.deserialize(ss);
    auto receivedAcquisition = gadgetron_protocol::any_cast<gadgetron_protocol::WrappedAcquisition>(receivedMessage);

    ASSERT_EQ(testAcq.getHead(), receivedAcquisition.getHead());
    EXPECT_TRUE(0 == std::memcmp(testAcq.getDataPtr(), receivedAcquisition.getDataPtr(), testAcq.getDataSize()));
    EXPECT_TRUE(0 == std::memcmp(testAcq.getTrajPtr(), receivedAcquisition.getTrajPtr(), testAcq.getTrajSize()));

    // since our serializer/deserializer does not use gadgetron reader/writer, double check gadgetron can read our
    // serialization
    ss.str(ssCopy);
    Core::IO::read<uint16_t>(ss);
    Core::Readers::AcquisitionReader rdr;
    auto gadMsg = rdr.read(ss);
    auto [gadHdr, gadData, gadTraj] = Core::force_unpack<Core::Acquisition>(std::move(gadMsg));
    ASSERT_EQ(testAcq.getHead(), gadHdr);
    EXPECT_TRUE(0 == std::memcmp(testAcq.getDataPtr(), gadData.get_data_ptr(), testAcq.getDataSize()));
    EXPECT_TRUE(0 == std::memcmp(testAcq.getTrajPtr(), gadTraj.value().get_data_ptr(), testAcq.getTrajSize()));
}

TEST(SerDesTest, Waveform) {
    ISMRMRD::Waveform testWaveform(100, 1);
    testWaveform.head.measurement_uid = 1;
    std::stringstream ss;

    Gadgetron::Connection::WaveformSerializer ser;
    Gadgetron::Connection::WaveformDeserializer des;

    auto msg = gadgetron_protocol::any(testWaveform);
    Core::IO::write<uint16_t>(ss, ser.getId());
    ser.serialize(ss, msg);
    auto ssCopy = ss.str();

    ASSERT_EQ(Core::IO::read<uint16_t>(ss), Core::MessageID::GADGET_MESSAGE_ISMRMRD_WAVEFORM);

    auto receivedMessage = des.deserialize(ss);
    auto receivedWaveform = gadgetron_protocol::any_cast<ISMRMRD::Waveform>(receivedMessage);

    ASSERT_EQ(receivedWaveform, testWaveform);

    // since our serializer/deserializer does not use gadgetron reader/writer, double check gadgetron can read our
    // serialization
    ss.str(ssCopy);
    Core::IO::read<uint16_t>(ss);
    Core::Readers::WaveformReader rdr;
    Core::Message gadMsg = rdr.read(ss);
    auto [gadHdr, gadData] = Core::force_unpack<Core::Waveform>(std::move(gadMsg));
    ASSERT_EQ(testWaveform.head, gadHdr);
    EXPECT_TRUE(0 == std::memcmp(testWaveform.data, gadData.get_data_ptr(),
                                 (testWaveform.end_data() - testWaveform.begin_data()) * sizeof(uint32_t)));
}
