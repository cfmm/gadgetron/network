#include "../protocol/test/EchoServer.hpp"
#include "../serialization/ConvertingIoStream.hpp"
#include "../serialization/GadgetronDeserializers.hpp"
#include "../serialization/GadgetronSerializers.hpp"
#include "gadgetron_protocol/Deserializers.hpp"
#include "gadgetron_protocol/Helpers.hpp"
#include "gadgetron_protocol/Serializers.hpp"
#include "gadgetron_protocol/WrappedIsmrmrd.hpp"
#include "ismrmrd/ismrmrd.h"
#include "ismrmrd/meta.h"
#include <gtest/gtest.h>
#include <iostream>

namespace ba = boost::asio;
using tcp = boost::asio::ip::tcp;

class ConvertStrmTests : public ::testing::Test {

  public:
    ConvertStrmTests() : ::testing::Test() {

        // --- image to send ---
        ISMRMRD::ISMRMRD_ImageHeader hdrBase;
        ISMRMRD::ismrmrd_init_image_header(&hdrBase);
        hdrBase.matrix_size[0] = 128;
        hdrBase.matrix_size[1] = 128;
        hdrBase.matrix_size[2] = 1;
        hdrBase.channels = 8;
        // test anything except int16_t; gadgetron image reader omits images of type ISMRMRD_SHORT
        //  ( see the map "image_datatypes" in ImageReader.cpp)
        hdrBase.data_type = ISMRMRD::ISMRMRD_USHORT;
        auto hdr = ISMRMRD::ImageHeader(hdrBase);

        ISMRMRD::MetaContainer meta;
        meta.append("Map Type", "B1");
        std::stringstream ss;
        ISMRMRD::serialize(meta, ss);
        std::string attributeString = ss.str();

        testImg.setHead(hdr);
        testImg.setAttributeString(attributeString);
        for (auto& voxel : testImg) {
            voxel = 1;
        }
        // --- image to send ---
    }

    typedef uint16_t ImageDatatype;
    gadgetron_protocol::test::EchoServer echoServer;
    gadgetron_protocol::WrappedImage<ImageDatatype> testImg;
};

TEST_F(ConvertStrmTests, ConvertingIoStream) {
    boost::asio::io_context ioc;

    Gadgetron::Connection::ConvertingIoStream convertingStream(
        gadgetron_protocol::connectSocket("localhost", std::to_string(echoServer.getPort()), ioc));

    convertingStream.addGadgetronSerializer(std::make_unique<Gadgetron::Connection::ImageSerializer>());
    convertingStream.addGadgetronDeserializer(std::make_unique<Gadgetron::Connection::ImageDeserializer>());
    convertingStream.addNetworkSerializer(std::make_unique<gadgetron_protocol::serializers::ImageSerializer>());
    convertingStream.addNetworkDeserializer(std::make_unique<gadgetron_protocol::deserializers::ImageDeserializer>());

    convertingStream.addGadgetronSerializer(std::make_unique<Gadgetron::Connection::CloseSerializer>());
    convertingStream.addGadgetronDeserializer(std::make_unique<Gadgetron::Connection::CloseDeserializer>());
    convertingStream.addNetworkSerializer(std::make_unique<gadgetron_protocol::serializers::CloseSerializer>());
    convertingStream.addNetworkDeserializer(std::make_unique<gadgetron_protocol::deserializers::CloseDeserializer>());
    convertingStream.startConverting();

    auto serializeDeserializer = Gadgetron::Connection::SerializeDeserializeHost();
    serializeDeserializer.addSerializer(std::make_unique<Gadgetron::Connection::ImageSerializer>());
    serializeDeserializer.addDeserializer(std::make_unique<Gadgetron::Connection::ImageDeserializer>());
    serializeDeserializer.addSerializer(std::make_unique<Gadgetron::Connection::CloseSerializer>());

    // test anything except int16_t; gadgetron image reader omits images of type ISMRMRD_SHORT
    //  ( see the map "image_datatypes" in ImageReader.cpp)
    ISMRMRD::Image<ImageDatatype> img(testImg); // NOLINT(*-slicing)
    gadgetron_protocol::any msg{std::move(img)};
    // serializeDeserializer has gadgetron serializer and convertingStream gadgetron read loop has gadgetron
    // deserializer -> network serializer -> echo server
    serializeDeserializer.serialize(convertingStream, msg);

    std::this_thread::sleep_for(std::chrono::seconds(1));
    // convertingStream network read loop has echo server -> network deserializer -> gadgetron serializer
    gadgetron_protocol::any receivedMessage = serializeDeserializer.deserialize(convertingStream);
    // serializeDeserializer has gadgetron deserializer, which puts the image into WrappedImage type
    auto receivedImage = gadgetron_protocol::any_cast<gadgetron_protocol::WrappedImage<ImageDatatype>>(receivedMessage);
    EXPECT_EQ(receivedImage, testImg);

    auto msg2 = gadgetron_protocol::any{gadgetron_protocol::CloseObj()};
    auto status = serializeDeserializer.serialize(convertingStream, msg2);
    EXPECT_EQ(status, true);
}
