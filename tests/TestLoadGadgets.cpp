#include "GadgetTypes.hpp"
#include "demangle.hpp"
#include <boost/dll.hpp>
#include <gtest/gtest.h>

namespace {
template <typename T> class TestLoadGadgetFixture : public testing::Test {};

TYPED_TEST_SUITE(TestLoadGadgetFixture, Gadgetron::Tests::Types);

TYPED_TEST(TestLoadGadgetFixture, test_gadget) {

    boost::dll::shared_library lib = boost::dll::shared_library(
        "cfmm_gadgets", boost::dll::load_mode::append_decorations | boost::dll::load_mode::rtld_global |
                            boost::dll::load_mode::search_system_folders);

    std::string prefix = "gadget_factory_export_";
    std::string gadgetType = demangle(typeid(TypeParam).name());
    auto substrPos = gadgetType.rfind("::");
    if (substrPos != std::string::npos)
        gadgetType = gadgetType.substr(substrPos + 2);
    ASSERT_TRUE(lib.has(prefix + gadgetType));
};
} // namespace
