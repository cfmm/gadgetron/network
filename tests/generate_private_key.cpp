#include <openssl/pem.h>
#include <openssl/rsa.h>

// https://stackoverflow.com/questions/50363097/c-openssl-generate-rsa-keypair-and-read
void generatePrivateKey(const char* filename) {
    int ret;
    RSA* r = nullptr;
    BIGNUM* bne;
    BIO *bpPublic = nullptr, *bpPrivate = nullptr;
    int bits = 2048;
    unsigned long e = RSA_F4;

    // 1. generate rsa key
    bne = BN_new();
    ret = BN_set_word(bne, e);
    if (ret != 1) {
        goto free_all;
    }

    r = RSA_new();
    ret = RSA_generate_key_ex(r, bits, bne, nullptr);
    if (ret != 1) {
        goto free_all;
    }

    // 3. save private key
    bpPrivate = BIO_new_file(filename, "w+");
    // bpPrivate = BIO_new(BIO_s_mem());
    PEM_write_bio_RSAPrivateKey(bpPrivate, r, nullptr, nullptr, 0, nullptr, nullptr);

// 4. free
free_all:

    BIO_free_all(bpPublic);
    BIO_free_all(bpPrivate);
    RSA_free(r);
    BN_free(bne);
}
