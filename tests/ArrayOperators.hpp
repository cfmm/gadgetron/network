//
// Created by Martyn Klassen on 2019-02-28.
//

#ifndef NETWORK_ARRAY_OPERATORS_H
#define NETWORK_ARRAY_OPERATORS_H

#include "hoNDArray.h"

namespace Gadgetron {

template <typename T> bool operator==(hoNDArray<T> const& lhs, hoNDArray<T> const& rhs) {
    if (!(lhs.get_number_of_bytes() == rhs.get_number_of_bytes()))
        return false;
    if (!(lhs.get_number_of_dimensions() == rhs.get_number_of_dimensions()))
        return false;
    for (size_t i = 0; i < lhs.get_number_of_dimensions(); i++)
        if (!(lhs.get_size(i) == rhs.get_size(i)))
            return false;
    return memcmp(lhs.get_data_ptr(), rhs.get_data_ptr(), lhs.get_number_of_bytes()) == 0;
}

} // namespace Gadgetron

#endif // NETWORK_ARRAY_OPERATORS_H
