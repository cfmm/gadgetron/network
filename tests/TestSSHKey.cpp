//
// Created by Martyn Klassen on 2019-06-07.
//

#pragma clang diagnostic push
#pragma ide diagnostic ignored "cert-err58-cpp"
//
// Created by Martyn Klassen on 2019-02-28.
//

#include "SshInterface.hpp"
#include "helpers.hpp"
#include "gmock/gmock.h"
#include <chrono>
#include <iostream>
#include <openssl/evp.h>
#include <openssl/pem.h>
#include <openssl/rsa.h>

namespace {

class MockSshInterface : public SshInterface {
  public:
    bool connectTest(const std::string& remoteHost, const std::string& remotePort, const std::string& userName,
                     const std::string& privateKey, const std::string& password) {
        return SshInterface::connect(remoteHost, remotePort, userName, privateKey, password);
    }

    void disconnectTest() { SshInterface::disconnect(); }
};

class TestSSHKey : public ::testing::Test {
  protected:
    TestSSHKey()
        : root(resolvePath("network/tests/data")), password("testing_password_1234"), filename(root + "/test_cert") {
        // Initialize the OpenSSL library
        OpenSSL_add_all_algorithms();
        ERR_load_crypto_strings();

        // Create the EVP key context
        EVP_PKEY_CTX* ctx = EVP_PKEY_CTX_new_id(EVP_PKEY_RSA, nullptr);
        if (!ctx)
            throw std::runtime_error("Failed to create EVP_PKEY_CTX.");

        // Generate RSA key pair
        if (EVP_PKEY_keygen_init(ctx) <= 0 || EVP_PKEY_CTX_set_rsa_keygen_bits(ctx, 2048) <= 0) {
            EVP_PKEY_CTX_free(ctx);
            throw std::runtime_error("Failed to initialize key generation.");
        }
        EVP_PKEY* pkey = nullptr;
        if (EVP_PKEY_keygen(ctx, &pkey) <= 0) {
            EVP_PKEY_CTX_free(ctx);
            throw std::runtime_error("Failed to generate RSA key pair.");
        }

        std::filesystem::path filepath(filename);

        // PEM PKCS8 encrypted
        BIO* privateKeyBIO = BIO_new_file(filepath.c_str(), "w");
        const EVP_CIPHER* cipher = EVP_aes_256_cbc();
        if (!PEM_write_bio_PKCS8PrivateKey(privateKeyBIO, pkey, cipher, nullptr, 0, nullptr,
                                           const_cast<char*>(password.c_str()))) {
            EVP_PKEY_free(pkey);
            EVP_PKEY_CTX_free(ctx);
            BIO_free_all(privateKeyBIO);
            throw std::runtime_error("Failed to write the private key.");
        }
        BIO_free_all(privateKeyBIO);

        // Pub OpenSSH
        filepath.replace_extension(".pub");
        BIO* publicKeyBIO = BIO_new_file(filepath.c_str(), "w");
        if (!PEM_write_bio_PUBKEY(publicKeyBIO, pkey)) {
            EVP_PKEY_free(pkey);
            EVP_PKEY_CTX_free(ctx);
            BIO_free_all(publicKeyBIO);
            throw std::runtime_error("Failed to write the public key.");
        }
        BIO_free_all(publicKeyBIO);

        // PEM PKCS8 unencrypted
        filepath.replace_extension(".unencrypt");
        privateKeyBIO = BIO_new_file(filepath.c_str(), "w");
        if (!PEM_write_bio_PKCS8PrivateKey(privateKeyBIO, pkey, nullptr, nullptr, 0, nullptr, nullptr)) {
            EVP_PKEY_free(pkey);
            EVP_PKEY_CTX_free(ctx);
            BIO_free_all(privateKeyBIO);
            std::cout << "Failed to write the private key." << std::endl;
        }
        using std::filesystem::perms;
        std::filesystem::permissions(filepath, perms::owner_read | perms::owner_write,
                                     std::filesystem::perm_options::replace);
        BIO_free_all(privateKeyBIO);

        // PEM TraditionalOpenSSL encrypted
        filepath.replace_extension(".openssl");
        privateKeyBIO = BIO_new_file(filepath.c_str(), "w");
        if (!PEM_write_bio_PrivateKey(privateKeyBIO, pkey, cipher, nullptr, 0, nullptr,
                                      const_cast<char*>(password.c_str()))) {
            EVP_PKEY_free(pkey);
            EVP_PKEY_CTX_free(ctx);
            BIO_free_all(privateKeyBIO);
            std::cout << "Failed to write the private key." << std::endl;
        }
        std::filesystem::permissions(filepath, perms::owner_read | perms::owner_write,
                                     std::filesystem::perm_options::replace);
        BIO_free_all(privateKeyBIO);

        // PEM TraditionalOpenSSL unencrypted
        filepath.replace_extension(".unencrypt2");
        privateKeyBIO = BIO_new_file(filepath.c_str(), "w");
        if (!PEM_write_bio_PrivateKey(privateKeyBIO, pkey, nullptr, nullptr, 0, nullptr, nullptr)) {
            EVP_PKEY_free(pkey);
            EVP_PKEY_CTX_free(ctx);
            BIO_free_all(privateKeyBIO);
            std::cout << "Failed to write the private key." << std::endl;
        }
        std::filesystem::permissions(filepath, perms::owner_read | perms::owner_write,
                                     std::filesystem::perm_options::replace);
        BIO_free_all(privateKeyBIO);

        // Clean up resources
        EVP_PKEY_free(pkey);
        EVP_PKEY_CTX_free(ctx);
    }

    ~TestSSHKey() override = default;

    std::string root;
    std::string password;
    std::string filename;
};

TEST_F(TestSSHKey, parseKey) {
    std::string privateKey(filename);

    PublicPrivateKey a(privateKey, password);

    std::ifstream t(filename + ".unencrypt2");

    EXPECT_EQ(std::string(std::istreambuf_iterator<char>(t), std::istreambuf_iterator<char>()), a.getPrivateKey())
        << "Private keys differ";
    t.close();

    t.open(filename + ".pub");
    EXPECT_EQ(std::string(std::istreambuf_iterator<char>(t), std::istreambuf_iterator<char>()), a.getPublicKey())
        << "Public keys differ";
    t.close();

    std::string remoteHost("localhost");
    std::string userName("test_user");
    std::string remotePort("2200");
    std::string key = filename + ".unencrypt";
    std::string cmd = root + "/ssh_server.py " + key + " " + remotePort + " " + userName + " &";
    EXPECT_EQ(std::system(cmd.c_str()), 0) << "Failed to start ssh server";

    cmd = "openssl rsa -in " + filename + ".unencrypt -noout";
    EXPECT_EQ(std::system(cmd.c_str()), 0) << key << " is not a valid key file";

    cmd = "openssl rsa -in " + filename + ".unencrypt2 -noout";
    EXPECT_EQ(std::system(cmd.c_str()), 0) << key << " is not a valid key file";

    MockSshInterface inter;
    bool result = false;
    int count = 0;
    using namespace std::chrono_literals;
    while (!result && count++ < 5) {
        std::this_thread::sleep_for(2s);
        result = inter.connectTest(remoteHost, remotePort, userName, privateKey, password);
    }
    EXPECT_EQ(result, true);
    inter.disconnectTest();
}

} // namespace

#pragma clang diagnostic pop
