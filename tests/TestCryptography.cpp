#pragma clang diagnostic push
#pragma ide diagnostic ignored "cert-err58-cpp"

#include "Cryptography.hpp"
#include "log.h"
#include "gmock/gmock.h"

namespace {

class TestCryptography : public ::testing::Test {
  protected:
    TestCryptography() = default;

    ~TestCryptography() override = default;
};

TEST_F(TestCryptography, shuffle) {

    static const size_t n = 32;
    size_t data[n];

    for (size_t i = 0; i < n; i++)
        data[i] = i;

    std::string passphrase("test_passphrase");
    unsigned char salt[PKCS5_SALT_LEN];

    Gadgetron::Network::Cryptography::shuffle(data, n, passphrase, salt);

    Gadgetron::Network::Cryptography::deshuffle(data, n, passphrase, salt);

    for (size_t i = 0; i < n; i++)
        ASSERT_EQ(data[i], i) << "post-shuffle data mismatch at " << i;
}

TEST_F(TestCryptography, encrpyt) {
    std::string passphrase("test_passphrase");
    std::string aad("some_test");

    std::string msg("This message will be encrypted");

    std::string originalMessage(msg);
    ASSERT_TRUE(Gadgetron::Network::Cryptography::encrypt(passphrase, aad, msg)) << "Failed to encrypt message";

    std::string encryptedMessage(msg);

    ASSERT_TRUE(Gadgetron::Network::Cryptography::decrypt(passphrase, aad, msg)) << "Failed to decrypt message";

    ASSERT_EQ(originalMessage, msg) << "Encrypt/Decrypt pass failed";
}

TEST_F(TestCryptography, decryptPassphrase) {
    std::string passphrase("test_passphrase");
    std::string aad("some_test");
    std::string msg("This message will be encrypted");
    std::string originalMessage(msg);
    ASSERT_TRUE(Gadgetron::Network::Cryptography::encrypt(passphrase, aad, msg)) << "Failed to encrypt message";
    Gadgetron::GadgetronLogger::instance()->disableLogLevel(Gadgetron::GadgetronLogLevel::GADGETRON_LOG_LEVEL_ERROR);
    ASSERT_FALSE(Gadgetron::Network::Cryptography::decrypt("wrong", aad, msg)) << "Decrypted with wrong passphrase";
    Gadgetron::GadgetronLogger::instance()->enableLogLevel(Gadgetron::GadgetronLogLevel::GADGETRON_LOG_LEVEL_ERROR);
}

TEST_F(TestCryptography, decryptADD) {
    std::string passphrase("test_passphrase");
    std::string aad("some_test");
    std::string msg("This message will be encrypted");
    std::string originalMessage(msg);
    ASSERT_TRUE(Gadgetron::Network::Cryptography::encrypt(passphrase, aad, msg)) << "Failed to encrypt message";
    Gadgetron::GadgetronLogger::instance()->disableLogLevel(Gadgetron::GadgetronLogLevel::GADGETRON_LOG_LEVEL_ERROR);
    ASSERT_FALSE(Gadgetron::Network::Cryptography::decrypt(passphrase, "wrong", msg))
        << "Decrypted with wrong additional data";
    Gadgetron::GadgetronLogger::instance()->enableLogLevel(Gadgetron::GadgetronLogLevel::GADGETRON_LOG_LEVEL_ERROR);
}

TEST_F(TestCryptography, decryptPassPhraseADD) {
    std::string passphrase("test_passphrase");
    std::string aad("some_test");
    std::string msg("This message will be encrypted");
    std::string originalMessage(msg);
    ASSERT_TRUE(Gadgetron::Network::Cryptography::encrypt(passphrase, aad, msg)) << "Failed to encrypt message";
    Gadgetron::GadgetronLogger::instance()->disableLogLevel(Gadgetron::GadgetronLogLevel::GADGETRON_LOG_LEVEL_ERROR);
    ASSERT_FALSE(Gadgetron::Network::Cryptography::decrypt("wrong", "wrong", msg))
        << "Decrypted with wrong passphrase and additional data";
    Gadgetron::GadgetronLogger::instance()->enableLogLevel(Gadgetron::GadgetronLogLevel::GADGETRON_LOG_LEVEL_ERROR);
}

TEST_F(TestCryptography, rrng) {
    typedef rlcg::ReversibleHash RrngType;
    typedef std::uniform_int_distribution<size_t> DistributionType;
    typedef DistributionType::param_type ParamType;
    DistributionType d;
    RrngType::result_type key = 1;

    RrngType g(key);
    static constexpr size_t n = 10;

    RrngType::result_type data[n];
    for (RrngType::result_type& i : data)
        i = g();

    g.reverse();

    for (size_t i = 0; i < n; i++)
        ASSERT_EQ(data[n - i - 1], g()) << "mismatch in RRNG";
}

TEST_F(TestCryptography, rlcg) {
    typedef rlcg::ReversibleLCG RlcgType;
    typedef std::uniform_int_distribution<size_t> DistributionType;
    typedef DistributionType::param_type ParamType;
    DistributionType d;
    constexpr RlcgType::result_type key = 1;

    RlcgType g(key);
    static constexpr size_t n = 10;

    RlcgType::result_type data[n];
    for (RlcgType::result_type& i : data) {
        i = g();
    }

    g.reverse();

    for (size_t i = 0; i < n; i++) {
        RlcgType::result_type v = g();
        ASSERT_EQ(data[n - i - 1], v) << "mismatch in RLCG";
    }
}
} // namespace

#pragma clang diagnostic pop
