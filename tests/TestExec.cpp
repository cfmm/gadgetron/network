#pragma clang diagnostic push
#pragma ide diagnostic ignored "cert-err58-cpp"
//
// Created by akuurstr on 2021-02-04.
//

#include "exec.hpp"
#include "gmock/gmock.h"

namespace {
class TestExec : public ::testing::Test {
  protected:
    TestExec() = default;

    ~TestExec() override = default;
};

TEST_F(TestExec, success_system_call) {
    std::string cmd = "ls";
    std::tuple<int, std::string> result = execTimeout(cmd, 10);
    EXPECT_EQ(std::get<0>(result), 0) << "Valid system call " << cmd
                                      << " received return code: " << std::get<0>(result);
}

TEST_F(TestExec, timeout_system_call) {
    std::string cmd = "sleep 5";
    std::tuple<int, std::string> result = execTimeout(cmd, 1);
    EXPECT_EQ(std::get<0>(result), ETIME)
        << "Timed out system call " << cmd << " received return code: " << std::get<0>(result);
}

} // namespace

#pragma clang diagnostic pop
