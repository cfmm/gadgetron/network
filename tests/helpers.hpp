//
// Created by Martyn Klassen on 2021-02-12.
//

#ifndef NETWORK_TEST_HELPERS_H
#define NETWORK_TEST_HELPERS_H

#include "Channel.h"
#include "Context.h"
#include "Node.h"
#include <filesystem>
#include <gtest/gtest.h>
#include <ismrmrd/ismrmrd.h>
#include <ismrmrd/version.h>
#include <ismrmrd/xml.h>

inline std::string resolvePath(const std::string& relPath) {
    namespace fs = std::filesystem;
    auto baseDir = fs::current_path();
    while (baseDir.has_parent_path()) {
        auto combinePath = baseDir / relPath;
        if (fs::exists(combinePath)) {
            return combinePath.string();
        }
        baseDir = baseDir.parent_path();
    }
    std::stringstream msg;
    msg << relPath << " not found from " << fs::current_path();
    throw std::runtime_error(msg.str());
}

using namespace Gadgetron;

class GadgetTester : public ::testing::Test {
  protected:
    GadgetTester() : context() {
        ISMRMRD::Encoding encoding;
        encoding.encodedSpace = {{32, 32, 1}, {256, 256, 5}};
        encoding.encodingLimits.kspace_encoding_step_0 = ISMRMRD::Limit();
        encoding.encodingLimits.kspace_encoding_step_1 = ISMRMRD::Limit();
        encoding.encodingLimits.kspace_encoding_step_0->minimum = 0;
        encoding.encodingLimits.kspace_encoding_step_0->center = 16;
        encoding.encodingLimits.kspace_encoding_step_0->maximum = 31;
        encoding.encodingLimits.kspace_encoding_step_1->minimum = 0;
        encoding.encodingLimits.kspace_encoding_step_1->center = 16;
        encoding.encodingLimits.kspace_encoding_step_1->maximum = 31;
        encoding.reconSpace = {{32, 32, 1}, {256, 256, 5}};
        encoding.trajectory = ISMRMRD::TrajectoryType::CARTESIAN;
        context.header.experimentalConditions.H1resonanceFrequency_Hz = 63.87 * 1e6;
        context.header.encoding = {encoding};

        ISMRMRD::SubjectInformation subjectInfo;
        subjectInfo.patientID = "fake_ID";
        context.header.subjectInformation = subjectInfo;
    }

    ~GadgetTester() override = default;

    Gadgetron::Core::Context context;

    typedef std::pair<Core::ChannelPair, Core::ChannelPair> Channels;

    class TerminatingChannel : public Core::MessageChannel {
      public:
        explicit TerminatingChannel(size_t max) : max(max) {}

      private:
        size_t count = 0;
        size_t max = 0;

      public:
        Core::Message pop() override {
            if (count <= max)
                channel.close();
            count++;
            return Core::MessageChannel::pop();
        }
    };

    static Channels generateChannels(size_t count) {
        return {Core::make_channel<TerminatingChannel>(count), Core::make_channel<Core::MessageChannel>()};
    }

    static void runGadget(Core::GenericChannelGadget& gadget, Channels&& channels) {
        gadget.process(channels.first.input, channels.second.output);

        while (channels.second.input.try_pop()) {
        }
    }
    static void runGadget(Core::GenericChannelGadget& gadget) { runGadget(gadget, generateChannels(0)); }
    static void runGadget(Core::GenericChannelGadget& gadget, Core::AnyImage&& data) {
        auto channels = generateChannels(1);
        channels.first.output.push(std::forward<Core::AnyImage>(data));
        runGadget(gadget, std::move(channels));
    }

    static void runGadget(Core::GenericChannelGadget& gadget, std::vector<Core::AnyImage>&& data) {
        auto channels = generateChannels(data.size());
        for (auto image : data)
            channels.first.output.push(image);
        runGadget(gadget, std::move(channels));
    }

    template <typename T> Core::Image<T> makeImage() {
        ISMRMRD::ImageHeader header;
        Gadgetron::hoNDArray<T> data;
        ISMRMRD::MetaContainer meta;

        header.version = ISMRMRD_VERSION_MAJOR;
        header.data_type = ISMRMRD::Image<T>().getDataType();
        header.flags = 10;
        header.measurement_uid = 3332;
        header.matrix_size[0] = 1;
        header.matrix_size[1] = 1;
        header.matrix_size[2] = 1;
        header.field_of_view[0] = 1.0f;
        header.field_of_view[1] = 1.0f;
        header.field_of_view[2] = 1.0f;
        header.channels = 2;
        header.position[0] = 0.0f;
        header.position[1] = 0.0f;
        header.position[2] = 0.0f;
        header.read_dir[0] = 1.0f;
        header.read_dir[1] = 0.0f;
        header.read_dir[2] = 0.0f;
        header.phase_dir[0] = 0.0f;
        header.phase_dir[1] = 1.0f;
        header.phase_dir[2] = 0.0f;
        header.slice_dir[0] = 0.0f;
        header.slice_dir[1] = 0.0f;
        header.slice_dir[2] = 1.0f;
        header.patient_table_position[0] = 0.0f;
        header.patient_table_position[1] = 0.0f;
        header.patient_table_position[2] = 0.0f;
        header.average = 0;
        header.slice = 1;
        header.contrast = 2;
        header.phase = 3;
        header.repetition = 4;
        header.set = 5;

        data.create(header.matrix_size[0], header.matrix_size[1], header.matrix_size[2], header.channels);
        for (size_t i = 0; i < data.get_number_of_elements(); i++)
            data[i] = i;
        return {header, std::move(data), std::move(meta)};
    }
};

#endif // NETWORK_TEST_HELPERS_H
