//
// Created by Martyn Klassen on 2019-02-28.
//

#include "H5Cpp.h"
#include "LoadIsmrmrdDatasetChannels.hpp"
#include "SaveIsmrmrdDataset.hpp"
#include "generate_private_key.hpp"
#include "helpers.hpp"
#include "io/from_string.h"
#include "gmock/gmock.h"
#include <regex>

namespace {

class MockSaveIsmrmrdDatasetImages : public Gadgetron::SaveIsmrmrdDataset {
  public:
    MockSaveIsmrmrdDatasetImages(const Gadgetron::Core::Context& context,
                                 const Gadgetron::Core::GadgetProperties& properties)
        : SaveIsmrmrdDataset::SaveIsmrmrdDataset(context, properties) {
        ON_CALL(*this, execTimeout).WillByDefault(testing::Return(std::tuple<int, std::string>{0, ""}));
    }

    MOCK_METHOD((std::tuple<int, std::string>), execTimeout, (std::string&, long));
    MOCK_METHOD(void, unmount, ());

    auto getFilePath() { return mountPath / relativeFilePath; }
    ~MockSaveIsmrmrdDatasetImages() override { std::filesystem::remove_all(mountPath); }
};

class TestSaveLoad : public GadgetTester {
  protected:
    TestSaveLoad() {
        properties.insert({"filename", "test.ismrmrd"});
        properties.insert({"passphrase", "secret"});
        properties.insert({"groupname", "data"});
        properties.insert({"varname", "images"});

        // required to avoid error in gadget process function
        properties.insert({"remotemounts", "TestRemote"});
        properties.insert({"privatekey", "test_key.pem"});
        properties.insert({"username", "test_user"});
        properties.insert({"mountpoint", "fakeRemoteMount"});

        // for regex testing sshfs call
        properties.insert({"ssh_cmd", "/usr/bin/ssh"});
        properties.insert({"sshfs_cmd", "/usr/bin/sshfs"});
        properties.insert({"sshfs_timeout", "5"});

        generatePrivateKey(properties.at("privatekey").c_str());
    };

    ~TestSaveLoad() override { std::filesystem::remove(properties.at("privatekey")); }

    Core::GadgetProperties properties;
};
}; // namespace

TEST_F(TestSaveLoad, image) {
    MockSaveIsmrmrdDatasetImages gadget(context, properties);
    std::stringstream cmd;
    cmd << "^" << properties.at("sshfs_cmd") << " " << properties.at("username") << "@" << properties.at("remotemounts")
        << " \\\"" << properties.at("mountpoint") << ".*\\\"";
    std::stringstream op1;
    op1 << "-o.*[, ]idmap=user";
    std::stringstream op2;
    op2 << "-o.*[, ]identityfile=";
    std::stringstream op3;
    op3 << "-o.*[, ]StrictHostKeyChecking=no";
    std::stringstream op4;
    op4 << "-o.*[, ]ConnectTimeout=" << properties.at("sshfs_timeout");
    std::stringstream op5;
    op5 << "-o.*[, ]ssh_command='" << properties.at("ssh_cmd") << "'";

    auto cmdMatcher = testing::AllOf(testing::ContainsRegex(cmd.str()), testing::ContainsRegex(op1.str()),
                                     testing::ContainsRegex(op2.str()), testing::ContainsRegex(op3.str()),
                                     testing::ContainsRegex(op4.str()), testing::ContainsRegex(op5.str()));
    EXPECT_CALL(gadget, execTimeout(cmdMatcher, std::stol(properties.at("sshfs_timeout"))))
        .WillOnce(testing::Return(std::tuple<int, std::string>{0, ""}));
    EXPECT_CALL(gadget, unmount()).Times(2);
    runGadget(gadget, makeImage<complex_float_t>());
    auto createdFile = gadget.getFilePath();

    ASSERT_TRUE(std::filesystem::exists(createdFile));
    H5::H5File file(createdFile.c_str(), H5F_ACC_RDONLY);
    ASSERT_EQ(file.getNumObjs(), 1);
    ASSERT_EQ(file.getObjnameByIdx(0), properties.at("groupname"));

    H5::Group group = file.openGroup(properties.at("groupname"));
    ASSERT_EQ(group.getNumObjs(), 2);
    ASSERT_EQ(group.getObjnameByIdx(0), properties.at("varname"));
    ASSERT_EQ(group.getObjnameByIdx(1), "xml");

    properties.find("filename")->second = createdFile.string();
    Gadgetron::LoadIsmrmrdDatasetImages gadget2(context, properties);
    runGadget(gadget2);
}

TEST_F(TestSaveLoad, fallback) {
    properties.insert({"remotemounts", "128.0.0.1:/test2\nTestRemote"});

    MockSaveIsmrmrdDatasetImages gadget(context, properties);

    using ::testing::HasSubstr;
    auto remotemounts = Gadgetron::Core::IO::from_string<std::vector<std::string>>(properties.at("remotemounts"));
    auto end = remotemounts.end();
    auto iter = remotemounts.begin();
    while (iter != end) {
        EXPECT_CALL(gadget, execTimeout(HasSubstr("@" + *iter++), testing::_))
            .WillOnce(testing::Return(std::tuple<int, std::string>{1, ""}));
    }
    EXPECT_CALL(gadget, unmount()).Times(static_cast<int>(remotemounts.size() + 1));
    try {
        runGadget(gadget, makeImage<complex_float_t>());
    } catch (std::invalid_argument const& err) {
        EXPECT_EQ(err.what(), std::string("Bad remotemounts parameter - failure"));
    } catch (...) {
        FAIL() << "Expected std::invalid_argument";
    }
}
