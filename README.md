Local gitlab-runner
===================
Due to how submodules are handled, the ace-ismrmrd toolkit must be available locally in `../` as a bare repository.

```bash
git clone --bare https://git.cfmm.robarts.ca/lklasse4/ace-ismrmrd-toolkit.git ../ace-ismrmrd-toolkit.git
```

This module is used as a submodule of a gadgetron repository and therefore will typically reference `../.git/module/network`
rather than `.git` for the local repository. This requires that `../` be available within the docker runner. This can be
accomplished by mapping a volume into the docker image.

```sh
pushd .. && MYLOCATION=`pwd` && popd && gitlab-runner exec docker --cache-dir /cache --docker-volumes `pwd`/ci:/cache --docker-volumes ${MYLOCATION}:${MYLOCATION} build:linux
```

Unfortunately, `gitlab-runner exec` does not work with `extends` as documented in https://gitlab.com/gitlab-org/gitlab-runner/issues/3794
It has been a known issue for over a year, so a fix is not likely any time soon. Gitlab CI uses a common script instead of extends.

