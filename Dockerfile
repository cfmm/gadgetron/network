ARG PROTOCOL_TAG
FROM registry.gitlab.com/cfmm/gadgetron/connection/core:${PROTOCOL_TAG:-latest} AS core

RUN apt update && apt -y upgrade \
    clang \
    libfftw3-dev \
    liblapacke64-dev \
    libace-dev \
    liblapacke-dev \
    #libatlas-base-dev \ # incompatible with liblapacke-dev
    libarmadillo-dev \
    libtiff5-dev \
    libpng-dev \
    libxml2-dev \
    libsndfile-dev \
    libwrap0-dev \
    libdcmtk-dev \
    libssh2-1-dev \
    libnsl-dev \
    libopenmpi-dev \
    libicu-dev \
    liblzma-dev \
    libbz2-dev \
    libzstd-dev \
    python3-dev \
    python3-numpy \
    python3-cryptography \
    python3-paramiko \
    librange-v3-dev \
    libhowardhinnant-date-dev \
    nlohmann-json3-dev \
    && apt-get clean

# libssh2 compilation if custom version is required
#RUN cd /build \
# && wget https://www.libssh2.org/download/libssh2-1.11.0.tar.gz \
# && tar xf libssh*.tar.gz \
# && cd libssh2-1.11.0 \
# && ./configure --prefix=/usr --disable-static \
# && make -j2 \
# && make install \
# && ldconfig

# dcmtk compilation if custom version is required
#RUN cd /build \
# && git clone https://github.com/dcmtk/dcmtk \
# && cd dcmtk \
# && git checkout DCMTK-3.6.7 \
# && mkdir /build/dcmtk_build \
# && cd /build/dcmtk_build \
# && cmake -D BUILD_SHARED_LIBS=ON -D CMAKE_POSITION_INDEPENDENT_CODE=TRUE ../dcmtk \
# && make -j2 \
# && make install \
# && ldconfig

FROM core AS full

RUN apt update && apt -y upgrade \
    make \
    autoconf \
    automake \
    libtool \
    valgrind \
    rsync \
    locales-all \
    dos2unix \
    && apt-get clean

FROM core AS runner

RUN apt update && apt -y upgrade \
    golang-go \
    && apt-get clean

RUN cd /build  \
    && git clone https://github.com/ismrmrd/mrd-storage-server  \
    && cd mrd-storage-server  \
    && git checkout v0.0.12  \
    && go build && GOBIN=/usr/local/bin/ go install \
    && cd / && rm -rf /build/mrd-storage-server
