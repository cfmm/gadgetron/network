#ifndef CONVERTING_STREAM_FROM_SOCKET_HPP
#define CONVERTING_STREAM_FROM_SOCKET_HPP
#include <boost/asio.hpp>
#include <memory>

#include "ConvertingIoStream.hpp"
#include "GadgetronDeserializers.hpp"
#include "GadgetronSerializers.hpp"
#include "gadgetron_protocol/Deserializers.hpp"
#include "gadgetron_protocol/Serializers.hpp"

namespace Gadgetron::Connection {
std::unique_ptr<std::iostream> convertingStreamFromSocket(std::unique_ptr<boost::asio::ip::tcp::socket> socket) {

    auto converter = std::make_unique<ConvertingIoStream>(std::move(socket));

    converter->addNetworkSerializer(std::make_unique<gadgetron_protocol::serializers::ResponseSerializer>());
    converter->addNetworkSerializer(std::make_unique<gadgetron_protocol::serializers::ImageSerializer>());
    converter->addNetworkSerializer(std::make_unique<gadgetron_protocol::serializers::AcquisitionSerializer>());
    converter->addNetworkSerializer(std::make_unique<gadgetron_protocol::serializers::WaveformSerializer>());
    converter->addNetworkSerializer(std::make_unique<gadgetron_protocol::serializers::CloseSerializer>());

    converter->addNetworkDeserializer(std::make_unique<gadgetron_protocol::deserializers::ConfigFileDeserializer>());
    converter->addNetworkDeserializer(std::make_unique<gadgetron_protocol::deserializers::ConfigScriptDeserializer>());
    converter->addNetworkDeserializer(std::make_unique<gadgetron_protocol::deserializers::HeaderDeserializer>());
    converter->addNetworkDeserializer(std::make_unique<gadgetron_protocol::deserializers::QueryDeserializer>());
    converter->addNetworkDeserializer(std::make_unique<gadgetron_protocol::deserializers::ImageDeserializer>());
    converter->addNetworkDeserializer(std::make_unique<gadgetron_protocol::deserializers::AcquisitionDeserializer>());
    converter->addNetworkDeserializer(std::make_unique<gadgetron_protocol::deserializers::WaveformDeserializer>());
    converter->addNetworkDeserializer(std::make_unique<gadgetron_protocol::deserializers::CloseDeserializer>());

    converter->addGadgetronSerializer(std::make_unique<ConfigFileSerializer>());
    converter->addGadgetronSerializer(std::make_unique<ConfigScriptSerializer>());
    converter->addGadgetronSerializer(std::make_unique<HeaderSerializer>());
    converter->addGadgetronSerializer(std::make_unique<QuerySerializer>());
    converter->addGadgetronSerializer(std::make_unique<ImageSerializer>());
    converter->addGadgetronSerializer(std::make_unique<AcquisitionSerializer>());
    converter->addGadgetronSerializer(std::make_unique<WaveformSerializer>());
    converter->addGadgetronSerializer(std::make_unique<CloseSerializer>());

    converter->addGadgetronDeserializer(std::make_unique<ResponseDeserializer>());
    converter->addGadgetronDeserializer(std::make_unique<ImageDeserializer>());
    converter->addGadgetronDeserializer(std::make_unique<AcquisitionDeserializer>());
    converter->addGadgetronDeserializer(std::make_unique<WaveformDeserializer>());
    converter->addGadgetronDeserializer(std::make_unique<CloseDeserializer>());

    converter->startConverting();

    return converter;
}
} // namespace Gadgetron::Connection

#endif // CONVERTING_STREAM_FROM_SOCKET_HPP
