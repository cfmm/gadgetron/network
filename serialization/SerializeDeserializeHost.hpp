#pragma once
#ifndef SERIALIZE_DESERIALIZE_HOST_HPP
#define SERIALIZE_DESERIALIZE_HOST_HPP

#include "gadgetron_protocol/SerDes.hpp"

namespace Gadgetron::Connection { // NOLINT(*-identifier-naming)
using namespace gadgetron_protocol;

class DLL_EXPORT SerializeDeserializeHost : public SerDes {
  public:
    void serializeId(std::ostream& ostream, uint16_t id) const override;
    uint16_t deserializeId(std::istream& istream) const override;
};
} // namespace Gadgetron::Connection

#endif // SERIALIZE_DESERIALIZE_HOST_HPP
