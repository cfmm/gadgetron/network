#pragma once
#ifndef CONVERTING_IO_STREAM_HPP
#define CONVERTING_IO_STREAM_HPP

#include "SerializeDeserializeHost.hpp"
#include "gadgetron_protocol/BoostAsio.hpp"
#include "gadgetron_protocol/Export.hpp"
#include "gadgetron_protocol/Helpers.hpp"
#include "gadgetron_protocol/IoStream.hpp"
#include "gadgetron_protocol/SerDes.hpp"
#include "gadgetron_protocol/deserializers/Deserializer.hpp"
#include "gadgetron_protocol/serializers/Serializer.hpp"
#include <boost/asio/spawn.hpp>
#include <list>
#include <thread>

namespace Gadgetron::Connection {
using namespace gadgetron_protocol;
class DLL_EXPORT ConvertingIoStream : public IoStream {
  public:
    typedef SerDes::SerializerPointer SerializerPointer;
    typedef SerDes::DeserializerPointer DeserializerPointer;
    [[maybe_unused]] typedef SerDes::SerializerList SerializerList;
    [[maybe_unused]] typedef SerDes::DeserializerList DeserializerList;

    explicit ConvertingIoStream(std::unique_ptr<boost::asio::ip::tcp::socket> socket,
                                size_t bufferSize = defaultBufferSize, int threads = 2);

    ~ConvertingIoStream() override;

    [[maybe_unused]] boost::asio::io_context& getExecutor();

    void addNetworkSerializer(SerializerPointer&& ser);

    void addGadgetronSerializer(SerializerPointer&& ser);

    void addNetworkDeserializer(DeserializerPointer&& des);

    void addGadgetronDeserializer(DeserializerPointer&& des);

    void startConverting();
    void cancelGadgetronReadLoop();
    void cancelNetworkReadLoop();
    [[maybe_unused]] void cancelReadLoops();

  private:
    std::size_t socketRead(void* data, std::size_t length);
    std::size_t socketWrite(const void* data, std::size_t length);
    std::size_t gadgetronPipeRead(void* data, std::size_t length);
    std::size_t gadgetronPipeWrite(const void* data, std::size_t length);
    std::size_t clientPipeRead(void* data, std::size_t length);
    std::size_t clientPipeWrite(const void* data, std::size_t length);

    boost::asio::io_context ioContext;
    boost::asio::ip::tcp::socket memberSocket = boost::asio::ip::tcp::socket(ioContext);
    int nthreads;
    std::list<std::thread> threads;
    IoStream networkPipeIo;
    IoStream gadgetronPipeIo;
    boost::asio::readable_pipe gadgetronReadPipe;
    boost::asio::writable_pipe gadgetronWritePipe;
    boost::asio::readable_pipe clientReadPipe;
    boost::asio::writable_pipe clientWritePipe;
    SerializeDeserializeHost serDesGadgetron;
    SerDes serDesNetwork;

    // each coroutine needs its own cancellation signal, one signal cannot be used for 2 different slots
    boost::asio::cancellation_signal killNetworkReadLoop;
    boost::asio::cancellation_signal killGadgetronReadLoop;
    boost::asio::yield_context* net2gadYield;
    boost::asio::yield_context* gad2netYield;
};
} // namespace Gadgetron::Connection

#endif // CONVERTING_IO_STREAM_HPP
