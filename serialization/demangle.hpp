//
// Created by akuurstr on 06/11/23.
//
#ifdef __GNUC__
#include <cxxabi.h>
#endif

#ifndef GADGETRON_DEMANGLE_HPP
#define GADGETRON_DEMANGLE_HPP

char* demangle(const char* mangled) {
#ifdef __GNUC__
    int status = 0;
    char* demangled = abi::__cxa_demangle(mangled, nullptr, nullptr, &status);
#else
    char* demangled = mangled;
#endif
    return demangled;
}
#endif // GADGETRON_DEMANGLE_HPP
