#pragma once
#ifndef CONVERTING_STREAM_FROM_SOCKET_HPP
#define CONVERTING_STREAM_FROM_SOCKET_HPP
#include <boost/asio.hpp>
#include <memory>

namespace Gadgetron::Connection { // NOLINT(*-identifier-naming)
std::unique_ptr<std::iostream> convertingStreamFromSocket(std::unique_ptr<boost::asio::ip::tcp::socket> socket);
} // namespace Gadgetron::Connection

#endif // CONVERTING_STREAM_FROM_SOCKET_HPP
