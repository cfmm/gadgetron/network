/*
 gadgetron readers return a Core::Message object which we need convert to a std::any for gadgetron_protocol.
 unfortunately the gadgetron function to unpack messages makes copies of the data, which we want to avoid for
 large images. Instead of using gadgetron's readers, we will use lower level functions to read the stream.
*/
#pragma once
#ifndef GADGETRON_GADGETRON_DESERIALIZERS_HPP
#define GADGETRON_GADGETRON_DESERIALIZERS_HPP

#include "MessageID.h"
#include "gadgetron_protocol/WrappedIsmrmrd.hpp"
#include "gadgetron_protocol/deserializers/Deserializer.hpp"
#include "readers/AcquisitionReader.h"
#include "readers/ImageReader.h"
#include "readers/WaveformReader.h"

namespace Gadgetron::Connection { // NOLINT(*-identifier-naming)
using namespace Gadgetron;

class ResponseDeserializer : public gadgetron_protocol::deserializers::Deserializer {
  public:
    ResponseDeserializer() { memberId = Core::MessageID::RESPONSE; }

    gadgetron_protocol::any deserialize(std::istream& istream) const override {
        // method from apps/clients/gadgetron_ismrmrd_client/gadgetron_ismrmrd_client.cpp ln 279
        auto correlationId = Core::IO::read<uint64_t>(istream);
        auto response = Core::IO::read_string_from_stream<uint64_t>(istream);

        return {gadgetron_protocol::Response(response, correlationId)};
    }
};

class ImageDeserializer : public gadgetron_protocol::deserializers::Deserializer {
  public:
    ImageDeserializer() { memberId = Core::MessageID::GADGET_MESSAGE_ISMRMRD_IMAGE; }

    gadgetron_protocol::any deserialize(std::istream& istream) const override {
        auto imgStruct = std::make_unique<ISMRMRD::ISMRMRD_Image>();
        imgStruct->head = Core::IO::read<ISMRMRD::ImageHeader>(istream);

        auto serializedMeta = Core::IO::read_string_from_stream<uint64_t>(istream);

        if (serializedMeta.empty()) {
            imgStruct->attribute_string = nullptr;
        } else {
            size_t length = serializedMeta.size();
            imgStruct->head.attribute_string_len = static_cast<uint32_t>(length);
            // malloc because ismrmrd_cleanup_image() uses free
            imgStruct->attribute_string = (char*)malloc(sizeof(char) * (length + 1));
            strncpy(imgStruct->attribute_string, serializedMeta.c_str(), length + 1);
        }
        ISMRMRD::ismrmrd_make_consistent_image(imgStruct.get());

        auto datatype = imgStruct->head.data_type;

        if (datatype == ISMRMRD::ISMRMRD_USHORT) {
            return convert<uint16_t>(istream, std::move(imgStruct));
        } else if (datatype == ISMRMRD::ISMRMRD_SHORT) {
            return convert<int16_t>(istream, std::move(imgStruct));
        } else if (datatype == ISMRMRD::ISMRMRD_UINT) {
            return convert<uint32_t>(istream, std::move(imgStruct));
        } else if (datatype == ISMRMRD::ISMRMRD_INT) {
            return convert<int32_t>(istream, std::move(imgStruct));
        } else if (datatype == ISMRMRD::ISMRMRD_FLOAT) {
            return convert<float>(istream, std::move(imgStruct));
        } else if (datatype == ISMRMRD::ISMRMRD_DOUBLE) {
            return convert<double>(istream, std::move(imgStruct));
        } else if (datatype == ISMRMRD::ISMRMRD_CXFLOAT) {
            return convert<std::complex<float>>(istream, std::move(imgStruct));
        } else if (datatype == ISMRMRD::ISMRMRD_CXDOUBLE) {
            return convert<std::complex<double>>(istream, std::move(imgStruct));
        } else {
            throw std::runtime_error("Trying to deserialize message with unrecognized Image type");
        }
    }

  private:
    template <typename T>
    gadgetron_protocol::any convert(std::istream& istream, std::unique_ptr<ISMRMRD::ISMRMRD_Image> imgStruct) const {
        auto numData = imgStruct->head.matrix_size[0] * imgStruct->head.matrix_size[1] *
                       imgStruct->head.matrix_size[2] * imgStruct->head.channels;
        Core::IO::read(istream, static_cast<T*>(imgStruct->data), numData);
        gadgetron_protocol::WrappedImage<T> img(std::move(imgStruct));
        return gadgetron_protocol::any(std::move(img));
    }
};

class AcquisitionDeserializer : public gadgetron_protocol::deserializers::Deserializer {
  public:
    AcquisitionDeserializer() { memberId = Core::MessageID::GADGET_MESSAGE_ISMRMRD_ACQUISITION; }

    gadgetron_protocol::any deserialize(std::istream& istream) const override {
        auto acqStruct = std::make_unique<ISMRMRD::ISMRMRD_Acquisition>();
        acqStruct->head = Core::IO::read<ISMRMRD::AcquisitionHeader>(istream);
        ISMRMRD::ismrmrd_make_consistent_acquisition(acqStruct.get());

        if (acqStruct->head.trajectory_dimensions) {
            auto numTraj = acqStruct->head.trajectory_dimensions * acqStruct->head.number_of_samples;
            Core::IO::read(istream, static_cast<float*>(acqStruct->traj), numTraj);
        }
        auto numData = acqStruct->head.active_channels * acqStruct->head.number_of_samples;
        Core::IO::read(istream, static_cast<std::complex<float>*>(acqStruct->data), numData);

        gadgetron_protocol::WrappedAcquisition acq(std::move(acqStruct));

        return {std::move(acq)};
    }
};

class WaveformDeserializer : public gadgetron_protocol::deserializers::Deserializer {
  public:
    WaveformDeserializer() { memberId = Core::MessageID::GADGET_MESSAGE_ISMRMRD_WAVEFORM; }

    gadgetron_protocol::any deserialize(std::istream& istream) const override {
        ISMRMRD::Waveform waveform;
        waveform.head = Core::IO::read<ISMRMRD::WaveformHeader>(istream);
        ISMRMRD::ismrmrd_make_consistent_waveform(&waveform);
        auto numData = waveform.head.number_of_samples * waveform.head.channels;
        Core::IO::read(istream, static_cast<uint32_t*>(waveform.data), numData);
        return {std::move(waveform)};
    }
};

class CloseDeserializer : public gadgetron_protocol::deserializers::Deserializer {
  public:
    CloseDeserializer() { memberId = Core::MessageID::CLOSE; }

    gadgetron_protocol::any deserialize(std::istream& istream) const override {
        return {gadgetron_protocol::CloseObj()};
    }
};

} // namespace Gadgetron::Connection

#endif // GADGETRON_GADGETRON_DESERIALIZERS_HPP
