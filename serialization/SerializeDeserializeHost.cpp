#include "SerializeDeserializeHost.hpp"

namespace Gadgetron::Connection {
using namespace gadgetron_protocol;

void SerializeDeserializeHost::serializeId(std::ostream& ostream, uint16_t id) const {
    ostream.write(reinterpret_cast<const char*>(&id), sizeof(uint16_t));
}

uint16_t SerializeDeserializeHost::deserializeId(std::istream& istream) const {
    uint16_t id = 0;
    istream.read(reinterpret_cast<char*>(&id), sizeof(uint16_t));
    return id;
}
} // namespace Gadgetron::Connection
