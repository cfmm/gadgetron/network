/*
 gadgetron writers requre a Core::Message parameter which is unpacked and its components are serialized onto
 the output stream. unfortunately the gadgetron function to unpack messages makes copies of the data, which
 we want to avoid for large images. Instead of using gadgetron's writer, we will use lower level functions to
 write images, acquisitions, and waveforms directly to the stream.
*/
#pragma once
#ifndef GADGETRON_SERIALIZERS_HPP
#define GADGETRON_SERIALIZERS_HPP
#include "MessageID.h"
#include "gadgetron_protocol/MessageId.hpp"
#include "gadgetron_protocol/Serializers.hpp"
#include "gadgetron_protocol/serializers/Serializer.hpp"
#include "writers/AcquisitionWriter.h"
#include "writers/ImageWriter.h"
#include "writers/WaveformWriter.h"
#include <ismrmrd/meta.h>

namespace Gadgetron::Connection { // NOLINT(*-identifier-naming)
class ConfigFileSerializer : public gadgetron_protocol::serializers::ConfigFileSerializer {
  public:
    ConfigFileSerializer() { memberId = Core::MessageID::FILENAME; }

    void serialize(std::ostream& ostream, gadgetron_protocol::any const& obj) const override {
        auto const& rConfigFilename = gadgetron_protocol::any_cast<gadgetron_protocol::ConfigFile const&>(obj);

        auto stringLength = static_cast<gadgetron_protocol::ConfigFile::difference_type>(rConfigFilename.size());
        if (stringLength > 1023) {
            throw std::runtime_error("Config filename must be less than 1023 characters.");
        }
        std::array<char, 1024> data; // NOLINT(*-pro-type-member-init)
        std::copy(rConfigFilename.begin(), rConfigFilename.begin() + stringLength, data.data());
        data[rConfigFilename.size()] = '\0';

        Core::IO::write<std::array<char, 1024>>(ostream, data);
    }
};

class ConfigScriptSerializer : public gadgetron_protocol::serializers::ConfigScriptSerializer {
  public:
    ConfigScriptSerializer() { memberId = Core::MessageID::CONFIG; }

    void serialize(std::ostream& ostream, gadgetron_protocol::any const& obj) const override {
        auto const& rConfigScript = gadgetron_protocol::any_cast<gadgetron_protocol::ConfigScript const&>(obj);

        auto n = static_cast<uint32_t>(rConfigScript.size());
        Core::IO::write<uint32_t>(ostream, n);
        ostream.write(rConfigScript.c_str(), n);
    }
};

class HeaderSerializer : public gadgetron_protocol::serializers::HeaderSerializer {
  public:
    HeaderSerializer() { memberId = Core::MessageID::HEADER; }

    void serialize(std::ostream& ostream, gadgetron_protocol::any const& obj) const override {
        auto const& rHeader = gadgetron_protocol::any_cast<ISMRMRD::IsmrmrdHeader const&>(obj);
        std::stringstream ss;
        ISMRMRD::serialize(rHeader, ss);
        Core::IO::write<uint32_t>(ostream, static_cast<uint32_t>(ss.tellp()));
        ostream << ss.str();
    }
};

class QuerySerializer : public gadgetron_protocol::serializers::QuerySerializer {
  public:
    QuerySerializer() { memberId = Core::MessageID::QUERY; }

    void serialize(std::ostream& ostream, gadgetron_protocol::any const& obj) const override {
        auto const& rCastObj = gadgetron_protocol::any_cast<gadgetron_protocol::Query const&>(obj);
        // method from apps/clients/gadgetron_ismrmrd_client/gadgetron_ismrmrd_client.cpp ln 1214
        uint64_t reserved = 0;
        Core::IO::write<uint64_t>(ostream, reserved);
        Core::IO::write<uint64_t>(ostream, rCastObj.correlationId);
        Core::IO::write_string_to_stream<uint64_t>(ostream, rCastObj.query);
    }
};

class ImageSerializer : public gadgetron_protocol::serializers::ImageSerializer {
  public:
    ImageSerializer() { memberId = Core::MessageID::GADGET_MESSAGE_ISMRMRD_IMAGE; }

    void serialize(std::ostream& ostream, gadgetron_protocol::any const& obj) const override {
        if (obj.type() == typeid(ISMRMRD::Image<uint16_t>)) {
            serialize<uint16_t>(ostream, obj);
        } else if (obj.type() == typeid(ISMRMRD::Image<int16_t>)) {
            serialize<int16_t>(ostream, obj);
        } else if (obj.type() == typeid(ISMRMRD::Image<uint32_t>)) {
            serialize<uint32_t>(ostream, obj);
        } else if (obj.type() == typeid(ISMRMRD::Image<int32_t>)) {
            serialize<int32_t>(ostream, obj);
        } else if (obj.type() == typeid(ISMRMRD::Image<float>)) {
            serialize<float>(ostream, obj);
        } else if (obj.type() == typeid(ISMRMRD::Image<double>)) {
            serialize<double>(ostream, obj);
        } else if (obj.type() == typeid(ISMRMRD::Image<std::complex<float>>)) {
            serialize<std::complex<float>>(ostream, obj);
        } else if (obj.type() == typeid(ISMRMRD::Image<std::complex<double>>)) {
            serialize<std::complex<double>>(ostream, obj);
        }
    }

  private:
    template <typename T> void serialize(std::ostream& ostream, gadgetron_protocol::any const& obj) const {

        auto const& rImg = gadgetron_protocol::any_cast<ISMRMRD::Image<T> const&>(obj);
        auto hdr = rImg.getHead();
        auto imageData = hoNDArray<T>(hdr.matrix_size[0], hdr.matrix_size[1], hdr.matrix_size[2], hdr.channels,
                                      const_cast<T*>(rImg.getDataPtr()));

        Core::optional<ISMRMRD::MetaContainer> meta;
        if (rImg.getAttributeStringLength() > 0) {
            meta = ISMRMRD::MetaContainer();
            ISMRMRD::deserialize(rImg.getAttributeString(), *meta);
        }
        Core::IO::write(ostream, Core::Image<T>{hdr, std::move(imageData), meta});
    }
};

class AcquisitionSerializer : public gadgetron_protocol::serializers::AcquisitionSerializer {
  public:
    AcquisitionSerializer() { memberId = Core::MessageID::GADGET_MESSAGE_ISMRMRD_ACQUISITION; }

    void serialize(std::ostream& ostream, gadgetron_protocol::any const& obj) const override {
        auto const& rAcq = gadgetron_protocol::any_cast<ISMRMRD::Acquisition const&>(obj);

        auto hdr = rAcq.getHead();

        auto data = hoNDArray<std::complex<float>>(hdr.number_of_samples, hdr.active_channels,
                                                   const_cast<std::complex<float>*>(rAcq.getDataPtr()));

        Core::optional<hoNDArray<float>> trajectory;
        if (hdr.trajectory_dimensions) {
            trajectory = hoNDArray<float>(hdr.trajectory_dimensions, hdr.number_of_samples,
                                          const_cast<float*>(rAcq.getTrajPtr()));
        }
        Core::IO::write(ostream, hdr);
        if (trajectory)
            Core::IO::write(ostream, trajectory->get_data_ptr(), trajectory->get_number_of_elements());
        Core::IO::write(ostream, data.get_data_ptr(), data.get_number_of_elements());
    }
};

class WaveformSerializer : public gadgetron_protocol::serializers::WaveformSerializer {
  public:
    WaveformSerializer() { memberId = Core::MessageID::GADGET_MESSAGE_ISMRMRD_WAVEFORM; }

    void serialize(std::ostream& ostream, gadgetron_protocol::any const& obj) const override {
        auto const& waveform = gadgetron_protocol::any_cast<ISMRMRD::Waveform const&>(obj);
        ISMRMRD::ISMRMRD_WaveformHeader const& hdr = waveform.head;
        auto data = hoNDArray<uint32_t>(hdr.number_of_samples, hdr.channels, waveform.data);
        Core::IO::write(ostream, hdr);
        Core::IO::write(ostream, data.get_data_ptr(), data.get_number_of_elements());
    }
};

class CloseSerializer : public gadgetron_protocol::serializers::CloseSerializer {
  public:
    CloseSerializer() { memberId = Core::MessageID::CLOSE; }
    void serialize(std::ostream& ostream, gadgetron_protocol::any const& obj) const override {}
};

} // namespace Gadgetron::Connection
#endif // GADGETRON_SERIALIZERS_HPP
