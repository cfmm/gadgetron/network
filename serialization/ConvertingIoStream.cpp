#include "ConvertingIoStream.hpp"
#include "demangle.hpp"
#include "gadgetron_protocol/MakeUnique.hpp"
#include "log.h"
#include <boost/scope_exit.hpp>

#define NON_FATAL_TIMEOUT 0

namespace Gadgetron::Connection {
using namespace gadgetron_protocol;
ConvertingIoStream::ConvertingIoStream(std::unique_ptr<boost::asio::ip::tcp::socket> socket, size_t bufferSize,
                                       int nthreads)
    : IoStream([this](auto data, auto length) { return this->clientPipeRead(data, length); },
               [this](auto data, auto length) { return this->clientPipeWrite(data, length); }, bufferSize),
      networkPipeIo([this](auto data, auto length) { return this->socketRead(data, length); },
                    [this](auto data, auto length) { return this->socketWrite(data, length); }, bufferSize),
      gadgetronPipeIo([this](auto data, auto length) { return this->gadgetronPipeRead(data, length); },
                      [this](auto data, auto length) { return this->gadgetronPipeWrite(data, length); }, bufferSize),
      gadgetronReadPipe(ioContext), gadgetronWritePipe(ioContext), clientReadPipe(ioContext),
      clientWritePipe(ioContext), nthreads(nthreads), gad2netYield(nullptr), net2gadYield(nullptr) {

    auto fd = socket->release();
    memberSocket.assign(boost::asio::ip::tcp::v4(), fd);

    boost::asio::connect_pipe(gadgetronReadPipe, clientWritePipe);
    boost::asio::connect_pipe(clientReadPipe, gadgetronWritePipe);

    // the socket async read is completed in its own io context
    // the coroutines and pipe operations are performed by this class' io context
    // i.e. this io_context will run the coroutine up until the socket async read with a yield. then the coroutine
    // will give control back to this io context's event loop.  usually the same iocontext's event loop would
    // perform  the socket's async read. when the async read was done, it would give control back to the coroutine.
    // However, since our socket's operations are done on a different (its own) io_context event loop, our
    // iocontext event loop will continue doing other work until the socket's io context's event loop finds time
    // to finish the socket async read...then our event loop will be able to give control back to the coroutine.

    // convert network to gadgetron
    boost::asio::spawn(
        ioContext,
        [this](boost::asio::yield_context yield) {
            GDEBUG("Converter starting network-side-reading-loop.\n");

            // On leaving scope ensure the pointer to the yield context is reset to nullptr
            BOOST_SCOPE_EXIT(net2gadYield) { net2gadYield = nullptr; }
            BOOST_SCOPE_EXIT_END

#pragma clang diagnostic push
#pragma ide diagnostic ignored "LocalValueEscapesScope"
            // net2gadYield must never ever be used anywhere outside the execution context of this coroutine.
            net2gadYield = &yield;
#pragma clang diagnostic pop
            bool close = false;
            while (!close && networkPipeIo.good() && gadgetronPipeIo.good()) {
                std::any msg;
                try {
                    msg = serDesNetwork.deserialize(networkPipeIo);
                } catch (const std::exception& e) {
                    GERROR_STREAM("Converter network-side-reading-loop deserialization error: " << e.what());
                    throw;
                }
                char* demangled = demangle(msg.type().name());
                GDEBUG_STREAM("Converter network-side-reading-loop deserialized message of type " << demangled);
                if (msg.type() == typeid(CloseObj)) {
                    close = true;
                }
                if (!serDesGadgetron.serialize(gadgetronPipeIo, msg)) {
                    auto errorMessage = "Converter network-side-reading-loop could not serialize message"
                                        " of type " +
                                        std::string(demangled);
                    GERROR_STREAM(errorMessage);
                    throw std::invalid_argument(errorMessage);
                }
                GDEBUG_STREAM("Converter network-side-reading-loop serialized message of type " << demangled);
                free(demangled);
            }
            GDEBUG("Converter exited network-side-reading-loop.\n");
        },
        boost::asio::bind_cancellation_slot(killNetworkReadLoop.slot(), [](const std::exception_ptr& eptr) {
            if (eptr) {
                try {
                    std::rethrow_exception(eptr);
                }
#if NON_FATAL_TIMEOUT
                catch (const boost::system::system_error& e) {
                    auto msg = e.what();
                    if (strlen(msg) >= 18 && std::strncmp(e.what(), "Operation canceled", 18) == 0) {
                        GDEBUG("Converter network-side-reading-loop received cancellation signal.\n");
                        exceptions(goodbit); // avoids uncaught exception in the cancellation handler
                        setstate(badbit);
                    } else {
                        GERROR_STREAM("Converter network-side-reading-loop encountered error: " << e.what());
                        throw;
                    }
                }
#endif
                catch (const std::exception& e) {
                    GERROR_STREAM("Converter network-side-reading-loop encountered error: " << e.what());
                    throw;
                }
            }
        }));

    // convert gadgetron to network
    boost::asio::spawn(
        ioContext,
        [this](boost::asio::yield_context yield) {
            GDEBUG("Converter starting gadgetron-side-reading-loop.\n");

            // On leaving scope ensure the pointer to the yield context is reset to nullptr
            BOOST_SCOPE_EXIT(gad2netYield) { gad2netYield = nullptr; }
            BOOST_SCOPE_EXIT_END

#pragma clang diagnostic push
#pragma ide diagnostic ignored "LocalValueEscapesScope"
            // gad2netYield must never ever be used anywhere outside the execution context of this coroutine.
            gad2netYield = &yield;
#pragma clang diagnostic pop
            bool close = false;
            while (!close && gadgetronPipeIo.good() && networkPipeIo.good()) {
                std::any msg;
                try {
                    msg = serDesGadgetron.deserialize(gadgetronPipeIo);
                } catch (const std::exception& e) {
                    GERROR_STREAM("Converter gadgetron-side-reading-loop deserialization error: " << e.what());
                    throw;
                }
                char* demangled = demangle(msg.type().name());
                GDEBUG_STREAM("Converter gadgetron-side-reading-loop deserialized message of type " << demangled);
                if (msg.type() == typeid(CloseObj)) {
                    close = true;
                }

                if (!serDesNetwork.serialize(networkPipeIo, msg)) {
                    auto errorMessage = "Converter gadgetron-side-reading-loop could not serialize message"
                                        " of type " +
                                        std::string(demangled);
                    GERROR_STREAM(errorMessage);
                    throw std::invalid_argument(errorMessage);
                }
                GDEBUG_STREAM("Converter gadgetron-side-reading-loop serialized message of type " << demangled);
                free(demangled);
            }
            GDEBUG("Converter exited gadgetron-side-reading-loop.\n");
        },
        boost::asio::bind_cancellation_slot(killGadgetronReadLoop.slot(), [](const std::exception_ptr& eptr) {
            if (eptr) {
                try {
                    std::rethrow_exception(eptr);
                }
#if NON_FATAL_TIMEOUT
                catch (const boost::system::system_error& e) {
                    auto msg = e.what();
                    if (strlen(msg) >= 18 && std::strncmp(e.what(), "Operation canceled", 18) == 0) {
                        GDEBUG("Converter gadgetron-side-reading-loop received cancellation signal.\n");
                        exceptions(goodbit); // avoids uncaught exception in the cancellation handler
                        setstate(badbit);
                    } else {
                        GERROR_STREAM("Converter gadgetron-side-reading-loop encountered error: " << e.what());
                        throw;
                    }

                }
#endif
                catch (const std::exception& e) {
                    GERROR_STREAM("Converter gadgetron-side-reading-loop encountered error: " << e.what());
                    throw;
                }
            }
        }));
}

void ConvertingIoStream::startConverting() {
    for (int i = 0; i < nthreads; ++i) {
        threads.emplace_back([&]() { ioContext.run(); });
    }
}

[[maybe_unused]] boost::asio::io_context& ConvertingIoStream::getExecutor() { return ioContext; }

void ConvertingIoStream::addNetworkSerializer(SerializerPointer&& ser) { serDesNetwork.addSerializer(std::move(ser)); }

void ConvertingIoStream::addGadgetronSerializer(SerializerPointer&& ser) {
    serDesGadgetron.addSerializer(std::move(ser));
}

void ConvertingIoStream::addNetworkDeserializer(DeserializerPointer&& des) {
    serDesNetwork.addDeserializer(std::move(des));
}

void ConvertingIoStream::addGadgetronDeserializer(DeserializerPointer&& des) {
    serDesGadgetron.addDeserializer(std::move(des));
}

void ConvertingIoStream::cancelGadgetronReadLoop() {
    killGadgetronReadLoop.emit(boost::asio::cancellation_type::terminal);
}
void ConvertingIoStream::cancelNetworkReadLoop() { killNetworkReadLoop.emit(boost::asio::cancellation_type::terminal); }
[[maybe_unused]] void ConvertingIoStream::cancelReadLoops() {
    cancelNetworkReadLoop();
    cancelGadgetronReadLoop();
}

ConvertingIoStream::~ConvertingIoStream() {
    GDEBUG("Converter entering destructor, but waiting for reading loops to exit.\n");
    for (auto& t : threads) {
        t.join();
    }
    GDEBUG("Converter exiting destructor.\n");
}

#pragma clang diagnostic push
#pragma ide diagnostic ignored "UnreachableCallsOfFunction"
std::size_t ConvertingIoStream::socketRead(void* data, std::size_t length) {
    BOOST_ASSERT_MSG(nullptr != net2gadYield, "Yield context for net2gad is invalid during read");
    return memberSocket.async_read_some(boost::asio::buffer(data, length), *net2gadYield);
}

std::size_t ConvertingIoStream::socketWrite(const void* data, std::size_t length) {
    BOOST_ASSERT_MSG(nullptr != gad2netYield, "Yield context for gad2net is invalid during write");
    return boost::asio::async_write(memberSocket, boost::asio::buffer(data, length), *gad2netYield);
}

std::size_t ConvertingIoStream::gadgetronPipeRead(void* data, std::size_t length) {
    BOOST_ASSERT_MSG(nullptr != gad2netYield, "Yield context for gad2net is invalid during read");
    return gadgetronReadPipe.async_read_some(boost::asio::buffer(data, length), *gad2netYield);
}

std::size_t ConvertingIoStream::gadgetronPipeWrite(const void* data, std::size_t length) {
    BOOST_ASSERT_MSG(nullptr != net2gadYield, "Yield context for net2gad is invalid during write");
    return boost::asio::async_write(gadgetronWritePipe, boost::asio::buffer(data, length), *net2gadYield);
}

std::size_t ConvertingIoStream::clientPipeRead(void* data, std::size_t length) {
    std::future<size_t> f = clientReadPipe.async_read_some(boost::asio::buffer(data, length), boost::asio::use_future);
    return f.get();
}

std::size_t ConvertingIoStream::clientPipeWrite(const void* data, std::size_t length) {
    std::future<size_t> f =
        boost::asio::async_write(clientWritePipe, boost::asio::buffer(data, length), boost::asio::use_future);
    return f.get();
}
#pragma clang diagnostic pop
} // namespace Gadgetron::Connection
