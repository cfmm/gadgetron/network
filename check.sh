#!/bin/bash

pushd .
mkdir -p code_style/compdb
cd code_style
cmake -DCMAKE_CXX_COMPILER=clang++ -DCMAKE_EXPORT_COMPILE_COMMANDS=ON ${1:-../gadgetron}
compdb -p . list > compdb/compile_commands.json
clang-tidy --version
popd
run-clang-tidy -quiet -p code_style/compdb '.*/gadgetron/network/(?!(protocol|gadgetron|code_style)).*'
